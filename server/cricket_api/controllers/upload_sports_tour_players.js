var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var sportsTourService = require('../../tours/services/index');

function uploadSportsTourPlayers(request, response, next) {
    logger.info('uploadSportsTourPlayers request', request.body);
    //console.log(request.file);
    var requestObject = request.body;

    var responseObject = new Object();

    statsService.uploadSportsTourPlayersListService(requestObject, function(error, data) {
        if(data.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = data.responseCode;
            responseObject.message = responseMessage[data.responseCode];
        } else {
            responseObject.respCode = data.responseCode;
            responseObject.respData = data.responseData;
        }

        logger.info("uploadSportsTourPlayers API :- Response - %j", responseObject);
        response.json(responseObject);
    });

}
 
module.exports = uploadSportsTourPlayers;


// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsTourPlayers(request, response);
    })();
}