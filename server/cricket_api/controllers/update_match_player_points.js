var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var cricketApiRequestService = require('../cricket_request/index');
var cricketApiRequest = require('../../cricket_api/cricket_request/index');
var tourMatchService = require('../../tour_matches/services/index');

function uploadMatchPlayerPoints(request, response, next) {
    logger.info('uploadMatchPlayerPoints request data', request.body);
    var requestObject = request.body;
    var responseObject = new Object();


    var status = ['RUNNING', 'UNDER_REVIEW'];
    tourMatchService.findTourMatchesList({
        matchId : requestObject.matchId,
        status: status,
        isLeaguePrizeUpdate: true
    }, function (err, matchListResponse) {

        var matchList = matchListResponse.responseData;

        async.eachSeries(matchList,
            // 2nd param is the function that each item is passed to
            function (matchRow, callback) {

                cricketApiRequest.fetchSportsMatcheLiveDataRequest({key: matchRow.key}, function (err, apiMatchData) {

                    statsService.calculateMatchPlayersPointService({
                        matchId: matchRow.matchId,
                        apiMatchData: apiMatchData
                    }, function (error, data) {
                        callback(null);
                    });

                })


            },
            // 3rd param is the function to call when everything's done
            function (err) {
                // All tasks are done now

                if (err) {
                    logger.error(err,'uploadMatchPlayerPoints response');
                    responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                    responseObject.message = responseMessage[responseCode.SOME_INTERNAL_ERROR];
                } else {
                    logger.info('uploadMatchPlayerPoints response', new Date());
                    responseObject.respCode = responseCode.SUCCESS;

                }

                //logger.info("uploadSportsTours API :- Response - %j", responseObject);
                response.json(responseObject);
            }
        );
    })


}

module.exports = uploadMatchPlayerPoints;


// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadMatchPlayerPoints(request, response);
    })();
}