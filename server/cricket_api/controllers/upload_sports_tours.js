var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var cricketApiRequestService = require('../cricket_request/index');

function uploadSportsTours(request, response, next) {
    logger.info('uploadSportsTours request',request.body);

    //console.log("Inside the upload sports tours:",request.body);
    var requestObject = request.body;
    var responseObject = new Object();

    cricketApiRequestService.fetchSportsTourRequest(requestObject,function(err,sportsTours){
        //console.log("API Data",sportsTours);
        if(err){
            responseObject.respCode = sportsTours;
            responseObject.message = responseMessage[sportsTours];
            response.json(responseObject);
            return;
        }else{
            statsService.uploadSportsTourListService(sportsTours, function (error, data) {
                //console.log("Data",data);
                if (error) {
                    responseObject.respCode = data.responseCode;
                    responseObject.message = responseMessage[data.responseCode];
                } else {
                    responseObject.respCode = data.responseCode;
                    responseObject.responseData = data.responseData;
                }

                //logger.info("uploadSportsTours API :- Response - %j", responseObject);
                response.json(responseObject);

                console.log("Response",responseObject);
            });
        }
    });

}

module.exports = uploadSportsTours;


// Unit Test Case
if (require.main === module) {
    (function() {
        var request = {};
        var response = {
            json : function(result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsTours(request, response);
    })();
}