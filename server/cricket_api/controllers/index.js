/**
 * Created by sumit on 2/16/2017.
 */
var uploadSportsTours = require("./upload_sports_tours");
var uploadSportsTourPlayers = require("./upload_sports_tour_players");
var uploadSportsTourMatches = require("./upload_sports_tour_matches");
var uploadMatchPlayerPointsApi = require("./update_match_player_points");

module.exports.uploadSportsToursApi = uploadSportsTours;
module.exports.uploadSportsTourPlayersApi = uploadSportsTourPlayers;
module.exports.uploadSportsTourMatchesApi = uploadSportsTourMatches; 
module.exports.uploadMatchPlayerPointsApi = uploadMatchPlayerPointsApi;
