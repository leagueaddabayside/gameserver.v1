var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var cricketApiRequestService = require('../cricket_request/index');
var sportsTourService = require('../../tours/services/index');

function uploadSportsTourMatches(request, response, next) {
    logger.info('uploadSportsTourMatches request',request.body);
    //console.log(request.file);
    var requestObject = request.body;
    var responseObject = new Object();

    requestObject.status = ['ACTIVE','APPROVED'];
    sportsTourService.findTourList(requestObject,function(err,sportsTours){
        if(err){
            logger.error(err);
            responseObject.respCode = sportsTours;
            responseObject.message = responseMessage[sportsTours];
            response.json(responseObject);
            return;
        }


        async.eachSeries(sportsTours.responseData,
            // 2nd param is the function that each item is passed to
            function (item, callback) {
                logger.trace('fetchSportsTourMatchesRequest',item);
                cricketApiRequestService.fetchSportsTourMatchesRequest({cricketAPIToken : requestObject.cricketAPIToken,seasonKey : item.key},function(err,tourDataResponse){
                    if(err){
                        logger.error(err);
                        callback(err);
                        return;
                    }
                    var reqMatchObj = {};
                    reqMatchObj.seasonData = tourDataResponse.season;
                    reqMatchObj.tourId = item.tourId;

                        statsService.uploadSportsTourMatchListService(reqMatchObj, function (error, data) {
                            if (error) {
                                logger.error(error);
                                callback(error);
                                return;
                            }
                            callback(null,data);
                            return;
                        });

                });
            },
            // 3rd param is the function to call when everything's done
            function (err) {
                if(err){
                    logger.error(err);
                    responseObject.respCode = responseCode.SUCCESS;
                }else{
                    responseObject.respCode = responseCode.SUCCESS;
                }
                // All tasks are done now
                logger.info('uploadSportsTourMatches final done response',responseObject);
                responseObject.message = responseMessage[responseObject.respCode];
                response.json(responseObject);

            }
        );

    });

}

module.exports = uploadSportsTourMatches;


// Unit Test Case
if (require.main === module) {
    (function() {
        var request = {};
        var response = {
            json : function(result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsTourMatches(request, response);
    })();
}