/**
 * Created by sumit on 2/20/2017.
 */

let matchTypeList = [{
    "format": 'one-day',
    "name": "One Day",
    "formatType": "ONE_DAY"
}, {
    "format": 'test',
    "name": "Test",
    "formatType": "TEST"
}, {
    "format": 't20',
    "name": "T20",
    "formatType": "T20"
}]

let matchStatusList = [{
    "status": 'started',
    "description": "One Day",
    "statusType": "ONE_DAY"
}, {
    "status": 'completed',
    "description": "Test",
    "statusType": "TEST"
}, {
    "status": 'notstarted',
    "description": "T20",
    "statusType": "T20"
}]

let skillTypesData = [{
    "skillTypeId": 1,
    "name": "Batsman",
    "skillType" : "BATSMAN"
}, {
    "skillTypeId": 2,
    "name": "Bowler",
    "skillType" : "BOWLER"
}, {
    "skillTypeId": 3,
    "name": "All-Rounder",
    "skillType" : "ALL_ROUNDER"
}, {
    "skillTypeId": 4,
    "name": "Wicket-Keeper",
    "skillType" : "WEEKET_KEEPER"
}, {
    "skillTypeId": 0,
    "name": "Unknown",
    "skillType" : "UNKNOWN"
}]

function getEventFormatType(format) {
    for (var i = 0, length = matchTypeList.length; i < length; i++) {
        var eventRow = matchTypeList[i];
        if (format == eventRow.format) {
            return eventRow.formatType;
        }
    }

}

function getPlayerSkillType(skillTypeId) {
    for (var i = 0, length = skillTypesData.length; i < length; i++) {
        var skillRow = skillTypesData[i];
        if (skillTypeId == skillRow.skillTypeId) {
            return skillRow.skillType;
        }
    }
}

module.exports.getEventFormatType = getEventFormatType;
module.exports.getPlayerSkillType = getPlayerSkillType;


// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.status = "ACTIVE";

    console.log(getEventFormatType(1));
}