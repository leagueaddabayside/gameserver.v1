/**
 * Created by sumit on 2/16/2017.
 */
 "use strict"
// moment.js is the preferred date library
let moment = require('moment');
// access Node.js Crypto library for signature generation
let request = require('request');
let logger = require('../../utils/logger').gameLogs;

function getCricketApiAuthentication(obj,callback) {
    var data = {};
    //for live cricket api
    data.access_key = 'aab8006064fde70f8967c84c61938e57';
    data.secret_key = 'b0f0981d1ca7352c086b2a6a9ecd6e1d';
    data.app_id = 'leagueadda';
    data.device_id = 'server01';
 
    //for kabaddi test
    /*data.access_key = '5b58e1fa09030b30660fb00aa778031c';
    data.secret_key = 'f4340fd1c82e93b85a0973ef0ca7e306';
    data.app_id = 'fantasyleague.kabaddi.test';
    data.device_id = 'server01';
    
    Response Code -  null { 
    access_token: '2s150599678083667s930332716430402971',
    expires: '1510643579.0' }

    $ curl -X POST \
     -d "access_key=5b58e1fa09030b30660fb00aa778031c" \
     -d "secret_key=f4340fd1c82e93b85a0973ef0ca7e306" \
     -d "app_id=fantasyleague.kabaddi.test" \
     -d "device_id=server01" \
     https://rest.cricketapi.com/rest/v2/auth/

    */

    request.post({
        url: 'https://rest.cricketapi.com/rest/v2/auth/',
        form: data
    }, function (error, response, body) {
        logger.error(error);
        logger.info(response.statusCode);
        logger.info(body);
        var respData = JSON.parse(body);

        console.log("Resp Data",respData);

        callback(null,respData.auth);
    });
}

module.exports = getCricketApiAuthentication;


// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.status = "ACTIVE";

    getCricketApiAuthentication(requestObject, function (error, responseObject) {
        console.log("Response Code - ", error, responseObject);

    });
}


