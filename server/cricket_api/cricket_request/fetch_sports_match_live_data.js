/**
 * Created by sumit on 2/15/2017.
 */
let request = require('request');
let statsRequest = require('./index');
let logger = require('../../utils/logger').gameLogs;
var responseCode = require("../../utils/response_code");
var zlib = require('zlib');
const redis = require('../../redis/redisConnection');
var moment = require('moment');
var getCricketApiAuthentication = require('./cricketapi_authentication');
var config = require('../../../bin/config');
var scoreTemplate = require("../../score_template/services/index");
var utf8 = require('utf8');

function fetchSportsMatchLiveData(requestObject, responseCb) {
    logger.info('fetchSportsMatchLiveData requestObject', requestObject);


    if (!config.isProduction) {
        logger.info('fetchSportsMatchLiveData isProduction', false);

        scoreTemplate.findScoreTemplateByProperty({templateName : 'SIMULATOR_DATA'},function(err,simulatorData){
            logger.info('SIMULATOR_DATA', simulatorData);
            if(simulatorData && simulatorData.responseData){
                let responseData = simulatorData.responseData;

                if(responseData.data && responseData.data.cricketApiStatus == 'YES'){
                    let currentTemplate = responseData.data[requestObject.key];
                    if(currentTemplate){
                        logger.info('fetchSportsMatchLiveData currentTemplate', currentTemplate,requestObject.key);
                        scoreTemplate.findScoreTemplateByProperty({templateId : currentTemplate},function(err,scoreTemplate){
                            if(scoreTemplate && scoreTemplate.responseData && scoreTemplate.responseData.data){
                                responseCb(null,scoreTemplate.responseData.data.gameScoreData.data);
                            }
                        });
                    }else{
                        responseCb(true);
                    }

                }else{
                    getCricketApiLiveMatchData(requestObject,responseCb);
                }
            }else{
                getCricketApiLiveMatchData(requestObject,responseCb);
            }

        })
    }else{
        getCricketApiLiveMatchData(requestObject,responseCb);
    }

}

function getCricketApiLiveMatchData(requestObject,responseCb) {
    getLiveMatchToken(function (err, token) {
        logger.info('fetchSportsMatchLiveData token', token);
        console.log(token);
        request('https://rest.cricketapi.com/rest/v2/match/' + requestObject.key + '/?access_token=' + token, {encoding: null},
            function (err, response, body) {
                console.log('status code',response.statusCode);
                if (err) {
                    logger.error(err);
                    responseCb(true, responseCode.CRICKETAPI_INTERNAL_ERROR);
                    return;
                }

                if (response.statusCode === 200) {
                    logger.info(response.statusCode, response.headers['content-encoding']);
                    console.log("Response:",response.headers);
                    if (response.headers['content-encoding'] == 'gzip') {
                        zlib.gunzip(body, function (err, dezipped) {
                            logger.info(dezipped.toString());

                            var respData = JSON.parse(dezipped.toString());
                            console.log('respData',respData)
                            if (respData.status_code == 200) {
                                responseCb(null, respData.data);
                            } else {
                                logger.error('', respData.status_code);
                                responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                            }

                        });

                    } else {
                        try{
                            let respUtf8 =JSON.parse(utf8.decode(body+''));
                            logger.info(respUtf8);
                            if(respUtf8.status_code == 403){
                                redis.del('CRICKETAPI_TOKEN_EXPIRE');
                            }
                            //console.log(response.headers,utf8.decode(body+''));
                            responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                        }catch(e){
                            logger.error(e);
                            responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                        }

                    }
                } else {
                    logger.error(response.statusCode, response.statusMessage);
                    responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                }

            });
    })
}

function getLiveMatchToken(cb) {
    let timeFromEpoch = moment.utc().unix();

    redis.get('CRICKETAPI_TOKEN_EXPIRE', function (err, data) {
        if (data && data > timeFromEpoch) {
            redis.get('CRICKETAPI_ACCESSTOKEN', function (err, token) {
                if (token) {
                    cb(null, token);
                } else {
                    getCricketApiAuthentication(err, function (authResponse) {
                        redis.set('CRICKETAPI_TOKEN_EXPIRE', authResponse.expires);
                        redis.set('CRICKETAPI_ACCESSTOKEN', authResponse.access_token);
                        cb(null, authResponse.access_token);
                    });
                }
            });
        } else {
            getCricketApiAuthentication({}, function (err, authResponse) {
                redis.set('CRICKETAPI_TOKEN_EXPIRE', authResponse.expires);
                redis.set('CRICKETAPI_ACCESSTOKEN', authResponse.access_token);
                cb(null, authResponse.access_token);
            });
        }
    });
}


module.exports = fetchSportsMatchLiveData;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.key = 'indaus_2017_test_02';

    //requestObject.status = "ACTIVE";

    fetchSportsMatchLiveData(requestObject, function (error, responseObject) {
        console.log("Response Code - ", error, responseObject);

    });
}