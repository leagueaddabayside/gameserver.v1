/**
 * Created by sumit on 2/15/2017.
 */
let request = require('request');
let statsRequest = require('./index');
let logger = require('../../utils/logger').gameLogs;
var responseCode = require("../../utils/response_code");
var zlib = require('zlib');
var utf8 = require('utf8');
const redis = require('../../redis/redisConnection');

function fetchSportsToursPlayersRequest(requestObject, responseCb) {
    logger.info('fetchSportsToursPlayersRequest requestObject',requestObject);
    console.log("Tour Player Data",requestObject);
    request('https://rest.cricketapi.com/rest/v2/season/'+requestObject.seasonKey+'/team/'+requestObject.teamKey+'/?access_token='+requestObject.cricketAPIToken, {encoding: null},
        function(err, response, body) {
            if (err) {
                logger.error(err);
                responseCb(true, responseCode.CRICKETAPI_INTERNAL_ERROR);
                return;
            }

            if (response.statusCode === 200) {
                logger.info(response.statusCode,response.headers['content-encoding']);
                if (response.headers['content-encoding'] == 'gzip') {
                    zlib.gunzip(body, function (err, dezipped) {
                        logger.info(dezipped.toString());

                        var respData =JSON.parse(dezipped.toString());
                        if (respData.status_code == 200) {
                            responseCb(null, respData.data);
                        } else {
                            logger.error('',respData.status_code);
                            responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                        }

                    });
                } else {
                    try{
                        let respUtf8 =JSON.parse(utf8.decode(body+''));
                        logger.info(respUtf8);
                        if(respUtf8.status_code == 403){
                            redis.del('CRICKETAPI_TOKEN_EXPIRE');
                        }
                        //console.log(response.headers,utf8.decode(body+''));
                        responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                    }catch(e){
                        logger.error(e);
                        responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
                    }

                }
            } else {
                logger.error(response.statusCode, response.statusMessage);
                responseCb(true, responseCode.CRICKETAPI_DEFINED_ERROR);
            }

        });
}

module.exports = fetchSportsToursPlayersRequest;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.teamKey = 'nzaus_2017_aus';
    requestObject.seasonKey = 'nzaus_2017';

    //requestObject.status = "ACTIVE";

    fetchSportsToursPlayersRequest(requestObject, function(error, responseObject) {
        console.log("Response Code - " ,error, responseObject);

    });
}