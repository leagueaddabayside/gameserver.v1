/**
 * Created by sumit on 2/15/2017.
 */
let request = require('request');
let statsRequest = require('./index');
let logger = require('../../utils/logger').gameLogs;
var responseCode = require("../../utils/response_code");
var zlib = require('zlib');
const redis = require('../../redis/redisConnection');
var moment = require('moment');
var getCricketApiAuthentication = require('./cricketapi_authentication');

function getCricketApiToken(requestObj, cb) {
    let timeFromEpoch = moment.utc().unix();

    redis.get('CRICKETAPI_TOKEN_EXPIRE', function (err, data) {
        if (data && data > timeFromEpoch) {
            redis.get('CRICKETAPI_ACCESSTOKEN', function (err, token) {
                if (token) {
                    cb(null, token);
                } else {
                    getCricketApiAuthentication(err, function (authResponse) {
                        redis.set('CRICKETAPI_TOKEN_EXPIRE', authResponse.expires);
                        redis.set('CRICKETAPI_ACCESSTOKEN', authResponse.access_token);
                        cb(null, authResponse.access_token);
                    });
                }
            });
        } else {
            getCricketApiAuthentication({}, function (err, authResponse) {
                redis.set('CRICKETAPI_TOKEN_EXPIRE', authResponse.expires);
                redis.set('CRICKETAPI_ACCESSTOKEN', authResponse.access_token);
                cb(null, authResponse.access_token);
            });
        }
    });
}


module.exports = getCricketApiToken;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.key = 'indaus_2017_test_02';

    //requestObject.status = "ACTIVE";

    getCricketApiToken(requestObject, function (error, responseObject) {
        console.log("Response Code - ", error, responseObject);

    });
}