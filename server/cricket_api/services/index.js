var uploadSportsTourListService = require("./upload_sports_tour_list");
var uploadSportsTourPlayersListService = require("./upload_sports_tour_players_list");
var uploadSportsTourMatchListService = require("./upload_sports_tour_match_list");
var calculateMatchPlayersPointService = require("./calculate_match_players_point");

module.exports.uploadSportsTourListService =uploadSportsTourListService;
module.exports.uploadSportsTourPlayersListService =uploadSportsTourPlayersListService;
module.exports.uploadSportsTourMatchListService =uploadSportsTourMatchListService;
module.exports.calculateMatchPlayersPointService =calculateMatchPlayersPointService;
