var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourPlayersService = require('../../tour_players/services/index');
async = require("async");
var moment = require("moment");
var statsData = require('../cricketapi_data');
var cricketApiRequestService = require('../cricket_request/index');
var sportsTourService = require('../../tours/services/index');



function uploadSportsTourPlayersApi(requestObject, callback) {

    requestObject.status = ['ACTIVE','APPROVED'];
    var responseObject = new Object();

    sportsTourService.findTourList(requestObject, function (err, sportsTours) {
        if (err) {
            responseObject.responseCode = sportsTours.responseCode;
            responseObject.message = responseMessage[sportsTours.responseCode];
            callback(responseObject);
            return;
        } else {

            var teamArr = [];
            var tourList = sportsTours.responseData;
            for (var i = 0, length = tourList.length; i < length; i++) {
                var tourRow = tourList[i];
                logger.info('tourRow',tourRow);
                if(tourRow.teams){
                    Object.keys(tourRow.teams).forEach(function (key) {
                        var teamRow = tourRow.teams[key];
                        var teamObj = {};
                        if (teamRow.key && teamRow.key !== null) {
                            teamObj.teamKey = teamRow.key;
                            teamObj.seasonKey = tourRow.key;
                            teamObj.tourId = tourRow.tourId;
                            teamObj.name = teamRow.name;
                            teamObj.card_name = teamRow.card_name;
                            teamArr.push(teamObj);
                        }
                    });

                }

            }


            uploadSportsTourPlayersList(teamArr,requestObject.cricketAPIToken, function (error, data) {
                if (error) {
                    responseObject.responseCode = responseCode.SOME_INTERNAL_ERROR;
                } else {
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseObject.responseData = data.responseData;
                }

                logger.info("uploadSportsTourPlayersApi API :- Response - %j", responseObject);
                callback(null,responseObject);
            });
        }
    });
}



function uploadSportsTourPlayersList(requestObject,cricketAPIToken, responseCb) {

    logger.info('uploadSportsTourPlayersList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    async.eachSeries(requestObject,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            logger.info('uploadSportsTourPlayers',item);
            item.cricketAPIToken = cricketAPIToken;
           uploadSportsTourPlayers(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })
        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            logger.info('uploadSportsTourPlayersList final',err);
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsTourPlayers(teamObj, responseCb) {

    cricketApiRequestService.fetchSportsTourPlayersRequest({cricketAPIToken : teamObj.cricketAPIToken,teamKey : teamObj.teamKey,seasonKey :teamObj.seasonKey},function(err,teamPlayersResponse){
        if (err) {
            logger.error(err);
            responseCb(null);
            return;
        }

        let playersArr = [];

        Object.keys(teamPlayersResponse.players).forEach(function (key) {
            let playerObj = makeTeamPlayerData(teamPlayersResponse.players[key]);
            playerObj.tourId = teamObj.tourId;
            var team ={name : teamObj.name,card_name : teamObj.card_name,key : teamObj.teamKey};
            playerObj.team = team;
            playersArr.push(playerObj);
        });


        async.eachSeries(playersArr,
            // 2nd param is the function that each item is passed to
            function (playerRequestObj, cb) {
                logger.info('uploadSportsTourPlayerData',playerRequestObj);
                uploadSportsTourPlayerData(playerRequestObj, function (error, responsePlayerObject) {
                    cb(error, {});
                    return;
                })
            },
            // 3rd param is the function to call when everything's done
            function (err) {
                // All tasks are done now
                logger.info('uploadSportsTourPlayersList final done');
                responseCb(null, {});

            }
        );

    });

}

function uploadSportsTourPlayerData(playerObj, responseCb) {
    var tourPlayerRequestObject = new Object();
    tourPlayerRequestObject.key = playerObj.key;
    tourPlayerRequestObject.tourId = playerObj.tourId;
    sportsTourPlayersService.findTourPlayersByProperty(tourPlayerRequestObject, function (error, tourPlayerResponseObject) {
        if (error) {
            logger.info(error);
            callback(error);
            return;
        }
        logger.info('findTourPlayersByProperty',tourPlayerResponseObject);

        if (tourPlayerResponseObject.responseData && tourPlayerResponseObject.responseData.key) {
            //responseCb(null, {responseCode :responseCode.SUCCESS });
           // return;
            let updatePlayerObj = {};
            updatePlayerObj.playerId = tourPlayerResponseObject.responseData.playerId;
            updatePlayerObj.fullName = playerObj.fullName;
            updatePlayerObj.name = playerObj.name;
            logger.info('updatePlayerObj ',updatePlayerObj);
            sportsTourPlayersService.updateTourPlayers(updatePlayerObj, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });

        } else {
            playerObj.status = 'ACTIVE';
            logger.info('addTourPlayers',playerObj);
            sportsTourPlayersService.addTourPlayers(playerObj, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });

}


function makeTeamPlayerData(requestObject) {
    var playerObject = {};
    playerObject.key = requestObject.key;
    playerObject.name = requestObject.card_name;
    playerObject.shortName = requestObject.card_name;
    playerObject.fullName = requestObject.full_name;
    var identifyRoles = requestObject.identified_roles;

    let isRoleFound = true;
    if (typeof identifyRoles.keeper === "undefined" || identifyRoles.keeper === null){
        isRoleFound = false;
    }
    if (typeof identifyRoles.batsman === "undefined" || identifyRoles.batsman === null){
        isRoleFound = false;
    }
    if (typeof identifyRoles.bowler === "undefined" || identifyRoles.bowler === null){
        isRoleFound = false;
    }

    if (isRoleFound){
        if(identifyRoles.keeper){
            playerObject.keeper = true;
            playerObject.batsman = false;
            playerObject.bowler = false;
        }else{
            playerObject.keeper = identifyRoles.keeper;
            playerObject.batsman = identifyRoles.batsman;
            playerObject.bowler = identifyRoles.bowler;
        }

    }else{
        playerObject.keeper = false;
        playerObject.batsman = true;
        playerObject.bowler = true;
    }
    playerObject.ONE_DAY = true;
    playerObject.TEST = true;
    playerObject.T20 = true;

    playerObject.points = 0;
    playerObject.credit = 8;

    return playerObject;
}

 


module.exports = uploadSportsTourPlayersApi;