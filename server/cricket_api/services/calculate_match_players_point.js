var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var cricketApiRequest = require('../cricket_request/index');
var utility = require("../../utils/utility");
var MATCH_POINT = require("../../utils/match_points");

var cricketApiData = require('../cricketapi_data');
var tourMatchService = require("../../tour_matches/services/index");
var tourPlayersService = require("../../tour_players/services/index");


async = require("async");
var moment = require("moment");

function calculateMatchPlayersPoints(requestData, responseCb) {

    // console.log('uploadSportsTourList:', toursArr);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    let apiMatchData = requestData.apiMatchData;
    tourMatchService.findTourMatchesByProperty({matchId: requestData.matchId}, function (err, matchResponseObj) {
        var matchResponse = matchResponseObj.responseData;

        if (matchResponse) {

            if(!(matchResponse.status == 'UNDER_REVIEW' || matchResponse.status == 'RUNNING')){
                logger.info('calculateMatchPlayersPoints not running or under_review');
                responseCb(true, {responseCode: responseCode.MATCH_STATUS_UPDATED});
                return;
            }

            logger.info('calculateMatchPlayersPoints return post validated', matchResponse.matchPreviewStatus, matchResponse.status);
            if (matchResponse.matchPreviewStatus == 'post-match-validated' && matchResponse.status == 'UNDER_REVIEW') {
                logger.info('calculateMatchPlayersPoints return post validated');
                responseCb(err, {responseCode: responseCode.MATCH_REVIEW_STATUS_POST_VALIDATED});
                return;
            }


            if (matchResponse && matchResponse.players.length == 22) {

                var matchPlayers = matchResponse.players;
                updateTourMatchPlayersPoint(matchResponse, matchPlayers, apiMatchData, function (err, resp) {
                    if (err) {
                        responseCb(err, resp);
                        return;
                    }
                    //console.log('update tour match',matchPlayers);
                    var updateMatchObj = {};
                    updateMatchObj.matchId = matchResponse.matchId;
                    updateMatchObj.players = matchPlayers;

                    if (resp) {
                        if (resp.matchPreviewStatus) {
                            updateMatchObj.matchPreviewStatus = resp.matchPreviewStatus;
                        }
                        if (resp.matchStatus) {
                            updateMatchObj.status = resp.matchStatus;
                        }

                    }
                    let updatePointsRequest = {players: matchPlayers, matchId: matchResponse.matchId};

                    tourPlayersService.updateTourPlayerPointsService(updatePointsRequest, function () { });

                    tourMatchService.updateTourMatches(updateMatchObj, function (err, updateMatchResp) {
                        responseCb(err, updateMatchResp);
                    })

                })

            } else {
                updateTourMatchPlayingEleven(matchResponse, apiMatchData, function (err, resp) {
                    responseCb(err, resp);
                });
            }
        } else {
            responseCb(true, {responseCode: responseCode.MATCH_DOES_NOT_EXIST});
            return;
        }

    });

}

function updateTourMatchPlayingEleven(matchResponse, matchData, cb) {
    logger.info('updateTourMatchPlayingEleven', matchResponse);
    var playingXI = [];
    var matchPlayersArr = [];
    if (matchData.card) {
        var isContinue = false;
        if ((matchData.card.status == 'notstarted' || matchData.card.data_review_checkpoint == 'pre-match-review')) {
            cb(true, {responseCode: responseCode.MATCH_NOT_STARTED});
            return;
        }

        var teams = matchData.card.teams;

        if (teams.a.match) {
            var matchA = teams.a.match;
            if (matchA && matchA.playing_xi && matchA.playing_xi.length > 0) {
                playingXI.push(...matchA.playing_xi);
                playingXI.push(...teams.b.match.playing_xi);
            }
        }
    }

    logger.info('playing 11 team ',matchResponse.matchId,playingXI);
    if (playingXI.length > 0) {
        tourPlayersService.findTourPlayersList({
            tourId: matchResponse.tourId
        }, function (err, tourPlayers) {
            logger.info('updateTourMatchPlayingEleven tourPlayers', tourPlayers);

            for (var i = 0, length = tourPlayers.responseData.length; i < length; i++) {
                var currentRow = tourPlayers.responseData[i];
                var currentObj = {};
                currentObj.playerId = currentRow.playerId;
                currentObj.name = currentRow.name;
                currentObj.key = currentRow.key;
                currentObj.keeper = currentRow.keeper;
                currentObj.batsman = currentRow.batsman;
                currentObj.bowler = currentRow.bowler;
                currentObj.isSecondStart = false;
                currentObj.totalPoints = 0;

                if (matchResponse.formatType === 'TEST') {
                    currentObj.firstInnings = getDefaultInningsData();
                    currentObj.secondInnings = getDefaultInningsData();
                } else {
                    currentObj.firstInnings = getDefaultInningsData();
                }

                if (playingXI.indexOf(currentRow.key) > -1) {
                    currentObj.isPlayingEleven = true;
                    currentObj.startingPoint = MATCH_POINT.startingPoint;
                    matchPlayersArr.push(currentObj); //need to check if all players added or not
                } else {
                    currentObj.isPlayingEleven = false;
                    currentObj.startingPoint = 0;
                }

            }

            tourMatchService.updateTourMatches({
                matchId: matchResponse.matchId,
                players: matchPlayersArr
            }, function (err, resp) {
                cb(null, resp);
            })


        });
    } else {
        cb(null, {responseCode: responseCode.PLAYING11_NOT_DECIDE})
    }


}


function getDefaultInningsData() {
    var inningsData = {};
    inningsData.currentPoints = 0;
    inningsData.fours = 0;
    inningsData.sixes = 0;
    inningsData.runs = 0;
    inningsData.ballFaced = 0;
    inningsData.batsmanDots = 0;
    inningsData.stRate = 0;
    inningsData.dismissed = false;
    inningsData.bowlerBalls = 0;
    inningsData.bowlerRuns = 0;
    inningsData.overs = 0;
    inningsData.wickets = 0;
    inningsData.maidenOvers = 0;
    inningsData.bowlerDots = 0;
    inningsData.ecRate = 0;
    inningsData.extras = 0;
    inningsData.catches = 0;
    inningsData.runOuts = 0;
    inningsData.stumbeds = 0;
    inningsData.pointBreakup = {
        runs: 0,
        fours: 0,
        sixes: 0,
        st_rate: 0,
        fifties_hundred: 0,
        wickets: 0,
        maidens: 0,
        ec_rate: 0,
        bonus_pos: 0,
        bonus_neg: 0,
        catch: 0,
        runout: 0
    };


    return inningsData;
}

function updateTourMatchPlayersPoint(matchResponse, matchPlayers, matchData, cb) {
    var playingXI = [];
    var matchPlayersArr = [];

    if (matchData.card) {

        var cardData = matchData.card;

        if ((cardData.status == 'notstarted' || cardData.data_review_checkpoint == 'pre-match-review')) {
            cb(true, {responseCode: responseCode.MATCH_NOT_STARTED});
            return;
        }

        var apiPlayers = cardData.players;
        let nowData = matchData.card.now;
        var currentInnings = 1;
        let nowDataBalls = 0;
        if(nowData && nowData.balls){
            nowDataBalls = parseInt(nowData.balls);
            if (nowData.innings == '2' && nowDataBalls > 0) {
                currentInnings = 2;
            }
        }


        for (var i = 0, length = matchPlayers.length; i < length; i++) {
            var matchPlayer = matchPlayers[i];
            if (matchPlayer.isPlayingEleven) {
                var apiPlayer = apiPlayers[matchPlayer.key];
                getPlayerPoints(matchResponse.formatType, currentInnings, apiPlayer, matchPlayer);
                // console.log('updateTourMatchPlayersPoint matchPlayer',matchPlayer);

            }
        }
        var respObj = {};
        if ((cardData.status == 'completed')) {
            if (cardData.status_overview == 'result') {
                respObj.matchStatus = 'UNDER_REVIEW';
                respObj.matchPreviewStatus = cardData.data_review_checkpoint;
            } else if (cardData.status_overview == 'abandoned') {
                respObj.matchStatus = 'ABANDONED';
                respObj.matchPreviewStatus = cardData.data_review_checkpoint;
            } else {
                respObj.matchStatus = 'UNDER_REVIEW';
                respObj.matchPreviewStatus = cardData.data_review_checkpoint;
            }

        }
        cb(null, respObj);
    } else {
        cb(true);
    }

}


function getPlayerPoints(formatType, currentInnings, apiPlayer, matchPlayer) {
    matchPlayer.totalPoints = matchPlayer.startingPoint;

    var innings = apiPlayer.match.innings;
    if (innings) {
        if (currentInnings == '1') {
            var firstInning = innings[1];
            var playerFirstInnings = matchPlayer.firstInnings;
            getInningsData(formatType, firstInning, playerFirstInnings, matchPlayer);
            matchPlayer.totalPoints += matchPlayer.firstInnings.currentPoints;

        } else if (currentInnings == '2' && matchPlayer.isSecondStart) {
            var secondInning = innings['2'];
            var playerSecondInnings = matchPlayer.secondInnings;
            getInningsData(formatType, secondInning, playerSecondInnings, matchPlayer);
            matchPlayer.totalPoints += matchPlayer.firstInnings.currentPoints + playerSecondInnings.currentPoints;

        } else if (currentInnings == '2' && matchPlayer.isSecondStart == false) {
            matchPlayer.isSecondStart = true;
            var firstInning = innings[1];
            var playerFirstInnings = matchPlayer.firstInnings;
            getInningsData(formatType, firstInning, playerFirstInnings, matchPlayer);
            matchPlayer.totalPoints += matchPlayer.firstInnings.currentPoints;

        }
    }


}

function getInningsData(formatType, apiInnings, playerInnings, matchPlayer) {
    var currentPoints = 0;
    playerInnings.fours = apiInnings.batting.fours || 0;
    playerInnings.sixes = apiInnings.batting.sixes || 0;
    playerInnings.runs = apiInnings.batting.runs || 0;
    playerInnings.ballFaced = apiInnings.batting.balls || 0;
    playerInnings.batsmanDots = apiInnings.batting.dots || 0;
    playerInnings.stRate = apiInnings.batting.strike_rate || 0;
    playerInnings.dismissed = apiInnings.batting.dismissed || false;
    playerInnings.bowlerBalls = apiInnings.bowling.balls || 0;
    playerInnings.bowlerRuns = apiInnings.bowling.runs || 0;
    playerInnings.overs = apiInnings.bowling.overs || 0;
    playerInnings.ecRate = apiInnings.bowling.economy || 0;
    playerInnings.wickets = apiInnings.bowling.wickets || 0;
    playerInnings.maidenOvers = apiInnings.bowling.maiden_overs || 0;
    playerInnings.bowlerDots = apiInnings.bowling.dots || 0;
    playerInnings.extras = apiInnings.bowling.extras || 0;
    playerInnings.catches = apiInnings.fielding.catches || 0;
    playerInnings.runOuts = apiInnings.fielding.runouts || 0;
    playerInnings.stumbeds = apiInnings.fielding.stumbeds || 0;

    playerInnings.currentPoints = 0;

    var rollType = getPlayerRole(matchPlayer);

    var pointBreakup = playerInnings.pointBreakup;

    pointBreakup.runs = 0;
    pointBreakup.fours = 0;
    pointBreakup.sixes = 0;
    pointBreakup.st_rate = 0;
    pointBreakup.fifties_hundred = 0;
    pointBreakup.wickets = 0;
    pointBreakup.maidens = 0;
    pointBreakup.ec_rate = 0;
    pointBreakup.bonus_pos = 0;
    pointBreakup.bonus_neg = 0;
    pointBreakup.catch = 0;
    pointBreakup.runout = 0;

    if (formatType == 'TEST') {

        let TEST_POINT = MATCH_POINT.TEST;
        pointBreakup.runs = playerInnings.runs * TEST_POINT.RUNS;
        pointBreakup.fours = playerInnings.fours * TEST_POINT.FOURS;
        pointBreakup.sixes = playerInnings.sixes * TEST_POINT.SIXES;
        if (playerInnings.runs >= 100) {
            pointBreakup.fifties_hundred = TEST_POINT.HUNDRED;
        } else if (playerInnings.runs >= 50) {
            pointBreakup.fifties_hundred = TEST_POINT.FIFTY;
        }

        pointBreakup.wickets = playerInnings.wickets * TEST_POINT.WICKETS;
        pointBreakup.catch = playerInnings.catches * TEST_POINT.CATCHES;

        pointBreakup.runout = playerInnings.runOuts * TEST_POINT.RUNOUT;
        pointBreakup.runout += playerInnings.stumbeds * TEST_POINT.STUMBEDS;

        if (playerInnings.dismissed && playerInnings.runs == 0 && rollType !== 'BOWLER') {
            logger.info('BOWLER', matchPlayer.name);
            pointBreakup.bonus_neg = TEST_POINT.DUCK;
        }
        if (playerInnings.wickets >= 5) {
            pointBreakup.bonus_pos = TEST_POINT.FIVE_WICKET;
        } else if (playerInnings.wickets >= 4) {
            pointBreakup.bonus_pos = TEST_POINT.FOUR_WICKET;
        }

    }
    else if (formatType == 'T20') {
        let T20_POINT = MATCH_POINT.T20;

        pointBreakup.runs = playerInnings.runs * T20_POINT.RUNS;
        pointBreakup.fours = playerInnings.fours * T20_POINT.FOURS;
        pointBreakup.sixes = playerInnings.sixes * T20_POINT.SIXES;
        if (playerInnings.runs >= 100) {
            pointBreakup.fifties_hundred = T20_POINT.HUNDRED;
        } else if (playerInnings.runs >= 50) {
            pointBreakup.fifties_hundred = T20_POINT.FIFTY;
        }

        pointBreakup.wickets = playerInnings.wickets * T20_POINT.WICKETS;
        pointBreakup.maidens = playerInnings.maidenOvers * T20_POINT.MAIDEN_OVERS;
        pointBreakup.catch = playerInnings.catches * T20_POINT.CATCHES;

        pointBreakup.runout = playerInnings.runOuts * T20_POINT.RUNOUT;
        pointBreakup.runout += playerInnings.stumbeds * T20_POINT.STUMBEDS;

        if (playerInnings.dismissed && playerInnings.runs == 0 && rollType !== 'BOWLER') {
            pointBreakup.bonus_neg += T20_POINT.DUCK;
        }
        if (playerInnings.wickets >= 5) {
            pointBreakup.bonus_pos = T20_POINT.FIVE_WICKET;
        } else if (playerInnings.wickets >= 4) {
            pointBreakup.bonus_pos = T20_POINT.FOUR_WICKET;
        }

        if (rollType !== 'BOWLER' && playerInnings.ballFaced >= 10) {
            if (playerInnings.stRate >= 60 && playerInnings.stRate <= 70) {
                pointBreakup.st_rate += -1;
            } else if (playerInnings.stRate >= 50 && playerInnings.stRate < 59.9) {
                pointBreakup.st_rate += -2;
            } else if (playerInnings.stRate < 50) {
                pointBreakup.st_rate += -3;
            }
        }

        if (playerInnings.overs >= 2) {
            if (playerInnings.ecRate > 11) {
                pointBreakup.ec_rate += -3;
            } else if (playerInnings.ecRate >= 10.1 && playerInnings.ecRate <= 11) {
                pointBreakup.ec_rate += -2;
            } else if (playerInnings.ecRate >= 9 && playerInnings.ecRate <= 10) {
                pointBreakup.ec_rate += -1;
            } else if (playerInnings.ecRate >= 5 && playerInnings.ecRate <= 6) {
                pointBreakup.ec_rate += 1;
            } else if (playerInnings.ecRate >= 4 && playerInnings.ecRate <= 4.99) {
                pointBreakup.ec_rate += 2;
            } else if (playerInnings.ecRate < 4) {
                pointBreakup.ec_rate += 3;
            }
        }

    }
    else if (formatType == 'ONE_DAY') {
        let ONE_DAY_POINT = MATCH_POINT.ONE_DAY;

        pointBreakup.runs = playerInnings.runs * ONE_DAY_POINT.RUNS;
        pointBreakup.fours = playerInnings.fours * ONE_DAY_POINT.FOURS;
        pointBreakup.sixes = playerInnings.sixes * ONE_DAY_POINT.SIXES;
        if (playerInnings.runs >= 100) {
            pointBreakup.fifties_hundred = ONE_DAY_POINT.HUNDRED;
        } else if (playerInnings.runs >= 50) {
            pointBreakup.fifties_hundred = ONE_DAY_POINT.FIFTY;
        }

        pointBreakup.wickets = playerInnings.wickets * ONE_DAY_POINT.WICKETS;
        pointBreakup.maidens = playerInnings.maidenOvers * ONE_DAY_POINT.MAIDEN_OVERS;
        pointBreakup.catch = playerInnings.catches * ONE_DAY_POINT.CATCHES;
        pointBreakup.runout = playerInnings.runOuts * ONE_DAY_POINT.RUNOUT;
        pointBreakup.runout += playerInnings.stumbeds * ONE_DAY_POINT.STUMBEDS;

        if (playerInnings.dismissed && playerInnings.runs == 0 && rollType !== 'BOWLER') {
            pointBreakup.bonus_neg += ONE_DAY_POINT.DUCK;
        }
        if (playerInnings.wickets >= 5) {
            pointBreakup.bonus_pos = ONE_DAY_POINT.FIVE_WICKET;
        } else if (playerInnings.wickets >= 4) {
            pointBreakup.bonus_pos = ONE_DAY_POINT.FOUR_WICKET;
        }

        if (rollType !== 'BOWLER' && playerInnings.ballFaced >= 20) {
            if (playerInnings.stRate >= 50 && playerInnings.stRate <= 60) {
                pointBreakup.st_rate += -1;
            } else if (playerInnings.stRate >= 40 && playerInnings.stRate < 49.9) {
                pointBreakup.st_rate += -2;
            } else if (playerInnings.stRate < 40) {
                pointBreakup.st_rate += -3;
            }
        }

        if (playerInnings.overs >= 5) {
            if (playerInnings.ecRate > 9) {
                pointBreakup.ec_rate += -3;
            } else if (playerInnings.ecRate >= 8.1 && playerInnings.ecRate <= 9) {
                pointBreakup.ec_rate += -2;
            } else if (playerInnings.ecRate >= 7 && playerInnings.ecRate <= 8) {
                pointBreakup.ec_rate += -1;
            } else if (playerInnings.ecRate >= 3.5 && playerInnings.ecRate <= 4.5) {
                pointBreakup.ec_rate += 1;
            } else if (playerInnings.ecRate >= 2.5 && playerInnings.ecRate <= 3.49) {
                pointBreakup.ec_rate += 2;
            } else if (playerInnings.ecRate < 2.5) {
                pointBreakup.ec_rate += 3;
            }
        }


    }

    var currentPoints = pointBreakup.runs + pointBreakup.fours + pointBreakup.sixes + pointBreakup.st_rate +
        pointBreakup.fifties_hundred + pointBreakup.wickets + pointBreakup.maidens + pointBreakup.ec_rate +
        pointBreakup.bonus_pos + pointBreakup.bonus_neg + pointBreakup.catch + pointBreakup.runout;
    playerInnings.currentPoints = currentPoints;
}
function getPlayerRole(matchPlayer) {
    var rollType = 'BATSMAN;'
    if (matchPlayer.bowler && matchPlayer.batsman) {
        rollType = 'ALL_ROUNDER';
    } else if (matchPlayer.keeper) {
        rollType = 'WEEKET_KEEPER';
    } else if (matchPlayer.batsman) {
        rollType = 'BATSMAN';
    } else if (matchPlayer.bowler) {
        rollType = 'BOWLER';
    }
    return rollType;
}

module.exports = calculateMatchPlayersPoints;

// Unit Test Case
if (require.main === module) {
    (function () {

        var requestObj = {matchId: 567};
        calculateMatchPlayersPoints(requestObj, function (err, playerPoints) {
            if (err) {
                throw err;
            }
            // console.log('calculateMatchPlayersPoints:', playerPoints);
        });
    })();
}