var logger = require("../../utils/logger").gameLogs;
var constant = require("../../utils/constant");

var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourService = require('../../tours/services/index');
var tourMatchesService = require('../../tour_matches/services/index');
async = require("async");
var moment = require("moment");
var cricketApiData = require('../cricketapi_data');

function uploadSportsTourMatchList(requestObject, responseCb) {

    logger.info('uploadSportsTourMatchList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    let matchesArr = [];

        let evenTypeMatchesObj = requestObject.seasonData.matches;
        Object.keys(evenTypeMatchesObj).forEach(function (key) {
            let matchRow = evenTypeMatchesObj[key];
            let currentMatchObj = {};

            var formatType = cricketApiData.getEventFormatType(matchRow.format);
            currentMatchObj.tourId = requestObject.tourId;
            currentMatchObj.name = matchRow.name;
            currentMatchObj.relatedName = matchRow.related_name;
            currentMatchObj.shortName = matchRow.short_name;
            currentMatchObj.venue = matchRow.venue.name;
            currentMatchObj.key = matchRow.key;
            currentMatchObj.formatType = formatType;
            currentMatchObj.teams = matchRow.teams;
            currentMatchObj.title = matchRow.title;
            currentMatchObj.status = matchRow.status;
            currentMatchObj.startDate = moment(matchRow.start_date.iso).subtract(constant.MATCH_FREEZE_TIME, 'minutes');;
            matchesArr.push(currentMatchObj);

        })


    async.eachSeries(matchesArr,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            logger.info('uploadSportsTourMatch', item);
            uploadSportsTourMatch(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            logger.info('uploadSportsTourMatchList final done');

            let tourReqObj = {};
            tourReqObj.teams = requestObject.seasonData.teams;
            tourReqObj.tourId = requestObject.tourId;
            sportsTourService.updateTour(tourReqObj,function(err,tourResponse){

                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
            });

        }
    );
}

function uploadSportsTourMatch(requestObject, responseCb) {
    var tourMatchRequestObject = new Object();
    tourMatchRequestObject.key = requestObject.key;

    tourMatchesService.findTourMatchesByProperty(tourMatchRequestObject, function (error, tourMatchResponseObject) {
        if (error) {
            logger.info(error);
            responseCb(error);
            return;
        }
        logger.info('findTourMatchesByProperty', tourMatchResponseObject);
        var tourMatchObject = makeTourMatchRequestData(requestObject);

        if (tourMatchResponseObject.responseData && tourMatchResponseObject.responseData.key) {
            tourMatchObject.matchId = tourMatchResponseObject.responseData.matchId;
            tourMatchesService.updateTourMatches(tourMatchObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        } else {
            logger.info('addTourMatches', tourMatchObject);

            if(requestObject.status == 'started'){
                tourMatchObject.status = 'RUNNING';
            }else if(requestObject.status == 'completed'){
                tourMatchObject.status = 'CLOSED';
            }else if(requestObject.status == 'notstarted'){
                let currentTime = new Date();
                if (tourMatchObject.startTime.isBefore(currentTime)){
                    tourMatchObject.status = 'RUNNING';
                }else{
                    tourMatchObject.status = 'ACTIVE';
                }
            }

            tourMatchObject.shortName = requestObject.shortName;
            tourMatchObject.relatedName = requestObject.relatedName;
            tourMatchesService.addTourMatches(tourMatchObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });


}

function makeTourMatchRequestData(requestObject) {
    var tourMatchObject = {};
    tourMatchObject.tourId = requestObject.tourId;
    tourMatchObject.name = requestObject.name;
    tourMatchObject.venue = requestObject.venue;
    tourMatchObject.key = requestObject.key;
    tourMatchObject.formatType = requestObject.formatType;
    tourMatchObject.teams = requestObject.teams;
    tourMatchObject.title = requestObject.title;
    tourMatchObject.startTime = requestObject.startDate;

    return tourMatchObject;
}

module.exports = uploadSportsTourMatchList;