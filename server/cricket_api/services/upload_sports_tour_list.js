var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourService = require('../../tours/services/index');
var utility = require("../../utils/utility");

var async = require("async");
var moment = require("moment");

function uploadSportsTourList(toursArr, responseCb) {

    logger.info('uploadSportsTourList:', toursArr);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items

    async.eachSeries(toursArr,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            logger.info('uploadSportsTour',item);
            if(item && item.key){
                uploadSportsTour(item, function (error, responseObject) {
                    callback(error, responseObject);
                    return;
                })
            }else{
                callback(true, item);
                return; 
            }
        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            logger.info('uploadSportsTourList final done',err);
            if(err){
                responseObject.responseCode = responseCode.SOME_INTERNAL_ERROR;
            }else{
                responseObject.responseCode = responseCode.SUCCESS;
            }
            responseCb(null, responseObject);
        }
    );
}

function uploadSportsTour(requestObject, responseCb) {
    var sportsTourRequestObject = new Object();
    sportsTourRequestObject.key = requestObject.key;
    sportsTourService.findTourByProperty(sportsTourRequestObject, function (error, sportsTourResponseObject) {
        if (error) {
            logger.error(error);
            callback(error);
            return;
        }
        logger.info('findTourByProperty',sportsTourResponseObject);
        var sportsTourObject = makeSportsTourRequestData(requestObject);

        if (sportsTourResponseObject.responseData && sportsTourResponseObject.responseData.key) {
            sportsTourObject.tourId = sportsTourResponseObject.responseData.tourId;
            sportsTourService.updateTour(sportsTourObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        } else {
            logger.info('addTour',sportsTourObject);
            sportsTourObject.shortName = requestObject.short_name;
            sportsTourObject.status = 'PENDING';
            sportsTourService.addTour(sportsTourObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });

}

function makeSportsTourRequestData(requestObject) {
    var sportsTourObject = {};
    sportsTourObject.tourName = requestObject.name;
    sportsTourObject.key = requestObject.key;
    sportsTourObject.venue = requestObject.venue;
    sportsTourObject.series = requestObject.series;
    if(requestObject.start_date){
        sportsTourObject.startDate =utility.getDateFromTimestampInSeconds(requestObject.start_date.timestamp);

    }
    return sportsTourObject;
}

module.exports = uploadSportsTourList;