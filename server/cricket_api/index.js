/**
 * Created by sumit on 2/15/2017.
 */
var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');
 
//console.log("middleware:",middleware.getAdminIdFromToken);

route.post("/cricketapi/uploadSportsTours",middleware.auditTrailLog,middleware.getAdminIdFromToken,middleware.validateCricketApiToken, obj.uploadSportsToursApi);
route.post("/cricketapi/uploadSportsTourPlayers",middleware.auditTrailLog,middleware.getAdminIdFromToken,middleware.validateCricketApiToken, obj.uploadSportsTourPlayersApi);
route.post("/cricketapi/uploadSportsTourMatches",middleware.auditTrailLog,middleware.getAdminIdFromToken,middleware.validateCricketApiToken, obj.uploadSportsTourMatchesApi);
route.post("/cricketapi/uploadSportsMatchPoints",middleware.auditTrailLog,middleware.getAdminIdFromToken,middleware.validateCricketApiToken, obj.uploadMatchPlayerPointsApi);

module.exports = route;
