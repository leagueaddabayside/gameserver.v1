var tourMaster = require("../models/tour_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function findTourByProperty(requestObject, callback) {
	var query = tourMaster.findOne({});
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.key !== "undefined" && requestObject.key !== null)
		query.where("key").equals(requestObject.key);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
				currentObj.tourId = currentRow.tourId;
				currentObj.tourName = currentRow.tourName;
				currentObj.shortName = currentRow.shortName;
				currentObj.key = currentRow.key;
				currentObj.venue = currentRow.venue;
				currentObj.teams = currentRow.teams;
				if (typeof currentRow.startDate !== "undefined" && currentRow.startDate !== null)
					currentObj.startDate = moment(currentRow.startDate).unix();

				if (typeof currentRow.activeDate !== "undefined" && currentRow.activeDate !== null)
					currentObj.activeDate = moment(currentRow.activeDate).unix();
				currentObj.status = currentRow.status;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findTourByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tourId = "tourId";
	//requestObject.key = "key";

	findTourByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}