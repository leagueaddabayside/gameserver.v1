var addTourService = require("./add_tour");
var findTourByPropertyService = require("./find_tour_by_property");
var findTourListService = require("./find_tour_list");
var updateTourService = require("./update_tour");
var updateTourDisplayOrderService = require("./update_tour_display_order");
// Require

module.exports.addTour = addTourService;
module.exports.findTourByProperty = findTourByPropertyService;
module.exports.findTourList = findTourListService;
module.exports.updateTour = updateTourService;
module.exports.updateTourDisplayOrder = updateTourDisplayOrderService
// Export
