var tourMaster = require("../models/tour_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addTour(requestObject, callback) {
	var newTour = new tourMaster({
		tourName : requestObject.tourName,
		shortName : requestObject.shortName,
		key : requestObject.key,
		venue : requestObject.venue,
		series : requestObject.series,
		formatType : requestObject.formatType,
		startDate : requestObject.startDate,
		endDate : requestObject.endDate,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newTour.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addTour;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourName = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.venue = "";
	requestObject.startDate = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addTour(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}