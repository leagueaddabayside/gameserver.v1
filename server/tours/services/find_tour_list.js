var tourMaster = require("../models/tour_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function findTourList(requestObject, callback) {
	var query = tourMaster.find({});

	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").in(requestObject.status);

	query.sort("startDate");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var tourArr = data;
			var tourList = [];
			for (var i=0, length=tourArr.length; i<length; i++) {
				var currentRow = tourArr[i];
				var currentObj = {};
				currentObj.tourId = currentRow.tourId;
				currentObj.tourName = currentRow.tourName;
				currentObj.shortName = currentRow.shortName;
				currentObj.key = currentRow.key;
				currentObj.venue = currentRow.venue;
				currentObj.teams = currentRow.teams;
				if (typeof currentRow.startDate !== "undefined" && currentRow.startDate !== null)
				currentObj.startDate = moment(currentRow.startDate).unix();

				if (typeof currentRow.activeDate !== "undefined" && currentRow.activeDate !== null)
					currentObj.activeDate =moment(currentRow.activeDate).unix();
				currentObj.status = currentRow.status;
				tourList.push(currentObj);
			}
			responseObject.responseData = tourList;
		callback(null, responseObject);
	});
}

module.exports = findTourList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findTourList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}