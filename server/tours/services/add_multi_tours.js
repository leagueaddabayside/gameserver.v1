var tourMaster = require("../models/tour_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addMultiTours(requestObject, callback) {

	var responseObject = new Object();

	tourMaster.create(requestObject, function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});


}

module.exports = addMultiTours;

// Unit Test Case
if (require.main === module) {
	var requestObject = [{tourName: "Aus T20 Bash", shortName: 'Aus T20 Bash', key: 'aus-t20', venue: 'Austrailia', startDate: '', status: 'ACTIVE'},
		                 {tourName: "India Vs England T20", shortName: 'AIndia Vs England T20', key: 'eng-t20', venue: 'India', startDate: '', status: 'ACTIVE'},
		                 {tourName: "NZ Vs Ban Test", shortName: 'NZ Vs Ban Test', key: 'nz-test', venue: 'Bangladesh', startDate: '', status: 'ACTIVE'} ];

	console.log(requestObject);

	addMultiTours(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);

	});
}