var tourMaster = require("../models/tour_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function updateTour(requestObject, callback) {
	var updateObject = new Object();
	if(typeof requestObject.startDate !== 'undefined' && requestObject.startDate !== null)
		updateObject.startDate = requestObject.startDate;
	if(typeof requestObject.shortName !== 'undefined' && requestObject.shortName !== null)
		updateObject.shortName = requestObject.shortName;
	if(typeof requestObject.venue !== 'undefined' && requestObject.venue !== null)
		updateObject.venue = requestObject.venue;

	if(typeof requestObject.series !== 'undefined' && requestObject.series !== null)
		updateObject.series = requestObject.series;

	if(typeof requestObject.formatType !== 'undefined' && requestObject.formatType !== null)
		updateObject.formatType = requestObject.formatType;

	if(typeof requestObject.activeDate !== 'undefined' && requestObject.activeDate !== null){
		updateObject.activeDate = moment(requestObject.activeDate).toDate();
	}
	if(typeof requestObject.teams !== 'undefined' && requestObject.teams !== null){
		updateObject.teams = requestObject.teams;
	}

	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {tourId: requestObject.tourId};
	tourMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateTour;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourId = 2;
	requestObject.activeDate = "2017-01-30";
	/*requestObject.shortName = "";
	requestObject.venue = "";
	requestObject.status = "";*/
	console.log(requestObject);

	updateTour(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}