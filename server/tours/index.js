var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addTour",middleware.getAdminIdFromToken, obj.addTour);
route.post("/updateTour",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.updateTour);
route.post("/findTourByProperty",middleware.getAdminIdFromToken, obj.findTourByProperty);
route.post("/findTourList",middleware.getAdminIdFromToken, obj.findTourList);
route.post("/fetchTours", obj.fetchTourList);
route.post("/updateTourDisplayOrder",middleware.getAdminIdFromToken, obj.updateTourDisplayOrder);
// Routes
module.exports = route;
