require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var TourMasterSchema = new mongoose.Schema({
	tourId: {type: Number, unique: true},
	tourName: {type: String, unique: true},
	shortName: {type: String},
	key: {type: String},
	venue: {type: String},
	series: {type: String},
	teams: {type: {}},
	startDate: {type: Date},
	endDate: {type: Date},
	formatType: {type: String, enum: ['T20', 'TEST', 'ONE_DAY','WARM_UP_50_OVER','LIMITED_OVERS','WARM_UP_T20']},
	status: {type: String, enum: ['PENDING', 'APPROVED', 'REJECTED', 'ACTIVE', 'CLOSED','HIDE']},
	displayOrder: {type: Number},
	activeDate: {type: Date},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("TourMaster", TourMasterSchema, "tour_master");

TourMasterSchema.plugin(autoIncrement.plugin, {
	model: "TourMaster",
	field: "tourId",
	startAt: 1,
	incrementBy: 1
});