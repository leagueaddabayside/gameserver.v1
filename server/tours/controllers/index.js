var addTourApi = require("./add_tour");
var findTourByPropertyApi = require("./find_tour_by_property");
var findTourListApi = require("./find_tour_list");
var fetchTourListApi = require("./fetch_tour_list");
var updateTourApi = require("./update_tour");
var updateTourDisplayOrderApi = require("./update_tour_display_order");
// Require

module.exports.addTour = addTourApi;
module.exports.findTourByProperty = findTourByPropertyApi;
module.exports.findTourList = findTourListApi;
module.exports.fetchTourList = fetchTourListApi;
module.exports.updateTour = updateTourApi;
module.exports.updateTourDisplayOrder = updateTourDisplayOrderApi
// Export
