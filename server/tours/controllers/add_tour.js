var logger = require("../../utils/logger").gameLogs;
var tourService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addTour(request, response, next) {
	var requestObject = request.body;
	//console.log("addTour API :- Request - %j", requestObject);

	var responseObject = new Object();
	tourService.addTour(requestObject, function(error, data) {
	if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("addTour API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addTour;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.tourName = "";
		requestObject.shortName = "";
		requestObject.key = "";
		requestObject.venue = "";
		requestObject.startDate = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addTour(request, response);
	})();
}