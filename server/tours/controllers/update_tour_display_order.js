var logger = require("../../utils/logger").gameLogs;
var tourService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateTourDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateTourDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	tourService.updateTourDisplayOrder(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("updateTourDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateTourDisplayOrder;