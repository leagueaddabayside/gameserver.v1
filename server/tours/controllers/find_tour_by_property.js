var logger = require("../../utils/logger").gameLogs;
var tourService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findTourByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findTourByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	tourService.findTourByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("findTourByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findTourByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.tourId = "tourId";
		//requestObject.tourName = "tourName";
		//requestObject.shortName = "shortName";
		//requestObject.key = "key";
		//requestObject.venue = "venue";
		//requestObject.startDate = "startDate";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findTourByProperty(request, response);
	})();
}