var logger = require("../../utils/logger").gameLogs;
var tourService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findTourList(request, response, next) {
	var requestObject = request.body;
	//console.log("findTourList API :- Request - %j", requestObject);

	var responseObject = new Object();
	tourService.findTourList(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;			responseObject.respData = data.responseData;
		}

		//logger.info("findTourList API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findTourList;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findTourList(request, response);
	})();
}