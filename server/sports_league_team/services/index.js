var addSportsLeagueTeamService = require("./add_sports_league_team");
var findSportsLeagueTeamByPropertyService = require("./find_sports_league_team_by_property");
var findSportsLeagueTeamListService = require("./find_sports_league_team_list");
var updateSportsLeagueTeamService = require("./update_sports_league_team");
var updateSportsLeagueTeamDisplayOrderService = require("./update_sports_league_team_display_order");
// Require

module.exports.addSportsLeagueTeam = addSportsLeagueTeamService;
module.exports.findSportsLeagueTeamByProperty = findSportsLeagueTeamByPropertyService;
module.exports.findSportsLeagueTeamList = findSportsLeagueTeamListService;
module.exports.updateSportsLeagueTeam = updateSportsLeagueTeamService;
module.exports.updateSportsLeagueTeamDisplayOrder = updateSportsLeagueTeamDisplayOrderService
// Export
