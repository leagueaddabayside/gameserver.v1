var sportsLeagueTeamMaster = require("../models/sports_league_team");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findSportsLeagueTeamByProperty(requestObject, callback) {
	var query = sportsLeagueTeamMaster.findOne({});
	if (typeof requestObject.teamId !== "undefined" && requestObject.teamId !== null)
		query.where("teamId").equals(requestObject.teamId);
	if (typeof requestObject.key !== "undefined" && requestObject.key !== null)
		query.where("key").equals(requestObject.key);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.teamId = currentRow.teamId;
			currentObj.location = currentRow.location;
			currentObj.shortName = currentRow.shortName;
			currentObj.key = currentRow.key;
			currentObj.country = currentRow.country;
			currentObj.status = currentRow.status;
			currentObj.createBy = currentRow.createBy;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findSportsLeagueTeamByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.teamId = "teamId";
	//requestObject.key = "key";

	findSportsLeagueTeamByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}