var sportsLeagueTeamMaster = require("../models/sports_league_team");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addSportsLeagueTeam(requestObject, callback) {
	var newSportsLeagueTeam = new sportsLeagueTeamMaster({
		location : requestObject.location,
		shortName : requestObject.shortName,
		key : requestObject.key,
		country : requestObject.country,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newSportsLeagueTeam.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addSportsLeagueTeam;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.location = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.country = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addSportsLeagueTeam(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}