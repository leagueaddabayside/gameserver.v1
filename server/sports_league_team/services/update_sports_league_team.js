var sportsLeagueTeamMaster = require("../models/sports_league_team");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateSportsLeagueTeam(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.location !== 'undefined' && requestObject.location !== null)
		updateObject.location = requestObject.location;
	if(requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {teamId: requestObject.teamId};
	sportsLeagueTeamMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateSportsLeagueTeam;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.location = "";
	requestObject.status = "";
	console.log(requestObject);

	updateSportsLeagueTeam(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}