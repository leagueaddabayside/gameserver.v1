var sportsLeagueTeamMaster = require("../models/sports_league_team");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var async = require("async");

function updateSportsLeagueTeamDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.sportsLeagueTeamList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {teamId : currentRow.teamId};
		sportsLeagueTeamMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateSportsLeagueTeamDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var sportsLeagueTeamList = [ {
		teamId : 1,
		displayOrder : 1
	}, {
		teamId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.sportsLeagueTeamList = sportsLeagueTeamList;
	console.log(requestObject);

	updateSportsLeagueTeam(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}