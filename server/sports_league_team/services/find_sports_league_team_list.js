var sportsLeagueTeamMaster = require("../models/sports_league_team");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findSportsLeagueTeamList(requestObject, callback) {
	var query = sportsLeagueTeamMaster.find({});
	query.sort("displayOrder");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var sportsLeagueTeamArr = data;
			var sportsLeagueTeamList = [];
			for (var i=0, length=sportsLeagueTeamArr.length; i<length; i++) {
				var currentRow = sportsLeagueTeamArr[i];
				var currentObj = {};
				currentObj.teamId = currentRow.teamId;
				currentObj.location = currentRow.location;
				currentObj.shortName = currentRow.shortName;
				currentObj.key = currentRow.key;
				currentObj.country = currentRow.country;
				currentObj.status = currentRow.status;
				currentObj.createBy = currentRow.createBy;
				sportsLeagueTeamList.push(currentObj);
			}
			responseObject.responseData = sportsLeagueTeamList;
		callback(null, responseObject);
	});
}

module.exports = findSportsLeagueTeamList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findSportsLeagueTeamList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}