require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var SportsLeagueTeamMasterSchema = new mongoose.Schema({
	teamId: {type: Number, unique: true},
	location: {type: String, unique: true},
	shortName: {type: String},
	key: {type: String},
	country: {type: Object},
	status: {type: String, enum: ['ACTIVE', 'INACTIVE']},
	displayOrder: {type: Number},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("SportsLeagueTeamMaster", SportsLeagueTeamMasterSchema, "sports_league_team");

SportsLeagueTeamMasterSchema.plugin(autoIncrement.plugin, {
	model: "SportsLeagueTeamMaster",
	field: "teamId",
	startAt: 1,
	incrementBy: 1
});