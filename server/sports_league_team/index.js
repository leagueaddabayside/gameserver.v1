var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();

route.post("/addSportsLeagueTeam", obj.addSportsLeagueTeam);
route.post("/updateSportsLeagueTeam", obj.updateSportsLeagueTeam);
route.post("/findSportsLeagueTeamByProperty", obj.findSportsLeagueTeamByProperty);
route.post("/findSportsLeagueTeamList", obj.findSportsLeagueTeamList);
	route.post("/updateSportsLeagueTeamDisplayOrder", obj.updateSportsLeagueTeamDisplayOrder);
// Routes
module.exports = route;
