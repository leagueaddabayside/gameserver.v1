var logger = require("../../utils/logger").gameLogs;
var sportsLeagueTeamService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateSportsLeagueTeam(request, response, next) {
	var requestObject = request.body;
	//console.log("updateSportsLeagueTeam API :- Request - %j", requestObject);

	var responseObject = new Object();
	sportsLeagueTeamService.updateSportsLeagueTeam(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateSportsLeagueTeam API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateSportsLeagueTeam;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.location = "";
		requestObject.shortName = "";
		requestObject.key = "";
		requestObject.country = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateSportsLeagueTeam(request, response);
	})();
}