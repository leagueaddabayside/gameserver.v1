var addSportsLeagueTeamApi = require("./add_sports_league_team");
var findSportsLeagueTeamByPropertyApi = require("./find_sports_league_team_by_property");
var findSportsLeagueTeamListApi = require("./find_sports_league_team_list");
var updateSportsLeagueTeamApi = require("./update_sports_league_team");
var updateSportsLeagueTeamDisplayOrderApi = require("./update_sports_league_team_display_order");
// Require

module.exports.addSportsLeagueTeam = addSportsLeagueTeamApi;
module.exports.findSportsLeagueTeamByProperty = findSportsLeagueTeamByPropertyApi;
module.exports.findSportsLeagueTeamList = findSportsLeagueTeamListApi;
module.exports.updateSportsLeagueTeam = updateSportsLeagueTeamApi;
module.exports.updateSportsLeagueTeamDisplayOrder = updateSportsLeagueTeamDisplayOrderApi
// Export
