var logger = require("../../utils/logger").gameLogs;
var sportsLeagueTeamService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findSportsLeagueTeamByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findSportsLeagueTeamByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	sportsLeagueTeamService.findSportsLeagueTeamByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("findSportsLeagueTeamByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findSportsLeagueTeamByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.teamId = "teamId";
		//requestObject.location = "location";
		//requestObject.shortName = "shortName";
		//requestObject.key = "key";
		//requestObject.country = "country";
		//requestObject.status = "status";
		//requestObject.createBy = "createBy";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findSportsLeagueTeamByProperty(request, response);
	})();
}