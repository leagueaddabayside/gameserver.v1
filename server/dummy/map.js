/**
 * Created by sumit on 2/22/2017.
 */

let map = new Map();
map.set('foo', 123);
map.set('12', 123);
map.set('13', 123);

console.log(map.get('foo'),map.size);
console.log([...map.keys()]);
console.log([...map.values()]);


let mapObj = new Map();
let obj1 = {};
obj1.playerId = 123;
obj1.playerName = 'name';

mapObj.set(123, obj1);

console.log(mapObj.get(123));
console.log(mapObj);

