/**
 * Created by sumit on 4/18/2017.
 */
var request = require('request');

var requestParam = {
    "players": [{
        "playerName": "AB de Villiers",
        "shortName": "ABD Villiers",
        "batter": true,
        "bowler": false,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 67,
        "isSelected": true,
        "isCaptain": true,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Aaron Finch",
        "shortName": "AJ Finch",
        "batter": true,
        "bowler": false,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "GL",
        "aliasTName": "GUJ",
        "playerId": 159,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": true,
        "isBonus": false
    }, {
        "playerName": "Adam Milne",
        "shortName": "AF Milne",
        "batter": false,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 62,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Billy Stanlake",
        "shortName": "B Stanlake",
        "batter": false,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 75,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Chirag Suri",
        "shortName": "C Suri",
        "batter": true,
        "bowler": false,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "GL",
        "aliasTName": "GUJ",
        "playerId": 164,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Ishan Kishan",
        "shortName": "IPKP Kishan",
        "batter": false,
        "bowler": false,
        "wicketKeeper": true,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "GL",
        "aliasTName": "GUJ",
        "playerId": 181,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "James Faulkner",
        "shortName": "JP Faulkner",
        "batter": true,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "GL",
        "aliasTName": "GUJ",
        "playerId": 170,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Pawan Negi",
        "shortName": "P Negi",
        "batter": true,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 52,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "Praveen Dubey",
        "shortName": "P Dubey",
        "batter": false,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 61,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "PraveenKumar",
        "shortName": "PKS Singh",
        "batter": false,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "GL",
        "aliasTName": "GUJ",
        "playerId": 173,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }, {
        "playerName": "STR Binny",
        "shortName": "STR Binny",
        "batter": true,
        "bowler": true,
        "wicketKeeper": false,
        "playerCredit": 8,
        "scoredPoints": 0,
        "teamName": "RCB",
        "aliasTName": "BAN",
        "playerId": 59,
        "isSelected": true,
        "isCaptain": false,
        "isViceCaptain": false,
        "isBonus": false
    }],
    "matchId": "31",
    "teamId": "1",
    "ts": 1493368996697,
    "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOjE3LCJpYXQiOjE0OTMzNjg3MTl9.hwwueUQUpTjZfHce8f4ygidUOWUFgU-ZbNKa5UN-vS4"
};
request.post({
    url: 'http://localhost:4000/saveTeam',
    form: requestParam
}, function (error, responseData, body) {
    console.log('getMatchScorecard', body);
    var respData = JSON.parse(body);

});