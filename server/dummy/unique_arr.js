/**
 * Created by sumit on 2/21/2017.
 */
var _ = require('lodash');
var data = [
    {
        url: 'www.example.com/hello',
        id: "22"
    },
    {
        url: 'www.example.com/hello',
        id: "22"
    },
    {
        url: 'www.example.com/hello-how-are-you',
        id: "23"
    },
    {
        url: 'www.example.com/i-like-cats',
        id: "24"
    },
    {
        url: 'www.example.com/i-like-pie',
        id: "25"
    }
]

var newData = _.uniqBy(data, function (e) {
    return e.id;
});
console.log(newData);