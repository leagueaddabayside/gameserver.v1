/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redis');

redis.set('some key', 'some value',function(){
    console.log('ccset some key');

});
/*
console.log('set some key');
redis.get('some key', function(err, value) {
    if (err) {
        throw err;
    }
    if (value) {
        console.log('value:', value);
    }
    else {
        console.log('value is gone');
        process.exit();
    }
});
*/

redis.expire('some key', 2);
let i =0;
setInterval(function() {
    redis.get('some key', function(err, value) {
        if (err) {
            throw err;
        }
        i++;
        if(i < 10){
            console.log('i:', i);

            redis.expire('some key', 2);
        }


        if (value) {
            console.log('value:', value);
        }
        else {
            console.log('value is gone');
            process.exit();
        }
    });
}, 1e3);