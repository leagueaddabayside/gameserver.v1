/**
 * Created by sumit on 3/2/2017.
 */
var redis = require('redis');

var port = process.env.REDIS_PORT || 6379;
var host = process.env.REDIS_HOST || '192.168.2.210';

module.exports = redis.createClient(port, host);