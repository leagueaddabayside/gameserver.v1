/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redis');

exports.set = setUserProfile;
exports.get = getUserProfile;

function setUserProfile(userId, profile, cb) {
    redis.hmset('profile:' + userId, profile, cb);
}

function getUserProfile(userId, cb) {
    redis.hgetall('profile:' + userId, cb);
}

/*
redis.hget('profile:' + userId, 'email', function(err, email) {
    if (err) {
        handleError(err);
    }
    else {
        console.log('User email:', email);
    }
});*/
