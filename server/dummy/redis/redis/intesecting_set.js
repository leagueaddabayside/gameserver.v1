/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redis');

function key(group) {
    return 'group:' + group;
}

redis.sinter(key('mods'), key('paying'), function(err, users) {
    if (err) {
        throw err;
    }

    console.log('paying mods: %j', users);
});