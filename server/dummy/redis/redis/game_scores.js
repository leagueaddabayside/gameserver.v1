/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redis');

exports.score = score;

function score(game, player, diff, cb) {
   // redis.zincrby(key(game), diff, player, cb);
    redis.zadd(key(game), diff, player, cb);

}

exports.rank = rank;

function rank1(game, cb) {
    redis.zrevrange(key(game), 0, -1, "WITHSCORES", function(err, ret) {
        if (err) {
            cb(err);
        }
        else {
            var rank = [];
            for (var i = 0 ; i < ret.length ; i += 2) {
                rank.push({player: ret[i], score: ret[i+1]});
            }
            cb(null, rank);
        }
    });
}

function rank(game, cb) {
    redis.zrange(key(game), 0, -1, "WITHSCORES", function(err, ret) {
        if (err) {
            cb(err);
        }
        else {

            var index = 0, prevValue = 0;

            var rank = [];

            var rowCount =0;
            for (var i = 0 ; i < ret.length ; i += 2) {

                rowCount++;

                var obj = {player: ret[i], score: ret[i+1]};
                if(prevValue != ret[i+1]){
                    index = rowCount;
                }
                prevValue = ret[i+1];
                obj.rank = index;
                rank.push(obj);
            }
            cb(null, rank);
        }
    });
}

function key(game) {
    return 'game:' + game;
}