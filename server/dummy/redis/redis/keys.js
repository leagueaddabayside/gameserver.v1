/**
 * Created by sumit on 3/3/2017.
 */
group:#{group}:members - [set] holds all the ids of the players in the group. #{group} in [0..20,000]
user:#{user}:groups - [set] holds all the users associated groups. #{user} in [0..50,000]
group:#{group}:game:1:rankings - [sorted set] holds all the player members current score for the game.
    game:1:rankings - [sorted set] holds all the 50,000 players scores
user:#{user}:groups:rankings - [sorted set] built on the fly. union of all groups the user is associated with. scores are aggregated.




match:#{matchId}:#{playerId} - name/fours/six/wicket/runs/maidain/points    - 22 per match
match:#{matchId}:users - hash - userId,teams,totalTeams - add teamName in user_match_team- 1 per match
match:#{matchId}:#{userId}:#{teamId} - hash - p1:rahane,tPoints,cap,vcap,teamName - team per user*team*match
match:#{matchId}:#{leagueId}:teams point [matchId+userId+teamId - sortedset 1 per league