/**
 * Created by sumit on 3/3/2017.
 */
var UserProfile = require('./user_profile');

var user = 'johndoe';
var profile = {
    name: 'John Doe',
    address: '31 Paper Street, Gotham City',
    zipcode: '987654',
    email: 'john.doe@example.com'
};

UserProfile.set(user, profile, function(err) {
    if (err) {
        throw err;
    }
    console.log('saved user profile');
    UserProfile.get(user, function(err, profile) {
        if (err) {
            throw err;
        }
        console.log('loaded user profile:', profile);
    });
});

