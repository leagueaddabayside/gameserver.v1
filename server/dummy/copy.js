/*
{
    "status": true,
    "version": "2.0.3",
    "status_code": 200,
    "expires": "1492080494.0",
    "Etag": "1492080494.0",
    "cache_key": "match|iplt20_2017_g10|full_card",
    "data": {
    "card_type": "full_card",
        "card": {
        "related_name": "10th Match",
            "key": "iplt20_2017_g10",
            "msgs": {
            "info": "Mumbai Indians won by 4 wickets.",
                "completed": "Mumbai Indians won by 4 wickets.",
                "others": [

            ]
        },
        "approx_completed_ts": 1492020887,
            "winner_team": "a",
            "winning_ratio": {
            "a": null,
                "b": null
        },
        "long_description": "Mumbai Indians won by 4 wickets. Mumbai Indians scored 159/6 in 18.4 and Sunrisers Hyderabad scored 158/8 in 20.0. JJ Bumrah played good and he is man of the match. - Mumbai Indians vs Sunrisers Hyderabad (International Cricket Match) - 10th Match - Indian Premier League 2017. On 12 April 2017 in Wankhede Stadium, Mumbai, India.",
            "title": "Mumbai Indians vs Sunrisers Hyderabad - 10th Match - Indian Premier League 2017",
            "dl_applied": false,
            "start_date": {
            "timestamp": 1492007400,
                "iso": "2017-04-12T14:30+00:00",
                "str": "12th Apr 2017 14:30 GMT"
        },
        "status": "completed",
            "man_of_match": "j_bumrah",
            "description": "Mumbai Indians won by 4 wickets. Mumbai Indians scored 159/6 in 18.4 and Sunrisers Hyderabad scored 158/8 in 20.0. JJ Bumrah played good and he is man of the match.",
            "short_name": "MI vs SRH",
            "format": "t20",
            "season": {
            "name": "Indian Premier League 2017",
                "key": "iplt20_2017"
        },
        "expires": "1492080494.0",
            "status_overview": "result",
            "match_overs": 20,
            "name": "Mumbai Indians vs Sunrisers Hyderabad",
            "ref": "http://www.litzscore.com/series/iplt20_2017/iplt20_2017_g10/",
            "venue": "Wankhede Stadium, Mumbai, India",
            "teams": {
            "a": {
                "key": "mi",
                    "short_name": "MI",
                    "match": {
                    "playing_xi": [
                        "p_patel",
                        "j_buttler",
                        "ni_rana",
                        "rg_sharma",
                        "k_pandya",
                        "k_pollard",
                        "h_pandya",
                        "hr_singh",
                        "l_malinga",
                        "m_mcclenaghan",
                        "j_bumrah"
                    ],

                        "key": "a",
                        "keeper": "p_patel",
                        "season_team_key": "iplt20_2017_mi",
                        "captain": "rg_sharma"
                },
                "name": "Mumbai Indians"
            },
            "b": {
                "key": "srh",
                    "short_name": "SRH",
                    "match": {
                    "playing_xi": [
                        "d_warner",
                        "s_dhawan",
                        "y_singh",
                        "d_hooda",
                        "v_shankar",
                        "n_ojha",
                        "b_cutting",
                        "b_kumar",
                        "ra_khan",
                        "a_nehra",
                        "m_rahman"
                    ],

                        "key": "b",
                        "keeper": "n_ojha",
                        "season_team_key": "iplt20_2017_srh",
                        "captain": "d_warner"
                },
                "name": "Sunrisers Hyderabad"
            }
        },
        "players": {

            "h_pandya": {
                "fullname": "Hardik Pandya",
                    "name": "Hardik Pandya",
                    "match": {
                    "innings": {
                        "1": {
                            "fielding": {
                                "catches": 2,
                                    "runouts": 0,
                                    "stumbeds": 0
                            },
                            "batting": {
                                "dots": 1,
                                    "sixes": 0,
                                    "runs": 2,
                                    "balls": 3,
                                    "fours": 0,
                                    "strike_rate": 66.67,
                                    "dismissed": false
                            },
                            "bowling": {
                                "dots": 10,
                                    "runs": 22,
                                    "balls": 18,
                                    "maiden_overs": 0,
                                    "wickets": 1,
                                    "extras": 3,
                                    "overs": "3.0",
                                    "economy": 7.33
                            }
                        }
                    }
                }

            },
        },
        "data_review_checkpoint": "post-match-validated",

    }
}
}*/
