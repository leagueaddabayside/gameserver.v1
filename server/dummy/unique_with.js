/**
 * Created by sumit on 4/4/2017.
 */
var _ = require('lodash');
var objects = [{ 'x': 1, 'y': 2,z:1 }, { 'x': 2, 'y': 1,z:2 }, { 'x': 1, 'y': 2,z:3 }];

let playerArr = _.uniqWith(objects, _.isEqual);
// => [{ 'x': 1, 'y': 2 }, { 'x': 2, 'y': 1 }]
console.log(playerArr);