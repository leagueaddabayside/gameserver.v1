/**
 * Created by sumit on 3/27/2017.
 */
let rankPrizeMap = new Map();
let prizeInfo = [
    {
        "prizeAmt" : 100000,
        "startRank" : 1
    },
    {
        "prizeAmt" : 50000,
        "startRank" : 2
    },
    {
        "prizeAmt" : 25000,
        "toRank" : 12,
        "startRank" : 3
    },
    {
        "prizeAmt" : 10000,
        "toRank" : 17,
        "startRank" : 13
    }
];
prizeInfo.forEach(function (value) {

    let prizeAmount = value.prizeAmt;
    let startRank = value.startRank;
    let endRank = value.toRank;
    if(endRank){
        for(let rank=startRank;rank<= endRank;rank++){
            rankPrizeMap.set(rank,prizeAmount);
        }
    }else{
        rankPrizeMap.set(startRank,prizeAmount);

    }

});
console.log(rankPrizeMap);