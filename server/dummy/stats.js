/**
 * Created by sumit on 2/15/2017.
 */
// setup express to configure a basic web server
var express = require('express');
var app = express();

// moment.js is the preferred date library
var moment = require('moment');
// access Node.js Crypto library for signature generation
var crypto = require('crypto');

// use request or request-promise to call into STATS API
var request = require('request');

// respond to all get requests
app.get('/', function (req, res) {
    // get the current time
    var timeFromEpoch = moment.utc().unix();

    // set the API key (note that this is not a valid key!)
    var apiKey = 'wj9zwq6prsfg8trhwh5n4vz7';

    // set the shared secret key
    var secret = 'kUTYq74TYx';

    // generate signature
    var sig = crypto.createHash('sha256').update(apiKey + secret + timeFromEpoch).digest('hex');

    request('http://api.stats.com/v1/stats/cricket/leagues/?accept=json&api_key=' + apiKey + '&sig=' + sig,
        function (err, response, body) {
            // parse the body as JSON

            console.log(err, response, body);

            var parsedBody = JSON.parse(body);
            res.json(parsedBody);
        });
});

var port = 3001;
app.listen(port, function () {
    console.log('Listening on port ' + port);
});

// run this file (node [filename]) and go to localhost:3001