var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var sportsTourService = require('../../tours/services/index');

var statsRequestService = require('../stats_request/index');

function uploadSportsTourTeams(request, response, next) {
    console.log('uploadSportsTourTeams',request.body);
    //console.log(request.file);
    var requestObject = request.body;
    requestObject.status = ['ACTIVE','APPROVED'];
    var responseObject = new Object();

    sportsTourService.findTourList(requestObject,function(err,sportsTours){
        if(err){
            responseObject.respCode = sportsTours;
            responseObject.message = responseMessage[sportsTours];
            response.json(responseObject);
            return;
        }else{
            statsService.uploadSportsTourTeamsListService(sportsTours.responseData, function (error, data) {
                if (error) {
                    responseObject.responseCode = data.responseCode;
                    responseObject.message = responseMessage[data.responseCode];
                } else {
                    responseObject.responseCode = data.responseCode;
                    responseObject.responseData = data.responseData;
                }

                logger.info("uploadSportsTours API :- Response - %j", responseObject);
                response.json(responseObject);
            });
        }
    });

}

module.exports = uploadSportsTourTeams;


// Unit Test Case
if (require.main === module) {
    (function() {
        var request = {};
        var response = {
            json : function(result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsTourTeams(request, response);
    })();
}