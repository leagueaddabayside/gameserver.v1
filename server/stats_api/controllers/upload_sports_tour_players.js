var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var sportsTourService = require('../../tours/services/index');

function uploadSportsTourPlayers(request, response, next) {
    console.log('uploadSportsTourPlayers', request.body);
    //console.log(request.file);
    var requestObject = request.body;
    requestObject.status = ['ACTIVE','APPROVED'];
    var responseObject = new Object();

    sportsTourService.findTourList(requestObject, function (err, sportsTours) {
        if (err) {
            responseObject.respCode = sportsTours;
            responseObject.message = responseMessage[sportsTours];
            response.json(responseObject);
            return;
        } else {

            var teamArr = [];
            var tourList = sportsTours.responseData;
            for (var i = 0, length = tourList.length; i < length; i++) {
                var tourRow = tourList[i];
                console.log('tourRow',tourRow);
                if(tourRow.teams){
                    for (var j = 0, length1 = tourRow.teams.length; j < length1; j++) {
                        var teamRow = tourRow.teams[j];
                        var teamObj = {};
                        if (teamRow.teamId && teamRow.teamId !== null) {
                            teamObj.teamId = teamRow.teamId;
                            teamObj.tourId = tourRow.tourId;
                            teamObj.displayName = teamRow.displayName;
                            teamArr.push(teamObj);
                        }
                    }
                }
            }


            statsService.uploadSportsTourPlayersListService(teamArr, function (error, data) {
                if (error) {
                    responseObject.responseCode = data.responseCode;
                    responseObject.message = responseMessage[data.responseCode];
                } else {
                    responseObject.responseCode = data.responseCode;
                    responseObject.responseData = data.responseData;
                }

                logger.info("uploadSportsTours API :- Response - %j", responseObject);
                response.json(responseObject);
            });
        }
    });

}

module.exports = uploadSportsTourPlayers;


// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsTourPlayers(request, response);
    })();
}