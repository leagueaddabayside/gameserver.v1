/**
 * Created by sumit on 2/16/2017.
 */
var uploadSportsLeagues = require("./upload_sports_leagues");
var uploadSportsTours = require("./upload_sports_tours");
var uploadSportsTourPlayers = require("./upload_sports_tour_players");
var uploadSportsTourMatches = require("./upload_sports_tour_matches");
var uploadSportsTourTeams = require("./upload_sports_tour_teams");

module.exports.uploadSportsLeaguesApi = uploadSportsLeagues;
module.exports.uploadSportsToursApi = uploadSportsTours;
module.exports.uploadSportsTourPlayersApi = uploadSportsTourPlayers;
module.exports.uploadSportsTourMatchesApi = uploadSportsTourMatches;
module.exports.uploadSportsTourTeamsApi = uploadSportsTourTeams;
