var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var statsService = require('../services/index');
var statsRequestService = require('../stats_request/index');

function uploadSportsLeagues(request, response, next) {
    logger.info('uploadSportsLeagues',request.body);
    //console.log(request.file);
    var requestObject = request.body;
    var responseObject = new Object();

    statsRequestService.fetchSportsLeagueRequest({},function(err,sportsLeague){
        if(err){
            responseObject.respCode = sportsLeague;
            responseObject.message = responseMessage[sportsLeague];
            response.json(responseObject);
            return;
        }else{
            statsService.uploadSportsLeagueListService(sportsLeague.apiResults, function (error, data) {
                if (error) {
                    responseObject.responseCode = data.responseCode;
                    responseObject.message = responseMessage[data.responseCode];
                } else {
                    responseObject.responseCode = data.responseCode;
                    responseObject.responseData = data.responseData;
                }
                logger.info("uploadSportsLeagues API :- Response - %j", responseObject);
                response.json(responseObject);
            });
        }
    });
}

module.exports = uploadSportsLeagues;
// Unit Test Case
if (require.main === module) {
    (function() {
        var request = {};
        var response = {
            json : function(result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        uploadSportsLeagues(request, response);
    })();
}