/**
 * Created by sumit on 2/15/2017.
 */
let request = require('request');
let statsRequest = require('./index');
let logger = require('../../utils/logger').gameLogs;
var responseCode = require("../../utils/response_code");

function fetchSportsToursPlayersRequest(requestObject, responseCb) {
    let statsAuth = statsRequest.getStatsAuthentication();

    request('http://api.stats.com/v1/stats/cricket/icc/participants/teams/'+requestObject.teamId+'/?accept=json&api_key=' + statsAuth.apiKey + '&sig=' + statsAuth.sig,
        function (err, response, body) {
            // parse the body as JSON
            if(err){
                logger.error(err);
                responseCb(true,responseCode.STATS_INTERNAL_ERROR);
                return;
            }
            if(response.statusCode === 200){
                var parsedBody = JSON.parse(body);
                responseCb(null,parsedBody);
            }else{
                logger.error(response.statusCode,response.statusMessage);
                responseCb(true,responseCode.STATS_DEFINED_ERROR);
            }
        });
}

module.exports = fetchSportsToursPlayersRequest;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.teamId = 6297;
    //requestObject.status = "ACTIVE";

    fetchSportsToursPlayersRequest(requestObject, function(error, responseObject) {
        console.log("Response Code - " ,error, responseObject);

    });
}