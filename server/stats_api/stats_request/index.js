var fetchSportsLeagueRequest = require("./fetch_sports_league");
var getStatsAuthentication = require("./get_stats_authentication");
var fetchSportsTourRequest = require("./fetch_sports_tours");
var fetchSportsTourPlayersRequest = require("./fetch_sports_tours_players");
var fetchSportsTourMatchesRequest = require("./fetch_sports_tours_matches");

module.exports.fetchSportsLeagueRequest =fetchSportsLeagueRequest;
module.exports.getStatsAuthentication =getStatsAuthentication;
module.exports.fetchSportsTourRequest =fetchSportsTourRequest;
module.exports.fetchSportsTourPlayersRequest =fetchSportsTourPlayersRequest;
module.exports.fetchSportsTourMatchesRequest =fetchSportsTourMatchesRequest;
