/**
 * Created by sumit on 2/20/2017.
 */

let eventTypeList = [{
    "eventTypeId": 1,
    "name": "ODI",
    "formatType": "ONE_DAY"
}, {
    "eventTypeId": 2,
    "name": "TEST",
    "formatType": "TEST"
}, {
    "eventTypeId": 3,
    "name": "T20",
    "formatType": "T20"
}, {
    "eventTypeId": 5,
    "name": "Warm-up 50 over",
    "formatType": "WARM_UP_50_OVER"
}, {
    "eventTypeId": 7,
    "name": "Limited Overs",
    "formatType": "LIMITED_OVERS"
}, {
    "eventTypeId": 8,
    "name": "Warm-up T20",
    "formatType": "WARM_UP_T20"
}]


let skillTypesData = [{
    "skillTypeId": 1,
    "name": "Batsman",
    "skillType" : "BATSMAN"
}, {
    "skillTypeId": 2,
    "name": "Bowler",
    "skillType" : "BOWLER"
}, {
    "skillTypeId": 3,
    "name": "All-Rounder",
    "skillType" : "ALL_ROUNDER"
}, {
    "skillTypeId": 4,
    "name": "Wicket-Keeper",
    "skillType" : "WEEKET_KEEPER"
}, {
    "skillTypeId": 0,
    "name": "Unknown",
    "skillType" : "UNKNOWN"
}]

function getEventFormatType(eventTypeId) {
    for (var i = 0, length = eventTypeList.length; i < length; i++) {
        var eventRow = eventTypeList[i];
        if (eventTypeId == eventRow.eventTypeId) {
            return eventRow.formatType;
        }
    }

}

function getPlayerSkillType(skillTypeId) {
    for (var i = 0, length = skillTypesData.length; i < length; i++) {
        var skillRow = skillTypesData[i];
        if (skillTypeId == skillRow.skillTypeId) {
            return skillRow.skillType;
        }
    }
}

module.exports.getEventFormatType = getEventFormatType;
module.exports.getPlayerSkillType = getPlayerSkillType;


// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.status = "ACTIVE";

    console.log(getEventFormatType(1));
}