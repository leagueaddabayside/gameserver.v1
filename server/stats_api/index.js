/**
 * Created by sumit on 2/15/2017.
 */
var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();

route.post("/uploadSportsLeagues", obj.uploadSportsLeaguesApi);
route.post("/uploadSportsTours", obj.uploadSportsToursApi);
route.post("/uploadSportsTourPlayers", obj.uploadSportsTourPlayersApi);
route.post("/uploadSportsTourMatches", obj.uploadSportsTourMatchesApi);
route.post("/uploadSportsTourTeams", obj.uploadSportsTourTeamsApi);

module.exports = route;
