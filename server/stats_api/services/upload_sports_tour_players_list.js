var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourPlayersService = require('../../tour_players/services/index');
async = require("async");
var moment = require("moment");
var statsData = require('../stats_data');
var statsRequestService = require('../stats_request/index');

function uploadSportsTourPlayersList(requestObject, responseCb) {

    console.log('uploadSportsTourPlayersList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    async.eachSeries(requestObject,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            console.log('uploadSportsTourPlayers',item);
           uploadSportsTourPlayers(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })
        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            console.log('uploadSportsTourPlayersList final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsTourPlayers(teamObj, responseCb) {

    statsRequestService.fetchSportsTourPlayersRequest({teamId : teamObj.teamId},function(err,teamPlayersResponse){
        if (err) {
            console.log('fetchSportsTourPlayersRequest error',err,teamPlayersResponse);
            responseCb(null);
            return;
        }

        let playersArr = [];
        var playerResponseApiResult = teamPlayersResponse.apiResults;
        if(playerResponseApiResult[0].league && playerResponseApiResult[0].league.players[0]) {
            let statsPlayersArr = playerResponseApiResult[0].league.players;
            for (var i=0, length=statsPlayersArr.length; i<length; i++) {
                let playerObj = makeTeamPlayerData(statsPlayersArr[i]);
                playerObj.tourId = teamObj.tourId;
                playersArr.push(playerObj);
            }
        }

        async.eachSeries(playersArr,
            // 2nd param is the function that each item is passed to
            function (playerRequestObj, cb) {
                console.log('uploadSportsTourPlayerData',playerRequestObj);
                uploadSportsTourPlayerData(playerRequestObj, function (error, responsePlayerObject) {
                    cb(error, {});
                    return;
                })
            },
            // 3rd param is the function to call when everything's done
            function (err) {
                // All tasks are done now
                console.log('uploadSportsTourPlayersList final done');
                responseCb(null, {});

            }
        );

    });

}

function uploadSportsTourPlayerData(playerObj, responseCb) {
    var tourPlayerRequestObject = new Object();
    tourPlayerRequestObject.key = playerObj.key;
    tourPlayerRequestObject.tourId = playerObj.tourId;
    sportsTourPlayersService.findTourPlayersByProperty(tourPlayerRequestObject, function (error, tourPlayerResponseObject) {
        if (error) {
            console.log('findTourPlayersByProperty error',error);
            callback(error);
            return;
        }
        console.log('findTourPlayersByProperty',tourPlayerResponseObject);

        if (tourPlayerResponseObject.responseData && tourPlayerResponseObject.responseData.key) {
            responseCb(null, {responseCode :responseCode.SUCCESS });
            return;

           /* sportsTourPlayersService.updateTourPlayers(playerObj, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });*/

        } else {
            playerObj.status = 'ACTIVE';
            console.log('addTourPlayers',playerObj);
            sportsTourPlayersService.addTourPlayers(playerObj, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });

}


function makeTeamPlayerData(requestObject) {
    var playerObject = {};
    playerObject.key = requestObject.playerId;
    playerObject.name = requestObject.firstName + ' ' +requestObject.lastName;
    playerObject.team = requestObject.team;
    var identifyRoles = getPlayerIdentifyRoles(requestObject.skillType.skillTypeId);
    playerObject.keeper = identifyRoles.keeper;
    playerObject.batsman = identifyRoles.batsman;
    playerObject.bowler = identifyRoles.bowler;
    playerObject.points = 0;
    playerObject.credit = 0;

    return playerObject;
}

function getPlayerIdentifyRoles(skillTypeId) {
    var identifyRoles = {};
    identifyRoles.keeper = false;
    identifyRoles.batsman = false;
    identifyRoles.bowler = false;

    var skillType = statsData.getPlayerSkillType(skillTypeId);
    if(skillType === 'BATSMAN'){
        identifyRoles.batsman = true;
    }else if(skillType === 'BOWLER'){
        identifyRoles.bowler = true;
    }else if(skillType === 'ALL_ROUNDER'){
        identifyRoles.bowler = true;
        identifyRoles.batsman = true;
    }else if(skillType === 'WEEKET_KEEPER'){
        identifyRoles.keeper = true;
    }else if(skillType === 'UNKNOWN'){
        identifyRoles.batsman = true;
    }
    return identifyRoles;
}


module.exports = uploadSportsTourPlayersList;