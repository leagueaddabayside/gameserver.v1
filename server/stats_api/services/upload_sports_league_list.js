var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsLeagueService = require('../../sports_league/services/index');
async = require("async");

function uploadSportsLeagueList(requestObject, responseCb) {

    console.log('uploadSportsLeagueList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    async.eachSeries(requestObject[0].leagues,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            console.log('uploadSportsLeague',item);
            uploadSportsLeague(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            console.log('uploadSportsLeagueList final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsLeague(requestObject, responseCb) {
    var sportsLeagueRequestObject = new Object();
    sportsLeagueRequestObject.tpLeagueId = requestObject.league.leagueId;
    sportsLeagueService.findSportsLeagueByProperty(sportsLeagueRequestObject, function (error, sportsLeagueResponseObject) {
        if (error) {
            console.log('findSportsLeagueByProperty error',error);
            callback(error);
            return;
        }
        console.log('findSportsLeagueByProperty',sportsLeagueResponseObject);
        var sportsLeagueObject = makeSportsLeagueRequestData(requestObject);

        if (sportsLeagueResponseObject.responseData && sportsLeagueResponseObject.responseData.key) {
            sportsLeagueService.updateSportsLeague(sportsLeagueObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        } else {
            console.log('addSportsLeague',sportsLeagueObject);

            sportsLeagueService.addSportsLeague(sportsLeagueObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });

}

function makeSportsLeagueRequestData(requestObject) {
    var sportsLeagueObject = {};
    var leagueObj = requestObject.league;
    sportsLeagueObject.tpLeagueId = leagueObj.leagueId;
    sportsLeagueObject.name = leagueObj.name;
    sportsLeagueObject.shortName = leagueObj.abbreviation;
    sportsLeagueObject.displayName = leagueObj.displayName;
    sportsLeagueObject.key = leagueObj.uriPaths[0].path;

    return sportsLeagueObject;
}

module.exports = uploadSportsLeagueList;