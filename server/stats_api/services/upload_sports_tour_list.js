var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourService = require('../../tours/services/index');
async = require("async");
var moment = require("moment");
var statsData = require('../stats_data');

function uploadSportsTourList(requestObject, responseCb) {

    console.log('uploadSportsTourList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    let toursArr = [];
    if(requestObject[0].league && requestObject[0].league.seasons[0] && requestObject[0].league.seasons[0].eventType){
       let evenTypeToursArr = requestObject[0].league.seasons[0].eventType;
        console.log('evenTypeToursArr',evenTypeToursArr);
        for (var i=0, length=evenTypeToursArr.length; i<length; i++) {
            var eventRow = evenTypeToursArr[i];
            console.log('eventRow i,length',eventRow,i,length);

            var formatType = statsData.getEventFormatType(eventRow.eventTypeId);

            for (var j=0, length1=eventRow.series.length; j<length1; j++) {
                var tourRow = eventRow.series[j];
                let currentTourObj = {};
                currentTourObj.tourName = tourRow.name;
                currentTourObj.key = tourRow.seriesId;
                currentTourObj.formatType = formatType;
                currentTourObj.startDate =moment(tourRow.startDate.full).toDate();
                currentTourObj.endDate = moment(tourRow.endDate.full).toDate();
                toursArr.push(currentTourObj);
            }
        }

    }else{
        responseCb(null, responseObject);
        return;
    }

    async.eachSeries(toursArr,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            console.log('uploadSportsTour',item);
            uploadSportsTour(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            console.log('uploadSportsTourList final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsTour(requestObject, responseCb) {
    var sportsTourRequestObject = new Object();
    sportsTourRequestObject.key = requestObject.key;
    sportsTourService.findTourByProperty(sportsTourRequestObject, function (error, sportsTourResponseObject) {
        if (error) {
            console.log('findTourByProperty error',error);
            callback(error);
            return;
        }
        console.log('findTourByProperty',sportsTourResponseObject);
        var sportsTourObject = makeSportsTourRequestData(requestObject);

        if (sportsTourResponseObject.responseData && sportsTourResponseObject.responseData.key) {
            sportsTourObject.tourId = sportsTourResponseObject.responseData.tourId;
            sportsTourService.updateTour(sportsTourObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        } else {
            console.log('addSportsLeague',sportsTourObject);
            sportsTourObject.shortName = sportsTourObject.tourName;
            sportsTourObject.status = 'PENDING';
            sportsTourService.addTour(sportsTourObject, function (error, responseObject) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }
    });

}

function makeSportsTourRequestData(requestObject) {
    var sportsTourObject = {};
    sportsTourObject.tourName = requestObject.tourName;
    sportsTourObject.key = requestObject.key;
    sportsTourObject.formatType = requestObject.formatType;
    sportsTourObject.startDate =requestObject.startDate;
    sportsTourObject.endDate = requestObject.endDate;
    return sportsTourObject;
}

module.exports = uploadSportsTourList;