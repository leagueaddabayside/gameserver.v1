var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourService = require('../../tours/services/index');
var tourMatchesService = require('../../tour_matches/services/index');
async = require("async");
var moment = require("moment");
var statsData = require('../stats_data');
var _ = require("lodash");

function uploadSportsTourTeamList(requestObject, responseCb) {

    console.log('uploadSportsTourTeamList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    async.eachSeries(requestObject,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            console.log('uploadSportsTourTeams',item);
            uploadSportsTourTeams(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            console.log('uploadSportsTourTeamList final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsTourTeams(requestObject, responseCb) {
    var tourMatchRequestObject = new Object();
    tourMatchRequestObject.tourId = requestObject.tourId;

    tourMatchesService.findTourMatchesList(tourMatchRequestObject,function (error, tourMatchResponseObject) {
        if (error) {
            console.log('findTourMatchesList error',error);
            responseCb(error);
            return;
        }
       // console.log('findTourMatchesList',tourMatchResponseObject);
        if(tourMatchResponseObject.responseData && tourMatchResponseObject.responseData.length > 0){

            var tourMatchArr = tourMatchResponseObject.responseData;
            var teamArr = [];
            for (var i=0, length=tourMatchArr.length; i<length; i++) {
                var matchRow = tourMatchArr[i];
                //console.log('matchRow',matchRow);
                for (var j=0, length1=matchRow.teams.length; j<length1; j++) {
                    var teamRow = matchRow.teams[j];
                    var teamObj = {};
                    if(teamRow.teamId && teamRow.teamId !== null){
                        teamObj.teamId = teamRow.teamId;
                        teamObj.displayName = teamRow.displayName;
                        teamArr.push(teamObj);
                    }

                }

            }
            console.log('teamArr',teamArr);
            var uniqueTeamArr = _.uniqBy(teamArr, function (e) {
                return e.teamId;
            });
            var updateTourRequestObject = {};
            updateTourRequestObject.tourId = requestObject.tourId;
            updateTourRequestObject.teams = uniqueTeamArr;
            sportsTourService.updateTour(updateTourRequestObject,function(error, responseObject){
                responseObject.responseCode = responseCode.SUCCESS;
                responseCb(null, responseObject);
                return;
            });
        }else{
            responseCb(null);
        }

    })

}

module.exports = uploadSportsTourTeamList;