var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var sportsTourService = require('../../tours/services/index');
var tourMatchesService = require('../../tour_matches/services/index');
async = require("async");
var moment = require("moment");
var statsData = require('../stats_data');

function uploadSportsTourMatchList(requestObject, responseCb) {

    console.log('uploadSportsTourList:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    let matchesArr = [];
    if(requestObject[0].league && requestObject[0].league.seasons[0] && requestObject[0].league.seasons[0].eventType){
       let evenTypeMatchesArr = requestObject[0].league.seasons[0].eventType;
        console.log('evenTypeMatchesArr',evenTypeMatchesArr);
        for (var i=0, length=evenTypeMatchesArr.length; i<length; i++) {
            var eventRow = evenTypeMatchesArr[i];
            console.log('eventRow i,length',eventRow,i,length);

            var formatType = statsData.getEventFormatType(eventRow.eventTypeId);

            for (var j=0, length1=eventRow.events.length; j<length1; j++) {
                var matchRow = eventRow.events[j];
                let currentMatchObj = {};
                currentMatchObj.name = matchRow.series.seriesGameNumber;
                currentMatchObj.relatedName = matchRow.series.seriesGameNumber;
                currentMatchObj.venue = matchRow.venue.name;
                currentMatchObj.key = matchRow.eventId;
                currentMatchObj.formatType = formatType;
                currentMatchObj.teams = matchRow.teams;
                currentMatchObj.seriesId = matchRow.series.seriesId;
                currentMatchObj.startDate =moment(matchRow.startDate[0].full).toDate();
                matchesArr.push(currentMatchObj);
            }
        }

    }else{
        responseCb(null, responseObject);
        return;
    }

    async.eachSeries(matchesArr,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            console.log('uploadSportsTourMatch',item);
            uploadSportsTourMatch(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            console.log('uploadSportsTourMatchList final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function uploadSportsTourMatch(requestObject, responseCb) {
    var sportsTourRequestObject = new Object();
    sportsTourRequestObject.key = requestObject.seriesId;

    sportsTourService.findTourByProperty(sportsTourRequestObject,function (error, sportsTourResponseObject) {
        if (error) {
            console.log('findTourByProperty error',error);
            responseCb(error);
            return;
        }
        console.log('findTourByProperty',sportsTourResponseObject);
        if(sportsTourResponseObject.responseData && sportsTourResponseObject.responseData.tourId){
            requestObject.tourId = sportsTourResponseObject.responseData.tourId;
            var tourMatchRequestObject = new Object();
            tourMatchRequestObject.key = requestObject.key;

            tourMatchesService.findTourMatchesByProperty(tourMatchRequestObject, function (error, tourMatchResponseObject) {
                if (error) {
                    console.log('findTourMatchesByProperty error',error);
                    responseCb(error);
                    return;
                }
                console.log('findTourMatchesByProperty',tourMatchResponseObject);
                var tourMatchObject = makeTourMatchRequestData(requestObject);

                if (tourMatchResponseObject.responseData && tourMatchResponseObject.responseData.key) {
                    tourMatchObject.matchId = tourMatchResponseObject.responseData.matchId;
                    tourMatchesService.updateTourMatches(tourMatchObject, function (error, responseObject) {
                        responseObject.responseCode = responseCode.SUCCESS;
                        responseCb(null, responseObject);
                        return;
                    });
                } else {
                    console.log('addTourMatches',tourMatchObject);
                    tourMatchObject.status = 'ACTIVE';
                    tourMatchesService.addTourMatches(tourMatchObject, function (error, responseObject) {
                        responseObject.responseCode = responseCode.SUCCESS;
                        responseCb(null, responseObject);
                        return;
                    });
                }
            });


        }else{
            responseCb(null);
        }

    })

}

function makeTourMatchRequestData(requestObject) {
    var tourMatchObject = {};
    tourMatchObject.tourId = requestObject.tourId;
    tourMatchObject.name = requestObject.name;
    tourMatchObject.relatedName = requestObject.relatedName;
    tourMatchObject.venue = requestObject.venue;
    tourMatchObject.key =requestObject.key;
    tourMatchObject.formatType = requestObject.formatType;
    tourMatchObject.teams = requestObject.teams;
    tourMatchObject.status = requestObject.status;
    tourMatchObject.startTime = requestObject.startDate;

    return tourMatchObject;
}

module.exports = uploadSportsTourMatchList;