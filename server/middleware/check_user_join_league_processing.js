var logger = require('../utils/logger.js').gameLogs;
var redisConn = require("../redis/redisConnection");
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function checkUserJoinLeagueProcessing(request, response, next) {
    var reqParams = request.body;
    var key = 'RUNNING:' + reqParams.userId + ':' + reqParams.leagueId;

    redisConn.get(key, function (err, data) {

        if (data) {
            response.json({
                respCode: responseCode.LEAGUE_JOIN_ALREADY_PROCESSING,
                message: responseMessage[responseCode.LEAGUE_JOIN_ALREADY_PROCESSING]
            })
            return;
        } else {
            redisConn.setex(key, 30, 'YES');
            next();

        }


    });

}
module.exports = checkUserJoinLeagueProcessing;
