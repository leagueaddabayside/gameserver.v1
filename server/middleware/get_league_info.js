var logger = require('../utils/logger.js').gameLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const leagueService = require('../league_master/services/index');


function getLeagueInfo(request, response, next) {
    var params = request.body;
    var requestObject = {};
    requestObject.leagueId = params.leagueId;
    leagueService.findLeagueByProperty(requestObject, function (err, leagueObjectResponse) {
        if (leagueObjectResponse.responseData && leagueObjectResponse.responseData.matchId) {
            var leagueInfo = leagueObjectResponse.responseData;

            request.body.leagueInfo = leagueInfo;
            request.body.matchId = leagueInfo.matchId;
            next();
        } else {
            response.json({
                respCode: responseCode.LEAGUE_DOES_NOT_EXIST,
                message: responseMessage[responseCode.LEAGUE_DOES_NOT_EXIST]
            })
        }

    });


}

module.exports = getLeagueInfo;
