var logger = require('../utils/logger.js').gameLogs;
const leagueTeamRankRedis = require('../redis/league_team_rank');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getLeagueRankPointsRedis(request, response, next) {
    var reqParams = request.body;
    logger.info('getLeagueRankPointsRedis reqParams', reqParams);
    leagueTeamRankRedis.rank(reqParams.leagueId,-1, function (err, resp) {
        logger.info('getLeagueRankPointsRedis resp', resp);

        if (resp) {
            request.body.leagueRankPoints = resp;
        }

        next();
    })

}

module.exports = getLeagueRankPointsRedis;
