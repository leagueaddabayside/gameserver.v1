var logger = require('../utils/logger.js').gameLogs;
const redis = require('../redis/redisConnection');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
let moment = require('moment');
const cricketRequest = require('../cricket_api/cricket_request/index');


function validateCricketApiToken(request, response, next) {
    var reqParams = request.body;
    let timeFromEpoch = moment.utc().unix();

    redis.get('CRICKETAPI_TOKEN_EXPIRE',function(err,data){
        if(data && data > timeFromEpoch){
            redis.get('CRICKETAPI_ACCESSTOKEN',function(err,token){
                if(token){
                    request.body.cricketAPIToken = token;
                    next();
                }else{
                    cricketRequest.getCricketApiAuthentication(err,function(authResponse){
                        console.log("TOKEN KEYS",authResponse);
                        redis.set('CRICKETAPI_TOKEN_EXPIRE',authResponse.expires);
                        redis.set('CRICKETAPI_ACCESSTOKEN',authResponse.access_token);
                        request.body.cricketAPIToken = authResponse.access_token;
                        next();
                    });
                }
            });
        }else{
            console.log("TOKEN ELSE");
            cricketRequest.getCricketApiAuthentication({},function(err,authResponse){
                console.log("TOKEN KEYS",authResponse);
                redis.set('CRICKETAPI_TOKEN_EXPIRE',authResponse.expires);
                redis.set('CRICKETAPI_ACCESSTOKEN',authResponse.access_token);
                request.body.cricketAPIToken = authResponse.access_token;
                next();
            });
        }
    });


}

module.exports = validateCricketApiToken;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.status = "ACTIVE";

    cricketRequest.getCricketApiAuthentication({},function(err,authResponse){
        redis.set('CRICKETAPI_TOKEN_EXPIRE',authResponse.expires);
        redis.set('CRICKETAPI_ACCESSTOKEN',authResponse.access_token);
        request.body.cricketAPIToken = authResponse.access_token;
        next();
    });
}