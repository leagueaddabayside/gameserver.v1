var logger = require('../utils/logger.js').gameLogs;
const playerMatchData = require('../redis/player_match_data');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getPlayerMatchInfo(request, response, next) {
    var reqParams = request.body;
    logger.info('getPlayerMatchInfo reqParams',reqParams);
    playerMatchData.get(reqParams.userId,reqParams.matchId,function(err,resp){
        logger.info('getPlayerMatchInfo resp',resp);

        let bonusUsed = 0;
        if(resp && resp.bonusUsed){
            bonusUsed = parseFloat(resp.bonusUsed);
        }
        request.body.bonusUsed = bonusUsed;
        next();
    })

}

module.exports = getPlayerMatchInfo;
