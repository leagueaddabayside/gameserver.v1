var logger = require('../utils/logger.js').gameLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const tourMatchService = require('../tour_matches/services/index');
var moment = require("moment");


function getTourMatchInfo(request, response, next) {
    var params = request.body;
    var requestObject = {};
    requestObject.matchId = params.matchId;
    tourMatchService.findTourMatchesByProperty(requestObject, function (err, tourMatchResponse) {
        if (tourMatchResponse.responseData && tourMatchResponse.responseData.matchId) {
            var matchInfo = tourMatchResponse.responseData;
            if (typeof matchInfo.startTime !== "undefined" && matchInfo.startTime !== null)
                matchInfo.startTime = moment(matchInfo.startTime).unix();

            request.body.matchInfo = matchInfo;
            next();
        } else {
            response.json({
                respCode: responseCode.MATCH_DOES_NOT_EXIST,
                message: responseMessage[responseCode.MATCH_DOES_NOT_EXIST]
            })
        }

    });


}

module.exports = getTourMatchInfo;
