var logger = require('../utils/logger.js').gameLogs;
const tokenizer = require('../redis/redis');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getUserIdFromToken(request, response, next) {
    var reqParams = request.body;

    tokenizer.checkToken(reqParams.token)
        .then(userId => {
            request.body.userId = userId;
            next();

        })
        .catch(function (err) {
            logger.error(err);
            response.json({
                respCode: responseCode.INVALID_USER_TOKEN,
                message: responseMessage[responseCode.INVALID_USER_TOKEN]
            })
        });

}

module.exports = getUserIdFromToken;
