var logger = require('../utils/logger.js').gameLogs;
const userMatchTeamPoints = require('../redis/user_match_team_points');
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getPlayerMatchTeamPoints(request, response, next) {
    var reqParams = request.body;
    logger.info('getPlayerMatchTeamPoints reqParams', reqParams);
    userMatchTeamPoints.get(reqParams.matchId, function (err, resp) {
       // console.log('getPlayerMatchTeamPoints resp', resp);


        if (resp) {
            request.body.matchTeamPoints = resp;
        }

        next();
    })

}

module.exports = getPlayerMatchTeamPoints;
