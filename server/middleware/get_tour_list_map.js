var logger = require('../utils/logger.js').gameLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const tourService = require('../tours/services/index');


function getTourListMap(request, response, next) {
    var params = request.body;
    var requestObject = {};
    requestObject.status = ['ACTIVE','CLOSED'];
    tourService.findTourList(requestObject,function(err,tourResponse){
        if(tourResponse.responseData){
            var tourList = tourResponse.responseData;
            var tourMap = {};
            for (var i=0, length=tourList.length; i<length; i++) {
                var tourRow = tourList[i];
                tourMap[tourRow.tourId] = tourRow.shortName;
            }
            request.body.tourMap = tourMap;
            next();
        }else{
            response.json({
                respCode: responseCode.SOME_INTERNAL_ERROR,
                message: responseMessage[responseCode.SOME_INTERNAL_ERROR]
            })
        }

    });


}

module.exports = getTourListMap;
