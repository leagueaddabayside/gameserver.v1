var logger = require('../utils/logger.js').gameLogs;
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');
const userTeamService = require('../user_match_teams/services/index');
var moment = require("moment");


function getUserMatchTeamInfo(request, response, next) {
    var params = request.body;
    var requestObject = {};
    requestObject.matchId = params.matchId;
    requestObject.userId = params.userId;

    userTeamService.findUserTeamByPropertyApi(requestObject, function (err, userTeamResponse) {
        if (userTeamResponse.responseData) {
            var userTeamInfo = userTeamResponse.responseData;
            if(userTeamInfo.totalTeams){
                request.body.totalTeams = userTeamInfo.totalTeams;
                request.body.leagueCount = userTeamInfo.leagueCount;
            }else{
                request.body.totalTeams = 0;
                request.body.leagueCount = 0;
            }

            next();
        } else {
            response.json({
                respCode: responseCode.SOME_INTERNAL_ERROR,
                message: responseMessage[responseCode.SOME_INTERNAL_ERROR]
            })
        }

    });


}

module.exports = getUserMatchTeamInfo;
