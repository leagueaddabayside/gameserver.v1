var logger = require('../utils/logger.js').gameLogs;
var UserProfile = require("../redis/user_profile");
const responseMessage = require('../utils/response_message');
const responseCode = require('../utils/response_code');


function getUserInfo(request, response, next) {
    var reqParams = request.body;

    UserProfile.get(reqParams.userId, function (err, profile) {
        if (err) {
            response.json({
                respCode: responseCode.INVALID_USER_TOKEN,
                message: responseMessage[responseCode.INVALID_USER_TOKEN]
            })
            return;
        }
        if (profile) {
            request.body.profile = profile;
            next();
        } else {
            response.json({
                respCode: responseCode.INVALID_USER_TOKEN,
                message: responseMessage[responseCode.INVALID_USER_TOKEN]
            })
            return;
        }


    });

}
module.exports = getUserInfo;
