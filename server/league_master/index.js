var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addLeague",middleware.getAdminIdFromToken, obj.addLeague);
route.post("/updateLeague",middleware.getAdminIdFromToken, obj.updateLeague);
route.post("/findLeagueByProperty",middleware.getAdminIdFromToken, obj.findLeagueByProperty);
route.post("/findLeagueList",middleware.getAdminIdFromToken, obj.findLeagueList);
route.post("/fetchLeagues",middleware.getUserIdFromToken, obj.fetchLeagues);
route.post("/fetchLeagueInfo",middleware.getUserIdFromToken, obj.fetchLeagueInfoApi);
route.post("/fetchLeaguePrizeInfo", obj.fetchLeaguePrizeInfo);

// Routes
module.exports = route;
