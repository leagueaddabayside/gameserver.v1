var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var leagueService = require('./index');
var leagueConfigMaster = require('../../league_config_master/models/league_config_master');

async = require("async");
var moment = require("moment");
var _ = require("lodash");


function createMatchLeagues(requestObject, callback) {

    var query = leagueConfigMaster.findOne({configId: requestObject.configId});

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        if (data) {

            if (data.status == 'ACTIVE' || data.status == 'APPROVED') {

                createNewMatchLeague(data, function (err, leagueRes) {

                    if (err) {
                        responseObject.responseCode = responseCode.MONGO_ERROR;
                        callback(error, responseObject);
                        return;
                    }
                    var currentLeagueCount = data.leagueCount;
                    let maxLeagueCount = data.maxLeagueCount;
                    currentLeagueCount += 1;
                    if (currentLeagueCount == maxLeagueCount) {
                        data.status = 'FILLED';
                    }else{
                        data.status = 'ACTIVE';
                    }
                    data.leagueCount = currentLeagueCount;
                    data.save(function (err, saveResp) {
                        callback(err, leagueRes);
                        return;
                    });

                })
            } else {
                logger.info('league is not in active state', data);
                responseObject.responseCode = responseCode.LEAGUE_NOT_CREATED;
                callback(error, responseObject);
                return;
            }
        } else {
            responseObject.responseCode = responseCode.LEAGUE_CONFIG_DOES_NOT_EXIST;
            callback(error, responseObject);
            return;
        }

    })

}


function createNewMatchLeague(requestObject, responseCb) {
    var leagueObject = makeLeagueRequestData(requestObject);
    leagueObject.status = 'FILLING';
    console.log('addLeague', leagueObject);
    leagueService.addLeague(leagueObject, function (error, responseObject) {
        responseObject.responseCode = responseCode.SUCCESS;
        responseCb(null, responseObject);
        return;
    });
}


function makeLeagueRequestData(requestObject) {
    var leagueObject = {};
    leagueObject.title = requestObject.title;
    leagueObject.tourId = requestObject.tourId;
    leagueObject.matchId = requestObject.matchId;
    leagueObject.configId = requestObject.configId;
    leagueObject.maxTeams = requestObject.maxTeams;
    leagueObject.entryFee = requestObject.entryFee;
    leagueObject.winningAmt = requestObject.winningAmt;
    leagueObject.minTeams = requestObject.minTeams;
    leagueObject.winnersCount = requestObject.winnersCount;
    leagueObject.currentTeams = 0;
    leagueObject.isMultiEntry = requestObject.isMultiEntry;
    leagueObject.leagueType = requestObject.leagueType;
    leagueObject.chipType = requestObject.chipType;
    leagueObject.prizeInfo = requestObject.prizeInfo;
    return leagueObject;
}

module.exports = createMatchLeagues;