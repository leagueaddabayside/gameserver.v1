var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateLeague(requestObject, callback) {
	var updateObject = new Object();
	if(typeof requestObject.currentTeams !== 'undefined' && requestObject.currentTeams !== null)
		updateObject.currentTeams = requestObject.currentTeams;
	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	if(typeof requestObject.winningAmt !== 'undefined' && requestObject.winningAmt !== null)
		updateObject.winningAmt = requestObject.winningAmt;
	if(typeof requestObject.prizeInfo !== 'undefined' && requestObject.prizeInfo !== null)
		updateObject.prizeInfo = requestObject.prizeInfo;

	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {leagueId: requestObject.leagueId};
	leagueMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateLeague;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.currentTeams = "";
	requestObject.status = "";
	console.log(requestObject);

	updateLeague(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}