var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var leagueService = require('./index');

function updateLeagueCurrentTeamCount(requestObject, countType, callback) {
    logger.info('updateLeagueCurrentTeamCount', requestObject);

    var responseObject = new Object();
    var query = leagueMaster.findOne({leagueId: requestObject.leagueId});

    query.exec(function (error, leagueResponse) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        let currentTeams = leagueResponse.currentTeams;
        let maxTeams = leagueResponse.maxTeams;

        if (countType === 'CREDIT') {
            currentTeams += 1;
        }else if(countType === 'DEBIT'){
            currentTeams =currentTeams -1;
        }

        if (maxTeams - currentTeams == 1) {
            //create new League
            logger.info('create new league',leagueResponse);

            var requestObj = {};
            requestObj.configId = leagueResponse.configId;

            leagueService.createMatchLeagueService(requestObj,function(err){
                logger.info(err);
            });
        }

        leagueResponse.currentTeams = currentTeams;
        if (maxTeams == currentTeams) {
            leagueResponse.status = 'FILLED';
        }else{
            leagueResponse.status = 'FILLING';
        }
        leagueResponse.updateAt = new Date();
        leagueResponse.save(function (error, data) {
            if (error) {
                logger.error(error);
                responseObject.responseCode = responseCode.MONGO_ERROR;
                callback(error, responseObject);
                return;
            }

            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData = data;
            callback(null, responseObject);
        });


    });

}

module.exports = updateLeagueCurrentTeamCount;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 254;
    requestObject.userId = 311;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    console.log(requestObject);

    updateLeagueCurrentTeamCount(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}