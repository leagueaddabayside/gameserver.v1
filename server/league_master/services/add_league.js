var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addLeague(requestObject, callback) {
	var newLeague = new leagueMaster({
		title : requestObject.title,
		tourId : requestObject.tourId,
		matchId : requestObject.matchId,
		configId : requestObject.configId,
		maxTeams : requestObject.maxTeams,
		minTeams : requestObject.minTeams,
		chipType : requestObject.chipType,
		entryFee : requestObject.entryFee,
		winnersCount : requestObject.winnersCount,
		currentTeams : requestObject.currentTeams,
		isMultiEntry : requestObject.isMultiEntry,
		leagueType : requestObject.leagueType,
		prizeInfo : requestObject.prizeInfo,
		winningAmt : requestObject.winningAmt,
		status : requestObject.status
	});

	var responseObject = new Object();
	newLeague.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addLeague;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.title = "";
	requestObject.tourId = "";
	requestObject.matchId = "";
	requestObject.configId = "";
	requestObject.maxTeams = "";
	requestObject.entryFee = "";
	requestObject.winnersCount = "";
	requestObject.currentTeams = "";
	requestObject.isMultiEntry = "";
	requestObject.prizeInfo = "";
	requestObject.leagueType = "";
	requestObject.status = "";
	console.log(requestObject);

	addLeague(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}