var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addMultiLeagues(requestObject, callback) {
	var responseObject = new Object();
	leagueMaster.create(requestObject, function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addMultiLeagues;

// Unit Test Case
if (require.main === module) {
	var requestObject = [{title: "Rs.100", tourId: 2, matchId: 10, configId: 1, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.100", tourId: 2, matchId: 10, configId: 2, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.1000", tourId: 2, matchId: 10, configId: 3, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.200", tourId: 2, matchId: 10, configId: 4, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.500", tourId: 2, matchId: 10, configId: 5, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.1000", tourId: 2, matchId: 10, configId: 6, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.200", tourId: 2, matchId: 10, configId: 7, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.50", tourId: 2, matchId: 10, configId: 8, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'},
		{title: "Rs.30", tourId: 2, matchId: 10, configId: 9, maxTeams: 6,entryFee : 20,winnersCount : 1,currentTeams : 1,isMultiEntry : true,leagueType : 'CONFIRMED', status: 'FILLING'} ];

	console.log(requestObject);
	addMultiLeagues(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);
	});
}