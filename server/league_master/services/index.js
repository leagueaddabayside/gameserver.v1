var addLeagueService = require("./add_league");
var findLeagueByPropertyService = require("./find_league_by_property");
var findLeagueListService = require("./find_league_list");
var updateLeagueService = require("./update_league");
var fetchLeagueListService = require("./fetch_league_list");
var updateCurrentPointsLeague = require("./update_current_team_league_count");
var createMatchLeagueService = require("./create_match_leagues");

// Require

module.exports.addLeague = addLeagueService;
module.exports.findLeagueByProperty = findLeagueByPropertyService;
module.exports.findLeagueList = findLeagueListService;
module.exports.updateLeague = updateLeagueService
module.exports.fetchLeagueListService = fetchLeagueListService;
module.exports.updateCurrentPointsLeague = updateCurrentPointsLeague;
module.exports.createMatchLeagueService = createMatchLeagueService;

// Export
