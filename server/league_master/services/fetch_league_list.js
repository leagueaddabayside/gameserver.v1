var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var userLeagueService = require("../../user_joined_leagues/services/index");

function findLeagueList(requestObject, callback) {
	var query = leagueMaster.find({});
	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.configId !== "undefined" && requestObject.configId !== null)
		query.where("configId").equals(requestObject.configId);
	var responseObject = new Object();

	query.sort({winningAmt : -1,maxTeams : -1});
	userLeagueService.findUserJoinedListApi({userId : requestObject.userId ,matchId : requestObject.matchId},function(err,userLeaguesResponse){
		let joinedLeagueMap = new Map();
		var joinedLeagueArr = userLeaguesResponse.responseData;
		for (var i=0, length=joinedLeagueArr.length; i<length; i++) {
			var currentRow = joinedLeagueArr[i];
			if(joinedLeagueMap.has(currentRow.leagueId)){
				let teamArr = joinedLeagueMap.get(currentRow.leagueId);
				teamArr.push(currentRow.teamId);
			}else{
				let teamArr = [];
				teamArr.push(currentRow.teamId)
				joinedLeagueMap.set(currentRow.leagueId,teamArr);
			}
		}


		query.exec(function (error, data) {
			if (error) {
				logger.error(error);
				responseObject.responseCode = responseCode.MONGO_ERROR;
				callback(error, responseObject);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			var leagueArr = data;
			var leagueMap = new Map();
			for (var i=0, length=leagueArr.length; i<length; i++) {
				var currentRow = leagueArr[i];
				var currentObj = {};
				currentObj.leagueId = currentRow.leagueId;
				currentObj.title = currentRow.title;
				currentObj.tourId = currentRow.tourId;
				currentObj.matchId = currentRow.matchId;
				currentObj.configId = currentRow.configId;
				currentObj.maxTeams = currentRow.maxTeams;
				currentObj.entryFee = currentRow.entryFee;
				currentObj.winnersCount = currentRow.winnersCount;
				currentObj.currentTeams = currentRow.currentTeams;
				currentObj.isMultiEntry = currentRow.isMultiEntry;
				currentObj.prizeInfo = currentRow.prizeInfo;
				currentObj.leagueType = currentRow.leagueType;
				currentObj.chipType = currentRow.chipType;
				currentObj.status = currentRow.status;
				currentObj.winningAmt = currentRow.winningAmt;
				if(joinedLeagueMap.has(currentRow.leagueId)){
					if(currentRow.isMultiEntry){
						currentObj.leagueStatus = 'REJOIN';
						currentObj.joinedTeams = joinedLeagueMap.get(currentRow.leagueId);
					}else{
						currentObj.leagueStatus = 'JOINED';
					}
				}else{
					currentObj.leagueStatus = 'NOT_JOIN';
				}

				if(leagueMap.has(currentRow.configId)){
					var oldLeagueObj = leagueMap.get(currentRow.configId);
					if(currentRow.currentTeams > oldLeagueObj.currentTeams){
						leagueMap.set(currentRow.configId,currentObj);
					}
				}else{
					leagueMap.set(currentRow.configId,currentObj);

				}
			}
			responseObject.responseData = [...leagueMap.values()];
			callback(null, responseObject);
		});


	});

}

module.exports = findLeagueList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findLeagueList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}