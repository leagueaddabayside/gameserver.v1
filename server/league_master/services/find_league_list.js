var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueList(requestObject, callback) {
	var query = leagueMaster.find({});
	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.configId !== "undefined" && requestObject.configId !== null)
		query.where("configId").equals(requestObject.configId);

	if (typeof requestObject.leagueArr !== "undefined" && requestObject.leagueArr !== null)
		query.where("leagueId").in(requestObject.leagueArr);

	if (typeof requestObject.matchArr !== "undefined" && requestObject.matchArr !== null)
		query.where("matchId").in(requestObject.matchArr);

	if (typeof requestObject.statusArr !== "undefined" && requestObject.statusArr !== null)
		query.where("status").in(requestObject.statusArr);

	if (typeof requestObject.leagueType !== "undefined" && requestObject.leagueType !== null)
		query.where("leagueType").in(requestObject.leagueType);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var leagueArr = data;
			var leagueList = [];
			for (var i=0, length=leagueArr.length; i<length; i++) {
				var currentRow = leagueArr[i];
				var currentObj = {};
				currentObj.leagueId = currentRow.leagueId;
				currentObj.title = currentRow.title;
				currentObj.tourId = currentRow.tourId;
				currentObj.matchId = currentRow.matchId;
				currentObj.configId = currentRow.configId;
				currentObj.maxTeams = currentRow.maxTeams;
				currentObj.entryFee = currentRow.entryFee;
				currentObj.winnersCount = currentRow.winnersCount;
				currentObj.currentTeams = currentRow.currentTeams;
				currentObj.isMultiEntry = currentRow.isMultiEntry;
				currentObj.leagueType = currentRow.leagueType;
				currentObj.chipType = currentRow.chipType;
				currentObj.prizeInfo = currentRow.prizeInfo;
				currentObj.status = currentRow.status;
				currentObj.winningAmt = currentRow.winningAmt;
				leagueList.push(currentObj);
			}
			responseObject.responseData = leagueList;
		callback(null, responseObject);
	});
}

module.exports = findLeagueList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findLeagueList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}