var leagueMaster = require("../models/league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueByProperty(requestObject, callback) {
	var query = leagueMaster.findOne({});
	if (typeof requestObject.leagueId !== "undefined" && requestObject.leagueId !== null)
		query.where("leagueId").equals(requestObject.leagueId);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);
	if (typeof requestObject.configId !== "undefined" && requestObject.configId !== null)
		query.where("configId").equals(requestObject.configId);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.leagueId = currentRow.leagueId;
			currentObj.title = currentRow.title;
			currentObj.tourId = currentRow.tourId;
			currentObj.matchId = currentRow.matchId;
			currentObj.configId = currentRow.configId;
			currentObj.maxTeams = currentRow.maxTeams;
			currentObj.entryFee = currentRow.entryFee;
			currentObj.winnersCount = currentRow.winnersCount;
			currentObj.currentTeams = currentRow.currentTeams;
			currentObj.isMultiEntry = currentRow.isMultiEntry;
			currentObj.prizeInfo = currentRow.prizeInfo;
			currentObj.leagueType = currentRow.leagueType;
			currentObj.chipType = currentRow.chipType;
			currentObj.status = currentRow.status;
			currentObj.winningAmt = currentRow.winningAmt;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findLeagueByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.leagueId = "leagueId";
	//requestObject.matchId = "matchId";
	//requestObject.configId = "configId";

	findLeagueByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}