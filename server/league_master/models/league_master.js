require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var LeagueMasterSchema = new mongoose.Schema({
	leagueId: {type: Number, unique: true},
	title: {type: String},
	chipType: {type: String},
	tourId: {type: Number},
	matchId: {type: Number},
	configId: {type: Number},
	maxTeams: {type: Number},
	minTeams : {type : Number},
	entryFee: {type: Number},
	winningAmt : {type : Number},
	winnersCount: {type: Number},
	currentTeams: {type: Number},
	isMultiEntry: {type: Boolean},
	leagueType: {type: String, enum: ['CONFIRMED','NOT_CONFIRMED']},
	prizeInfo: {type: Array},
	status: {type: String, enum: ['FILLING', 'CANCELLED','CANCELLED_FAILED', 'FILLED','FREEZE', 'DONE','WINNING_FAILED']},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("LeagueMaster", LeagueMasterSchema, "league_master");

LeagueMasterSchema.plugin(autoIncrement.plugin, {
	model: "LeagueMaster",
	field: "leagueId",
	startAt: 1,
	incrementBy: 1
});