var logger = require("../../utils/logger").gameLogs;
var leagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function fetchLeaguePrizeInfo(request, response, next) {
	var requestObject = request.body;
	//console.log("findLeagueConfigByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	leagueService.findLeagueByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;

			let leagueObj = data.responseData;
			let prizeObj ={};

			prizeObj.winningAmt = leagueObj.winningAmt;
			prizeObj.winnersCount = leagueObj.winnersCount;
			let prizeList = [];
			for (var i=0, length=leagueObj.prizeInfo.length; i<length; i++) {
				var currentRow = leagueObj.prizeInfo[i];
				var pObj ={};
				pObj.prize = currentRow.prizeAmt;
				var rank = currentRow.startRank;
				if(currentRow.toRank && currentRow.toRank != null){
					rank = rank+' - '+currentRow.toRank;
				}
				pObj.rank =rank;
				prizeList.push(pObj);
			}

			prizeObj.prizeList = prizeList;
			responseObject.respData = prizeObj;
		}

		logger.info("fetchLeaguePrizeInfo API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchLeaguePrizeInfo;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.configId = "configId";
		//requestObject.title = "title";
		//requestObject.tourId = "tourId";
		//requestObject.startDate = "startDate";
		//requestObject.leagueCount = "leagueCount";
		//requestObject.maxLeagueCount = "maxLeagueCount";
		//requestObject.matchId = "matchId";
		//requestObject.maxTeams = "maxTeams";
		//requestObject.entryFee = "entryFee";
		//requestObject.winnersCount = "winnersCount";
		//requestObject.currentTeams = "currentTeams";
		//requestObject.isMultiEntry = "isMultiEntry";
		//requestObject.prizeInfo = "prizeInfo";
		//requestObject.leagueType = "leagueType";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchLeaguePrizeInfo(request, response);
	})();
}