var logger = require("../../utils/logger").gameLogs;
var leagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function fetchLeagueInfo(request, response, next) {
	var requestObject = request.body;
	//console.log("findLeagueByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	leagueService.findLeagueByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("findLeagueByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchLeagueInfo;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.leagueId = "leagueId";
		//requestObject.title = "title";
		//requestObject.tourId = "tourId";
		//requestObject.matchId = "matchId";
		//requestObject.configId = "configId";
		//requestObject.maxTeams = "maxTeams";
		//requestObject.entryFee = "entryFee";
		//requestObject.winnersCount = "winnersCount";
		//requestObject.currentTeams = "currentTeams";
		//requestObject.isMultiEntry = "isMultiEntry";
		//requestObject.prizeInfo = "prizeInfo";
		//requestObject.leagueType = "leagueType";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchLeagueInfo(request, response);
	})();
}