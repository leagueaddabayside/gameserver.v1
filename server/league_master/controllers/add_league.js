var logger = require("../../utils/logger").gameLogs;
var leagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addLeague(request, response, next) {
	var requestObject = request.body;
	//console.log("addLeague API :- Request - %j", requestObject);

	var responseObject = new Object();
	leagueService.addLeague(requestObject, function(error, data) {
	if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("addLeague API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addLeague;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.title = "";
		requestObject.tourId = "";
		requestObject.matchId = "";
		requestObject.configId = "";
		requestObject.maxTeams = "";
		requestObject.entryFee = "";
		requestObject.winnersCount = "";
		requestObject.currentTeams = "";
		requestObject.isMultiEntry = "";
		requestObject.prizeInfo = "";
		requestObject.leagueType = "";
		requestObject.status = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addLeague(request, response);
	})();
}