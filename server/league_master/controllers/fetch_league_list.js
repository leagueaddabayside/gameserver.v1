var logger = require("../../utils/logger").gameLogs;
var leagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function fetchLeagues(request, response, next) {
	var requestObject = request.body;
	requestObject.status = ['FILLING'];
	//console.log("fetchLeagues API :- Request - %j", requestObject);

	var responseObject = new Object();
	leagueService.fetchLeagueListService(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("fetchLeagues API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchLeagues;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchLeagues(request, response);
	})();
}