var addLeagueApi = require("./add_league");
var findLeagueByPropertyApi = require("./find_league_by_property");
var findLeagueListApi = require("./find_league_list");
var fetchLeaguesApi = require("./fetch_league_list");
var updateLeagueApi = require("./update_league");
var fetchLeagueInfoApi = require("./fetch_league_info");
var fetchLeaguePrizeInfoApi = require("./fetch_league_prize_info");

// Require

module.exports.addLeague = addLeagueApi;
module.exports.findLeagueByProperty = findLeagueByPropertyApi;
module.exports.findLeagueList = findLeagueListApi;
module.exports.fetchLeagues = fetchLeaguesApi;
module.exports.updateLeague = updateLeagueApi;
module.exports.fetchLeagueInfoApi = fetchLeagueInfoApi
module.exports.fetchLeaguePrizeInfo = fetchLeaguePrizeInfoApi;

// Export
