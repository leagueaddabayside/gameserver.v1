var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/userJoinedLeague",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.checkUserJoinLeagueProcessing,middleware.getUserInfo,middleware.getLeagueInfo,middleware.getTourMatchInfo,middleware.getPlayerMatchRedisInfo, obj.userJoinedLeagueApi);
route.post("/updateUserJoined",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getLeagueInfo,middleware.getTourMatchInfo, obj.updateUserJoinedApi);
route.post("/fetchJoinedLeagueTeams",middleware.getUserIdFromToken,middleware.getLeagueRankPointsRedis,middleware.getPlayerMatchTeamRedisPoints,middleware.getTourMatchInfo, obj.fetchJoinedLeagueTeamsApi);
route.post("/fetchUserJoinedLeagues",middleware.getUserIdFromToken, obj.fetchUserJoinedLeaguesApi);
route.post("/checkJoinedLeague",middleware.getUserIdFromToken,middleware.getUserInfo,middleware.getLeagueInfo,middleware.getTourMatchInfo,middleware.getPlayerMatchRedisInfo, obj.checkJoinedLeaguesApi);

// Routes
module.exports = route;
