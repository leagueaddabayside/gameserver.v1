require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var UserJoinedLeaguesSchema = new mongoose.Schema({
    leagueJoinedId: {type: Number, unique: true},
    matchId: {type: Number},
    userId: {type: Number},
    leagueConfigId : Number,
    leagueId: {type: Number},
    teamId: {type: Number},
    screenName : {type : String},
    points : Number,
    rank : Number,
    winningAmt : String,
    winningDate: {type: Date},
    winningType : {type: String, enum: ['WINNING','WINNING_REFUND']},
    winningStatus : {type: String, enum: ['PENDING','NOT_WINNER','CANCELLED', 'DONE']},
    joinTxnId : Number,
    amount : Number,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date}
});

module.exports = mongoose.model("UserJoinedLeagues", UserJoinedLeaguesSchema, "user_joined_leagues");

UserJoinedLeaguesSchema.plugin(autoIncrement.plugin, {
    model: "UserJoinedLeagues",
    field: "leagueJoinedId",
    startAt: 1,
    incrementBy: 1
});