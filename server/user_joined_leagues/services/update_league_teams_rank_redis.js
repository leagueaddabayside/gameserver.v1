var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var tourMatchService = require("../../tour_matches/services/index");
var userMatchTeamRedis = require("../../redis/user_match_team_points");
var leagueTeamRankRedis = require("../../redis/league_team_rank");

var async = require("async");

function updateLeagueTeamRanksRedis(requestObject, responseCb) {
    var query = UserJoinedLeagues.find({matchId: requestObject.matchId});

    var responseObject = new Object();
    query.exec(function (error, matchLeagueList) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        let leagueUserMap = new Map();
        userMatchTeamRedis.get(requestObject.matchId, function (err, userTeamPointObj) {

            if(!userTeamPointObj){
                logger.info('updateLeagueTeamRanksRedis final done');
                responseObject.responseCode = responseCode.USER_TEAM_POINTS_NOT_UPDATED;
                responseCb(null, responseObject);
                return;
            }
            logger.info('userTeamPointObj',userTeamPointObj);
            //logger.info('matchLeagueList',matchLeagueList);
            for (var i = 0, length = matchLeagueList.length; i < length; i++) {
                var userJoinedRow = matchLeagueList[i];
                let teamKey = userJoinedRow.userId + ':' + userJoinedRow.teamId;
                let teamPoints = userTeamPointObj[teamKey];
                logger.info('teamPoints',teamPoints);
                if (leagueUserMap.has(userJoinedRow.leagueId)) {
                    leagueUserMap.get(userJoinedRow.leagueId).push(teamPoints);
                    leagueUserMap.get(userJoinedRow.leagueId).push(userJoinedRow.leagueJoinedId);
                } else {
                    leagueUserMap.set(userJoinedRow.leagueId, [teamPoints, userJoinedRow.leagueJoinedId]);
                }

            }
            logger.info('leagueUserMap',leagueUserMap);

            async.eachSeries([...leagueUserMap.keys()],
                // 2nd param is the function that each item is passed to
                function (leagueId, callback) {
                    // logger.info('uploadSportsTour',item);
                    logger.info('leagueId leagueUserMap',leagueId,leagueUserMap.get(leagueId));
                    leagueTeamRankRedis.addLeaguePoints(leagueId, leagueUserMap.get(leagueId), function (error, responseObject) {
                        logger.error('addLeaguePoints',error);
                        callback(error, responseObject);
                        return;
                    })

                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    logger.info('updateLeagueTeamRanksRedis final done');
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseCb(err, responseObject);

                }
            );

        })

    })


}

module.exports = updateLeagueTeamRanksRedis;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 463;
    //requestObject.userId = 311;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    logger.info(requestObject);

    updateLeagueTeamRanksRedis(requestObject, function (error, responseObject) {
        logger.info("Response Code - ", responseObject);

    });
}