var UserJoinedLeagues = require("../models/user_joined_leagues");
var joinedLeagueService = require("../services/index");

var logger = require("../../utils/logger").gameLogs;
var constant = require("../../utils/constant");
var config = require('../../../bin/config');

var responseCode = require("../../utils/response_code");
var userMatchTeamService = require("../../user_match_teams/services/index");

var leagueService = require("../../league_master/services/index");
var gameTxnService = require("../../game_txn_master/services/index");
var request = require('request');
var moment = require("moment");

function userCancelLeague(requestObject, callback) {
    var responseObject = new Object();
    joinedLeagueService.findUserJoinedListApi(requestObject, function (err, userJoinedResponse) {
        if (err) {
            callback(err, responseCode.LEAGUE_DOES_NOT_EXIST);
            return;
        }
        logger.info('userCancelLeague ', requestObject);
        let matchInfo = requestObject.matchObj;
        if (userJoinedResponse.responseData && userJoinedResponse.responseData.length > 0) {

            let websiteRequestLeagues = [];
            // 1st para in async.eachSeries() is the array of items
            async.eachSeries(userJoinedResponse.responseData,
                // 2nd param is the function that each item is passed to
                function (userJoinedRow, cb) {
                    let txnReqObject = {};
                    txnReqObject.userId = userJoinedRow.userId;
                    txnReqObject.matchId = userJoinedRow.matchId;
                    txnReqObject.tourId = userJoinedRow.tourId;
                    txnReqObject.leagueId = userJoinedRow.leagueId;
                    txnReqObject.configId = userJoinedRow.leagueConfigId;
                    txnReqObject.amount = userJoinedRow.amount;
                    txnReqObject.remarks = matchInfo.shortName +' - '+matchInfo.relatedName;
                    txnReqObject.txnType = 'REFUND_LEAGUE';

                    var websiteServerRequest = {};
                    websiteServerRequest.userId = userJoinedRow.userId;
                    websiteServerRequest.joinTxnId = userJoinedRow.joinTxnId;
                    websiteServerRequest.amount = userJoinedRow.amount;
                    websiteServerRequest.remarks = txnReqObject.remarks;
                    websiteServerRequest.remarks = matchInfo.shortName +' - '+matchInfo.relatedName;
                    websiteServerRequest.matchName =  matchInfo.shortName;
                    websiteServerRequest.startTime = matchInfo.startTime;
                    websiteServerRequest.matchStatus = matchInfo.status;
                    //update league count
                    userMatchTeamService.updateTeamLeagueCountOnCancel(userJoinedRow,function(err,teamResp){
                        gameTxnService.addGameTxnService(txnReqObject, function (err, txnResponse) {
                            logger.info('addGameTxnService txn response', txnResponse);
                            txnReqObject.txnId = txnResponse.responseData.txnId;
                            websiteServerRequest.refTxnId = txnReqObject.txnId;
                            websiteRequestLeagues.push(websiteServerRequest);
                            cb(null, txnResponse);
                        });
                    });

                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    logger.info('final done');
                    if (err) {
                        logger.info('err final done', err);
                        responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                        callback(responseObject);
                        return;
                    }

                    let cancelRequestObject = {};
                    cancelRequestObject.accessKey = constant.SERVER_REQUEST_ACCESS_KEY;
                    cancelRequestObject.leagues = websiteRequestLeagues;
                    logger.info('request to website server', cancelRequestObject);

                    request.post({
                        url: config.WebsiteServerUrl + '/payments/cancelleague',
                        form: cancelRequestObject
                    }, function (error, responseData, body) {
                        var respData = JSON.parse(body);
                        //logger.info('getMatchScorecard',body);
                        logger.info('response from website server', respData);

                        if (error || respData.respCode !== 100) {
                            failedUserCancelLeague(websiteRequestLeagues, function (err, cancelResp) {
                                leagueService.updateLeague({
                                    leagueId: requestObject.leagueId,
                                    status: 'CANCELLED_FAILED'
                                }, function (err, updateResponse) {
                                    callback(err, updateResponse);
                                });
                            });
                        }

                        if (respData.respCode == 100) {
                            successCancelJoinLeague(respData.respData, function (err, cancelResp) {
                                leagueService.updateLeague({
                                    leagueId: requestObject.leagueId,
                                    status: 'CANCELLED'
                                }, function (err, updateResponse) {
                                    callback(err, updateResponse);
                                });
                            });
                        }





                    });
                }
            );

        } else {
            callback(null, userJoinedResponse);
        }


    })

}

function successCancelJoinLeague(cancelUserJoinedArr, callback) {
    var responseObject = new Object();


    async.eachSeries(cancelUserJoinedArr, function (cancelJoinedRow, cb) {
        let updateTxnObj = {};
        updateTxnObj.txnId = cancelJoinedRow.refTxnId;
        updateTxnObj.status = 'SUCCESS';
        updateTxnObj.tpTxnDate = new Date();
        updateTxnObj.tpTxnId = cancelJoinedRow.txnId;
        gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
            cb(null, txnResponse);
        });
    }, function (err) {
        logger.info('successCancelJoinLeague', err);
        responseObject.responseCode = responseCode.SUCCESS;
        callback(null, responseObject);
    });

}


function failedUserCancelLeague(websiteRequestLeagues, cb) {
    var responseObject = new Object();

    logger.info('failedUserCancelLeague');
    async.eachSeries(websiteRequestLeagues, function (cancelJoinedRow, cb) {
        let updateTxnObj = {};
        updateTxnObj.txnId = cancelJoinedRow.refTxnId;
        updateTxnObj.status = 'FAILED';
        updateTxnObj.tpTxnDate = new Date();
        gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
            cb(null, txnResponse);
        });
    }, function (err) {

        responseObject.responseCode = responseCode.CANCEL_JOINED_LEAGUE_ERROR;
        cb(null, responseObject);
    });

}


module.exports = userCancelLeague;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();


    requestObject.userId = 2;
    requestObject.matchId = 569;
    requestObject.leagueId = 1441;
    requestObject.teamId = 1;
    requestObject.profile = {
        balance: 5000,
        depositAmt: 1000,
        bonusAmt: 500,
        winningAmt: 3500
    }

    logger.info(requestObject);

    userCancelLeague(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}