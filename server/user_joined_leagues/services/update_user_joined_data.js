var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateUserJoinedData(requestObject, callback) {
	var updateObject = new Object();
	if(typeof requestObject.points !== 'undefined' && requestObject.points !== null)
		updateObject.points = requestObject.points;

	if(typeof requestObject.rank !== 'undefined' && requestObject.rank !== null)
		updateObject.rank = requestObject.rank;
	if(typeof requestObject.winningAmt !== 'undefined' && requestObject.winningAmt !== null)
		updateObject.winningAmt = requestObject.winningAmt;

	if(typeof requestObject.winningStatus !== 'undefined' && requestObject.winningStatus !== null)
		updateObject.winningStatus = requestObject.winningStatus;
	if(typeof requestObject.winningDate !== 'undefined' && requestObject.winningDate !== null)
		updateObject.winningDate = requestObject.winningDate;
	if(typeof requestObject.winningType !== 'undefined' && requestObject.winningType !== null)
		updateObject.winningType = requestObject.winningType;

	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {leagueJoinedId: requestObject.leagueJoinedId};
	UserJoinedLeagues.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateUserJoinedData;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.leagueJoinedId = 1;
	requestObject.points = 20;


	updateUserJoinedData(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}