var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var constant = require("../../utils/constant");

var leagueService = require("../../league_master/services/index");
var userTeamService = require("../../user_match_teams/services/index");

var UserProfile = require("../../redis/user_profile");
var moment = require("moment");


function checkJoinedLeague(requestObject, callback) {
    var responseObject = new Object();

    let leagueObject = requestObject.leagueInfo;
    let matchInfo = requestObject.matchInfo;
    let currentTime = moment(new Date()).unix();
    if (matchInfo.status !== 'ACTIVE' || matchInfo.startTime < currentTime) {
        responseObject.responseCode = responseCode.MATCH_NOT_ALLOWED_TO_JOIN_LEAGUE;
        callback(true, responseObject);
        return;
    }
 //   console.log('checkJoinedLeague forward',matchInfo,currentTime);

    var respData = {};
    if (leagueObject.status !== 'FILLING') {
        respData.matchId = leagueObject.matchId;
        responseObject.responseCode = responseCode.LEAGUE_ALREADY_FILLED;
        responseObject.responseData = respData;
        callback(true, responseObject);
        return;
    }
    requestObject.matchId = leagueObject.matchId;
    requestObject.isMultiEntry = leagueObject.isMultiEntry;
    userTeamService.getAvailableLeagueTeamApi(requestObject, function (err, userTeamResponse) {
        if (err) {
            respData.matchId = leagueObject.matchId;
            respData.chipType = leagueObject.chipType;
            respData.leagueId = leagueObject.leagueId;
            if(userTeamResponse.responseCode == responseCode.TEAM_DOES_NOT_EXIST){

                respData.totalTeams = userTeamResponse.responseData.totalTeams;
            }
            userTeamResponse.responseData = respData;
            callback(err, userTeamResponse);
            return;
        }
        //console.log('checkJoinedLeague forward',matchInfo,currentTime);

        var leagueCost = leagueObject.entryFee;

        respData.teamArr = userTeamResponse.responseData.teamArr;
        respData.totalTeams = userTeamResponse.responseData.totalTeams;
        var profile = requestObject.profile;

        //console.log('loaded user profile:', profile);


        var depositAmt = parseFloat(profile.depositAmt);
        var bonusAmt = parseFloat(profile.bonusAmt);
        var winningAmt = parseFloat(profile.winningAmt);
        let balance = depositAmt + bonusAmt + winningAmt;

        let maxBonusUsed = 0;
        logger.info('requestObject.bonusUsed', requestObject.bonusUsed);

        if (leagueObject.maxTeams >= constant.MIN_TEAM_TO_USE_BONUS) {
            let currentMatchUsed = parseFloat(requestObject.bonusUsed);
            maxBonusUsed = constant.MAX_BONUS_USED_PER_MATCH - currentMatchUsed;
        }
        let cashBonus = bonusAmt;
        if (bonusAmt > maxBonusUsed) {
            cashBonus = maxBonusUsed;
        }
        let validBalance = cashBonus + depositAmt + winningAmt;

        if (validBalance >= leagueCost) {
            responseObject.responseCode = responseCode.SUCCESS;

        } else {
            responseObject.responseCode = responseCode.SHOW_RECHARGE_POPUP;
            respData.validBalance = validBalance;
            respData.remainingAmt = parseFloat(leagueCost - validBalance).toFixed(2);
            respData.reasonRecharge = 'Cash Bonus cannot be used in private leagues and in leagues that have less than ' + constant.MIN_TEAM_TO_USE_BONUS + ' members. Cash Bonus usage limit is Rs.' + constant.MAX_BONUS_USED_PER_MATCH + ' per match.';
        }
        //logger.info('checkJoinedLeague forward',matchInfo,currentTime);
        respData.validBalance = validBalance;
        respData.teamId = requestObject.teamId;
        respData.balance = parseFloat(balance).toFixed(2);
        respData.leagueCost = leagueCost;
        respData.matchId = leagueObject.matchId;
        respData.chipType = leagueObject.chipType;
        respData.leagueId = leagueObject.leagueId;

        responseObject.responseData = respData;
        callback(null, responseObject);


    });


}

module.exports = checkJoinedLeague;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.tourId = "";

    logger.info(requestObject);

    checkJoinedLeague(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}