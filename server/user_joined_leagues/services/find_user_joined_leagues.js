var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var leagueService = require("../../league_master/services/index");

function findUserJoinedLeagues(requestObject, callback) {
	var query = UserJoinedLeagues.find({});

	if (typeof requestObject.userId !== "undefined" && requestObject.userId !== null)
		query.where("userId").equals(requestObject.userId);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);

	if (typeof requestObject.leagueId !== "undefined" && requestObject.leagueId !== null)
		query.where("leagueId").equals(requestObject.leagueId);
	if (typeof requestObject.winningStatus !== "undefined" && requestObject.winningStatus !== null)
		query.where("winningStatus").equals(requestObject.winningStatus);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			responseObject.responseData = data;
			responseObject.responseCode = responseCode.SUCCESS;
			callback(null, responseObject);

		});


}

module.exports = findUserJoinedLeagues;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tourId = "tourId";
	//requestObject.key = "key";
	//requestObject.identifyRoles.keeper = "identifyRoles.keeper";

	findUserJoinedLeagues(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);

	});
}