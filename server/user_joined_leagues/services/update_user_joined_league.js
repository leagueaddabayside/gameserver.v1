var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var UserTeamService = require("../../user_match_teams/services/index");
var moment = require("moment");

function updateUserJoinedApi(requestObject, callback) {
    logger.info('updateUserJoinedApi requestObject',requestObject);
    var updateObject = new Object();

    if (typeof requestObject.teamId !== 'undefined' && requestObject.teamId !== null) {
        updateObject.teamId = requestObject.teamId;
    }

    updateObject.updateAt = new Date();
    var responseObject = new Object();


    let matchInfo = requestObject.matchInfo;
    let currentTime = moment(new Date()).unix();
    if (matchInfo.status !== 'ACTIVE' || matchInfo.startTime < currentTime) {
        responseObject.responseCode = responseCode.MATCH_NOT_ALLOWED_TO_JOIN_LEAGUE;
        callback(true, responseObject);
        return;
    }



    var findquery = {leagueId: requestObject.leagueId,userId: requestObject.userId,teamId: requestObject.teamId};
    var updatequery = {leagueJoinedId: requestObject.leagueJoinedId};


    UserJoinedLeagues.findOne(findquery, function (err, userJoinedLeague) {
        if(userJoinedLeague){
            responseObject.responseCode = responseCode.LEAGUE_ALREADY_JOINED;
            callback(null, responseObject);
        }else{
            logger.info('findOneAndUpdate updatequery',updatequery);
            logger.info('findOneAndUpdate updateObject',updateObject);

            UserJoinedLeagues.findOneAndUpdate(updatequery, updateObject, function (error, data) {
                if (error) {
                    logger.error(error);
                    responseObject.responseCode = responseCode.MONGO_ERROR;
                    callback(error, responseObject);
                    return;
                }
                let userTeamRequest = {};
                userTeamRequest.newTeamId = requestObject.teamId;
                userTeamRequest.matchId = data.matchId;
                userTeamRequest.userId = requestObject.userId;
                userTeamRequest.oldTeamId = data.teamId;
                userTeamRequest.leagueId = requestObject.leagueId;

                UserTeamService.updateUserJoinedLeagueTeamApi(userTeamRequest,function(err,teamResp){
                    responseObject.responseCode = responseCode.SUCCESS;
                    callback(null, responseObject);
                });

            });
        }
    });




}

module.exports = updateUserJoinedApi;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.teamId = 2;
    requestObject.leagueJoinedId = 1;

    logger.info(requestObject);

    updateUserJoinedApi(requestObject, function (error, responseObject) {
        logger.info("Response Code - " , responseObject);

    });
}