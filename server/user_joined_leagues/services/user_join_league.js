var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var constant = require("../../utils/constant");
var config = require('../../../bin/config');

var responseCode = require("../../utils/response_code");
var userMatchTeamService = require("../../user_match_teams/services/index");

var leagueService = require("../../league_master/services/index");
var gameTxnService = require("../../game_txn_master/services/index");
var request = require('request');
var moment = require("moment");

function userJoinedLeague(requestObject, callback) {
    var responseObject = new Object();
    let matchInfo = requestObject.matchInfo;

    validateUserLeagueJoined(requestObject, function (err, validateResponse) {
        if (err) {
            callback(err, validateResponse);
            return;
        }
        logger.info('updateCurrentPointsLeague user join league', requestObject);
        leagueService.updateCurrentPointsLeague(requestObject, 'CREDIT', function (err, LeagueResponseObj) {
            logger.info('LeagueResponse', LeagueResponseObj);
            let LeagueResponse = LeagueResponseObj.responseData;
            requestObject.tourId = LeagueResponse.tourId;
            requestObject.configId = LeagueResponse.configId;
            requestObject.amount = LeagueResponse.entryFee;
            requestObject.txnType = 'JOIN_LEAGUE';

            if(LeagueResponse.chipType == 'skill'){
               return successPracticeUserJoinLeague(requestObject, callback);
            }



            gameTxnService.addGameTxnService(requestObject, function (err, txnResponse) {
                logger.info('addGameTxnService txn response', txnResponse);
                requestObject.txnId = txnResponse.responseData.txnId;
                var websiteServerRequest = {};
                websiteServerRequest.accessKey = constant.SERVER_REQUEST_ACCESS_KEY;
                websiteServerRequest.matchId = requestObject.matchId;
                websiteServerRequest.leagueId = requestObject.leagueId;
                websiteServerRequest.teamId = requestObject.teamId;
                websiteServerRequest.amount = LeagueResponse.entryFee;
                websiteServerRequest.userId = requestObject.userId;
                websiteServerRequest.refTxnId = txnResponse.responseData.txnId;
                websiteServerRequest.maxBonusUsed = requestObject.maxBonusUsed;
                websiteServerRequest.bonusUsed = requestObject.bonusUsed;
                websiteServerRequest.remarks = matchInfo.shortName +' - '+matchInfo.relatedName;
                websiteServerRequest.matchName =  matchInfo.shortName;
                websiteServerRequest.startTime = matchInfo.startTime;
                logger.info('request to website server', websiteServerRequest);
                requestObject.amount = LeagueResponse.entryFee;
                request.post({
                    url: config.WebsiteServerUrl + '/payments/joinleague',
                    form: websiteServerRequest
                }, function (error, responseData, body) {
                    if(error){
                        logger.info('server request error',error);
                        failedUserJoinLeague(requestObject, callback);
                        return;
                    }
                    var respData = JSON.parse(body);
                    //logger.info('getMatchScorecard',body);
                    logger.info('response from website server', respData);
                    if (respData.respCode == 100) {
                        successUserJoinLeague(requestObject, respData.respData, callback);
                    } else {
                        failedUserJoinLeague(requestObject,respData.respCode, callback);
                    }

                });

            });
        });


    })

}

function successUserJoinLeague(requestObject, tpTxnId, callback) {
    var responseObject = new Object();
    var newUserJoined = new UserJoinedLeagues({
        matchId: requestObject.matchId,
        userId: requestObject.userId,
        leagueConfigId: requestObject.configId,
        leagueId: requestObject.leagueId,
        teamId: requestObject.teamId,
        screenName: requestObject.profile.screenName,
        joinTxnId : requestObject.txnId,
        amount : requestObject.amount,
        points: 0,
        rank: 0
    });

    newUserJoined.save(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        userMatchTeamService.updateTeamLeagueCountApi(requestObject, function (err, updateLeagueResponse) {

            let updateTxnObj = {};
            updateTxnObj.txnId = requestObject.txnId;
            updateTxnObj.status = 'SUCCESS';
            updateTxnObj.tpTxnDate = new Date();
            updateTxnObj.tpTxnId = tpTxnId;
            gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
                responseObject.responseCode = responseCode.SUCCESS;
                responseObject.responseData = {leagueCount : updateLeagueResponse.responseData.leagueCount};
                callback(null, responseObject);
            });

        });

    });
}

function successPracticeUserJoinLeague(requestObject, callback) {
    var responseObject = new Object();
    var newUserJoined = new UserJoinedLeagues({
        matchId: requestObject.matchId,
        userId: requestObject.userId,
        leagueConfigId: requestObject.configId,
        leagueId: requestObject.leagueId,
        teamId: requestObject.teamId,
        screenName: requestObject.profile.screenName,
        points: 0,
        rank: 0
    });

    newUserJoined.save(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        userMatchTeamService.updateTeamLeagueCountApi(requestObject, function (err, updateLeagueResponse) {
            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData =  {leagueCount : updateLeagueResponse.responseData.leagueCount};
            callback(null, responseObject);

        });

    });
}

function failedUserJoinLeague(requestObject,respCode, callback) {
    var responseObject = new Object();

    logger.info('failedUserJoinLeague updateCurrentPointsLeague', requestObject);

    leagueService.updateCurrentPointsLeague(requestObject, 'DEBIT', function (err, LeagueResponse) {

        let updateTxnObj = {};
        updateTxnObj.txnId = requestObject.txnId;
        updateTxnObj.status = 'FAILED';
        updateTxnObj.tpTxnDate = new Date();
        gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
            if(respCode == 128){
                responseObject.responseCode = responseCode.USER_INSUFFICIENT_BALANCE;
            }else{
                responseObject.responseCode = responseCode.LEAGUE_JOIN_ERROR;
            }
            callback(null, responseObject);
        });
    });
}

function validateUserLeagueJoined(requestObject, callback) {
    var responseObject = new Object();
    var respData = {};
    let leagueObject = requestObject.leagueInfo;
    let matchInfo = requestObject.matchInfo;
    let currentTime = moment(new Date()).unix();
    if (matchInfo.status !== 'ACTIVE' || matchInfo.startTime < currentTime) {
        responseObject.responseCode = responseCode.MATCH_NOT_ALLOWED_TO_JOIN_LEAGUE;
        callback(true, responseObject);
        return;
    }


    if (leagueObject.status !== 'FILLING') {
        respData.matchId = leagueObject.matchId;
        responseObject.responseData = respData;
        responseObject.responseCode = responseCode.LEAGUE_ALREADY_FILLED;
        callback(true, responseObject);
        return;
    }
    requestObject.matchId = leagueObject.matchId;
    requestObject.isMultiEntry = leagueObject.isMultiEntry;
    requestObject.chipType = leagueObject.chipType;

    var leagueCost = leagueObject.entryFee;


    var profile = requestObject.profile;

    logger.info('loaded user profile:', profile);

    var depositAmt = parseFloat(profile.depositAmt);
    var bonusAmt = parseFloat(profile.bonusAmt);
    var winningAmt = parseFloat(profile.winningAmt);

    let maxBonusUsed = 0;
    if (leagueObject.maxTeams >= constant.MIN_TEAM_TO_USE_BONUS) {
        let currentMatchUsed = parseFloat(requestObject.bonusUsed);
        maxBonusUsed = constant.MAX_BONUS_USED_PER_MATCH - currentMatchUsed;
    }
    requestObject.maxBonusUsed = maxBonusUsed;
    let cashBonus = bonusAmt;
    if (bonusAmt > maxBonusUsed) {
        cashBonus = maxBonusUsed;
    }
    let validBalance = cashBonus + depositAmt + winningAmt;

    if (validBalance >= leagueCost) {
        respData.popup = 'CONF';
        requestObject.isMultiEntry = leagueObject.isMultiEntry;
        userMatchTeamService.checkuserTeamExistApi(requestObject, function (err, userTeamResponse) {
            if (err) {
                callback(true, userTeamResponse);
                return;
            }
            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData = respData;
            callback(null, responseObject);
            return;

        });

    } else {
        responseObject.responseCode = responseCode.SHOW_RECHARGE_POPUP;
        responseObject.responseData = respData;
        callback(true, responseObject);
        return;
    }


}


module.exports = userJoinedLeague;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();


    requestObject.userId = 2;
    requestObject.matchId = 569;
    requestObject.leagueId = 1441;
    requestObject.teamId = 1;
    requestObject.profile = {
        balance: 5000,
        depositAmt: 1000,
        bonusAmt: 500,
        winningAmt: 3500
    }

    logger.info(requestObject);

    userJoinedLeague(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}