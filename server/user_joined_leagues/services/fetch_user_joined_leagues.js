var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var leagueService = require("../../league_master/services/index");
async = require("async");
var leagueTeamRankRedis = require("../../redis/league_team_rank");

function fetchUserJoinedLeagues(requestObject, callback) {

    var joinedLeagueRequestObj = {};
    joinedLeagueRequestObj.userId = requestObject.userId;
    joinedLeagueRequestObj.matchId = requestObject.matchId;

    var query = UserJoinedLeagues.find(joinedLeagueRequestObj);

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var joinedLeagueArr = data;
        let joinedLeagueMap = new Map();


        async.eachSeries(joinedLeagueArr,
            // 2nd param is the function that each item is passed to
            function (currentRow, callback) {
                // console.log('uploadSportsTour',item);
                console.log('redis currentRow',currentRow);
                leagueTeamRankRedis.rankUser(currentRow.leagueId, currentRow.leagueJoinedId, function (err, resp) {
                    console.log('redis resp currentRow',resp);
                    if (resp.rank) {
                        currentRow.rank = resp.rank;
                    }
                    if (resp.points) {
                        currentRow.points = resp.points;
                    }

                    var currentObj = {};
                    currentObj.leagueJoinedId = currentRow.leagueJoinedId;
                    currentObj.matchId = currentRow.matchId;
                    currentObj.userId = currentRow.userId;
                    currentObj.leagueConfigId = currentRow.leagueConfigId;
                    currentObj.leagueId = currentRow.leagueId;
                    currentObj.teamId = currentRow.teamId;
                    currentObj.screenName = currentRow.screenName;
                    currentObj.points = currentRow.points;
                    currentObj.rank = currentRow.rank;


                    if (joinedLeagueMap.has(currentRow.leagueId)) {
                        var oldLeagueObj = joinedLeagueMap.get(currentRow.leagueId);
                        if (currentRow.rank < oldLeagueObj.rank && currentRow.rank !== 0) {
                            currentObj.teamArr = oldLeagueObj.teamArr;
                            currentObj.teamArr.push({
                                teamId: currentRow.teamId,
                                leagueJoinedId: currentRow.leagueJoinedId
                            });

                            joinedLeagueMap.set(currentRow.leagueId, currentObj);
                        } else {
                            oldLeagueObj.teamArr.push({
                                teamId: currentRow.teamId,
                                leagueJoinedId: currentRow.leagueJoinedId
                            });
                        }

                    } else {
                        var teamArr = [];
                        teamArr.push({teamId: currentRow.teamId, leagueJoinedId: currentRow.leagueJoinedId});
                        currentObj.teamArr = teamArr;
                        joinedLeagueMap.set(currentRow.leagueId, currentObj);
                    }


                    callback(error, resp);
                    return;
                });

            },
            // 3rd param is the function to call when everything's done
            function (err) {
                let statusArr = ['FILLING', 'FILLED','FREEZE', 'DONE','WINNING_FAILED'];
                leagueService.findLeagueList({leagueArr: [...joinedLeagueMap.keys()],statusArr : statusArr}, function (err, leagueResponseData) {
                    var leagueArr = leagueResponseData.responseData;
                    for (var i = 0, length = leagueArr.length; i < length; i++) {
                        var currentLeague = leagueArr[i];

                        if(joinedLeagueMap.has(currentLeague.leagueId)){
                            var userJoinedLeague = joinedLeagueMap.get(currentLeague.leagueId);
                            userJoinedLeague.title = currentLeague.title;
                            userJoinedLeague.maxTeams = currentLeague.maxTeams;
                            userJoinedLeague.currentTeams = currentLeague.currentTeams;
                            userJoinedLeague.chipType = currentLeague.chipType;
                            userJoinedLeague.leagueType = currentLeague.leagueType;
                            userJoinedLeague.configId = currentLeague.configId;
                            userJoinedLeague.isMultiEntry = currentLeague.isMultiEntry;
                            userJoinedLeague.winnersCount = currentLeague.winnersCount;
                            if(currentLeague.winningStatus == 'DONE'){
                                userJoinedLeague.winningAmt = currentLeague.winningAmt;
                            }
                            userJoinedLeague.status = currentLeague.status;
                        }

                    }

                    for (let [key, value] of joinedLeagueMap.entries()) {
                        if(!value.currentTeams){
                            joinedLeagueMap.delete(key);
                        }
                    }

                    responseObject.responseData = [...joinedLeagueMap.values()];
                    responseObject.responseCode = responseCode.SUCCESS;
                    callback(null, responseObject);


                });


            }
        );


    });
}

module.exports = fetchUserJoinedLeagues;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    fetchUserJoinedLeagues(requestObject, function (error, responseObject) {
        console.log("Response Code - ", responseObject);

    });
}