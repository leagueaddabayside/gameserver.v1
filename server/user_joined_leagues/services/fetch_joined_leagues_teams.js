var UserJoinedLeagues = require("../models/user_joined_leagues");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var userTeamService = require("../../user_match_teams/services/index");
var Constant = require("../../utils/constant");
let _ = require('lodash');

function fetchJoinedLeagueTeams(requestObject, callback) {
	var query = UserJoinedLeagues.find({});

	if (typeof requestObject.leagueId !== "undefined" && requestObject.leagueId !== null)
		query.where("leagueId").equals(requestObject.leagueId);

	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);
	query.sort('rank');
	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		var joinedLeagueTeamsArr = data;
		var leagueMembersList = [];
		var leagueUsersList = [];
		let leagueRankPointsMap = new Map();

		if(requestObject.leagueRankPoints) {
			leagueRankPointsMap = requestObject.leagueRankPoints;
		}

		for (var i=0, length=joinedLeagueTeamsArr.length; i<length; i++) {
			var currentRow = joinedLeagueTeamsArr[i];
			var currentObj = {};
			currentObj.leagueJoinedId = currentRow.leagueJoinedId;
			currentObj.matchId = currentRow.matchId;
			currentObj.userId = currentRow.userId;
			currentObj.leagueConfigId = currentRow.leagueConfigId;
			currentObj.leagueId = currentRow.leagueId;
			currentObj.teamId = currentRow.teamId;
			currentObj.screenName = currentRow.screenName;
			currentObj.points = currentRow.points;
			currentObj.rank = currentRow.rank;

			if(leagueRankPointsMap.has(currentRow.leagueJoinedId)){
				currentObj.points = leagueRankPointsMap.get(currentRow.leagueJoinedId).teamPoints;
				currentObj.rank = leagueRankPointsMap.get(currentRow.leagueJoinedId).rank;
			}


			if(currentRow.winningStatus === 'DONE'){
				currentObj.winningAmt = currentRow.winningAmt;
			}
			if(requestObject.userId == currentRow.userId){
				leagueUsersList.push(currentObj);
			}else{
				leagueMembersList.push(currentObj);
			}
		}
		let totalTeamCount = leagueMembersList.length;
		leagueUsersList = _.sortBy(leagueUsersList, ['rank','leagueJoinedId']);
		leagueMembersList = _.sortBy(leagueMembersList, ['rank','leagueJoinedId']);

		if (typeof requestObject.limit !== "undefined" && requestObject.limit !== null){
			let limitSize = requestObject.limitSize;
			if(requestObject.limitSize){
				limitSize = requestObject.limitSize;
			}
			leagueMembersList = leagueMembersList.splice(requestObject.limit-limitSize,limitSize);
		}

		var teamReqObj = {};
		teamReqObj.teamId = leagueUsersList[0].teamId;
		teamReqObj.matchId = requestObject.matchId;
		teamReqObj.userId = requestObject.userId;
		teamReqObj.matchInfo = requestObject.matchInfo;
		teamReqObj.matchTeamPoints = requestObject.matchTeamPoints;
		userTeamService.findUserTeamByPropertyApi(teamReqObj,function(err,teamResponse){
			//console.log('teamResponse',teamResponse);
			var respData ={};
			respData.leagueUsers = leagueUsersList;
			respData.leagueMembers = leagueMembersList;
			respData.totalTeamCount = totalTeamCount;
			respData.matchId = requestObject.matchId;
			respData.teamId = teamReqObj.teamId;
			respData.maxTeamAllowed = Constant.MAX_USER_TEAM_ALLOWED;
			respData.userTeam = teamResponse.responseData;
			responseObject.responseData = respData;
			responseObject.responseCode = responseCode.SUCCESS;
			callback(null, responseObject);
		});

	});
}

module.exports = fetchJoinedLeagueTeams;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tourId = "tourId";
	//requestObject.key = "key";
	//requestObject.identifyRoles.keeper = "identifyRoles.keeper";

	fetchJoinedLeagueTeams(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);

	});
}