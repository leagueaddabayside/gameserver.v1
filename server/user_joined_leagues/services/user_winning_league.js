var UserJoinedLeagues = require("../models/user_joined_leagues");
var joinedLeagueService = require("../services/index");

var logger = require("../../utils/logger").gameLogs;
var constant = require("../../utils/constant");
var config = require('../../../bin/config');

var responseCode = require("../../utils/response_code");
var userMatchTeamService = require("../../user_match_teams/services/index");

var leagueService = require("../../league_master/services/index");
var gameTxnService = require("../../game_txn_master/services/index");
var request = require('request');
var moment = require("moment");

function userWinningLeague(requestObject, callback) {
    var responseObject = new Object();
    requestObject.winningStatus = 'PENDING';
    joinedLeagueService.findUserJoinedListApi(requestObject, function (err, userJoinedResponse) {
        if (err) {
            callback(err, responseCode.LEAGUE_DOES_NOT_EXIST);
            return;
        }
        logger.info('userWinningLeague ', requestObject);
        let matchInfo = requestObject.matchObj;
        if (userJoinedResponse.responseData && userJoinedResponse.responseData.length > 0) {
            let totalWinningAmount = 0;
            let websiteRequestLeagues = [];
            let joinedRowMap = new Map();
            // 1st para in async.eachSeries() is the array of items
            async.eachSeries(userJoinedResponse.responseData,
                // 2nd param is the function that each item is passed to
                function (userJoinedRow, cb) {

                    let txnReqObject = {};
                    txnReqObject.userId = userJoinedRow.userId;
                    txnReqObject.matchId = userJoinedRow.matchId;
                    txnReqObject.tourId = userJoinedRow.tourId;
                    txnReqObject.leagueId = userJoinedRow.leagueId;
                    txnReqObject.teamId = userJoinedRow.teamId;
                    txnReqObject.configId = userJoinedRow.leagueConfigId;
                    txnReqObject.amount = userJoinedRow.winningAmt || 0;
                    txnReqObject.txnType = 'WINNING_LEAGUE';

                    var websiteServerRequest = {};
                    websiteServerRequest.userId = userJoinedRow.userId;
                    websiteServerRequest.amount = userJoinedRow.winningAmt;
                    websiteServerRequest.entryAmount = userJoinedRow.amount;
                    websiteServerRequest.winningType = userJoinedRow.winningType;
                    websiteServerRequest.joinTxnId = userJoinedRow.joinTxnId;
                    if(userJoinedRow.winningAmt){
                        totalWinningAmount += parseFloat(userJoinedRow.winningAmt);
                    }

                    websiteServerRequest.remarks = txnReqObject.remarks;
                    websiteServerRequest.leagueId = userJoinedRow.leagueId;
                    websiteServerRequest.teamId = userJoinedRow.teamId;
                    websiteServerRequest.matchId = matchInfo.matchId;
                    websiteServerRequest.remarks = matchInfo.shortName +' - '+matchInfo.relatedName;
                    websiteServerRequest.matchName =  matchInfo.shortName;
                    if (typeof matchInfo.startTime !== "undefined" && matchInfo.startTime !== null){
                        websiteServerRequest.startTime = moment(matchInfo.startTime).unix();
                    }

                    gameTxnService.addGameTxnService(txnReqObject, function (err, txnResponse) {
                        logger.info('addGameTxnService txn response', txnResponse);
                        if(err){
                            logger.error('addGameTxnService for WINNING_LEAGUE',err);
                            cb(err);
                        }
                        txnReqObject.txnId = txnResponse.responseData.txnId;
                        websiteServerRequest.refTxnId = txnReqObject.txnId;
                        websiteRequestLeagues.push(websiteServerRequest);
                        joinedRowMap.set(txnReqObject.txnId, userJoinedRow);
                        cb(null, txnResponse);
                    });

                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    logger.info('final done');
                    if (err) {
                        logger.info('err final done', err);
                        responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                        callback(responseObject);
                        return;
                    }


                    let leagueObject = requestObject.leagueObject;

                    if (leagueObject.leagueType === 'NOT_CONFIRMED') {
                        totalWinningAmount = leagueObject.winningAmt;
                    }
                    //entry rake amount for league
                    let joinLeagueAmt  = leagueObject.entryFee * leagueObject.currentTeams;

                    let rakeAmount  =  joinLeagueAmt - totalWinningAmount;
                    let txnType = 'DEBIT';
                    if(rakeAmount > 0){
                        txnType = 'CREDIT';
                    }

                    let websiteRakeTxnRequest = {};
                    websiteRakeTxnRequest.matchId = leagueObject.matchId;
                    websiteRakeTxnRequest.tourId = leagueObject.tourId;
                    websiteRakeTxnRequest.leagueId = leagueObject.leagueId;
                    websiteRakeTxnRequest.configId = leagueObject.configId;
                    websiteRakeTxnRequest.leagueAmt = joinLeagueAmt;
                    websiteRakeTxnRequest.winningAmt = totalWinningAmount;
                    websiteRakeTxnRequest.rakeAmt = rakeAmount;

                    let winningRequestObject = {};
                    winningRequestObject.accessKey = constant.SERVER_REQUEST_ACCESS_KEY;
                    winningRequestObject.leagues = websiteRequestLeagues;
                    winningRequestObject.rakeData = websiteRakeTxnRequest;

                    logger.info('request to website server', winningRequestObject);

                    request.post({
                        url: config.WebsiteServerUrl + '/payments/league/win',
                        form: winningRequestObject
                    }, function (error, responseData, body) {
                        var respData = JSON.parse(body);
                        //logger.info('getMatchScorecard',body);
                        logger.info('response from website server', respData);
                        if (error || respData.respCode !== 100) {
                            failedUserWinningLeague(websiteRequestLeagues, function (err, winningResp) {
                                leagueService.updateLeague({
                                    leagueId: requestObject.leagueId,
                                    status: 'WINNING_FAILED'
                                }, function (err, updateResponse) {
                                    callback(err, updateResponse);
                                });
                            });
                        }

                        if (respData.respCode == 100) {
                            successWinningJoinLeague(respData.respData, joinedRowMap, function (err, winningResp) {

                                let leagueObject = requestObject.leagueObject;


                                if (leagueObject.leagueType === 'NOT_CONFIRMED') {
                                    totalWinningAmount = leagueObject.winningAmt;
                                }
                                //entry rake amount for league
                                let joinLeagueAmt  = leagueObject.entryFee * leagueObject.currentTeams;

                                let rakeAmount  =  joinLeagueAmt - totalWinningAmount;
                                let txnType = 'DEBIT';
                                if(rakeAmount > 0){
                                    txnType = 'CREDIT';
                                }

                                let rakeTxnRequest = {};
                                rakeTxnRequest.matchId = leagueObject.matchId;
                                rakeTxnRequest.tourId = leagueObject.tourId;
                                rakeTxnRequest.leagueId = leagueObject.leagueId;
                                rakeTxnRequest.configId = leagueObject.configId;
                                rakeTxnRequest.joinLeagueAmt = joinLeagueAmt;
                                rakeTxnRequest.winningAmt = totalWinningAmount;
                                rakeTxnRequest.rakeAmount = rakeAmount;
                                rakeTxnRequest.txnType = txnType;
                                rakeTxnRequest.remarks = matchInfo.name + ' - ' + matchInfo.relatedName;
                                logger.info('addRakeTxnService',rakeTxnRequest);
                                gameTxnService.addRakeTxnService(rakeTxnRequest,function(err,rakeTxnResp){
                                    logger.info('err,rakeTxnResp',err,rakeTxnResp);
                                    leagueService.updateLeague({
                                        leagueId: requestObject.leagueId,
                                        status: 'DONE'
                                    }, function (err, updateResponse) {
                                        callback(err, updateResponse);
                                    });
                                });


                            });
                        }

                    });
                }
            );

        } else {
            callback(null, userJoinedResponse);
        }


    })

}

function successWinningJoinLeague(cancelUserJoinedArr, joinedRowMap, callback) {
    var responseObject = new Object();


    async.eachSeries(cancelUserJoinedArr, function (cancelJoinedRow, cb) {
        let updateTxnObj = {};
        updateTxnObj.txnId = cancelJoinedRow.refTxnId;
        updateTxnObj.status = 'SUCCESS';
        updateTxnObj.tpTxnDate = new Date();
        updateTxnObj.tpTxnId = cancelJoinedRow.txnId;
        gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
            logger.info(joinedRowMap);
            joinedLeagueService.updateUserJoinedDataApi({
                leagueJoinedId: joinedRowMap.get(parseInt(cancelJoinedRow.refTxnId)).leagueJoinedId,
                winningDate : new Date(),
                winningStatus: 'DONE'
            }, function (err) {
                cb(null, txnResponse);
            })

        });
    }, function (err) {
        logger.info('successCancelJoinLeague', err);
        responseObject.responseCode = responseCode.SUCCESS;
        callback(null, responseObject);
    });

}


function failedUserWinningLeague(websiteRequestLeagues, cb) {
    var responseObject = new Object();

    logger.info('failedUserCancelLeague');
    async.eachSeries(websiteRequestLeagues, function (cancelJoinedRow, cb) {
        let updateTxnObj = {};
        updateTxnObj.txnId = cancelJoinedRow.refTxnId;
        updateTxnObj.status = 'FAILED';
        updateTxnObj.tpTxnDate = new Date();
        gameTxnService.updateGameTxnService(updateTxnObj, function (err, txnResponse) {
            cb(null, txnResponse);
        });
    }, function (err) {

        responseObject.responseCode = responseCode.CANCEL_JOINED_LEAGUE_ERROR;
        cb(null, responseObject);
    });

}


module.exports = userWinningLeague;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();


    requestObject.leagueId = 4;


    logger.info(requestObject);

    userWinningLeague(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}