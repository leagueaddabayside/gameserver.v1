var logger = require("../../utils/logger").gameLogs;
var userLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function checkJoinedLeague(request, response, next) {
	var requestObject = request.body;
	var responseObject = {};
	userLeagueService.checkJoinedLeaguesApi(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
			responseObject.respData = data.responseData;
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("addUserTeam API :- Response - %j", responseObject);
		response.json(responseObject);
	});

}

module.exports = checkJoinedLeague;

// Unit Test Case
if (require.main === module) {
	(function() {
		UserProfile.get(2, function(err, profile) {
			if (err) {
				throw err;
			}
			console.log('loaded user profile:', profile);
		});
	})();
}