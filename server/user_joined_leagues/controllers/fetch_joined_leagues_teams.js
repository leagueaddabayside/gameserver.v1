var logger = require("../../utils/logger").gameLogs;
var userLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function fetchJoinedLeagueTeamsApi(request, response, next) {
	var requestObject = request.body;
	//console.log("fetchJoinedLeagueTeamsApi API :- Request - %j", requestObject);

	var responseObject = new Object();
	userLeagueService.fetchJoinedLeagueTeamsApi(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("fetchJoinedLeagueTeamsApi API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchJoinedLeagueTeamsApi;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();


		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchJoinedLeagueTeamsApi(request, response);
	})();
}