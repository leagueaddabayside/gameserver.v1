var logger = require("../../utils/logger").gameLogs;
var userLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var redisConn = require("../../redis/redisConnection");
var UserProfile = require("../../redis/user_profile");

function userJoinedLeague(request, response, next) {
    var requestObject = request.body;
    console.log("userJoinedLeague API :- Request - %j", requestObject);

    var responseObject = new Object();
    userLeagueService.userJoinedLeagueApi(requestObject, function (error, data) {
        var key = 'RUNNING:' + requestObject.userId + ':' + requestObject.leagueId;
        redisConn.del(key);

        if (data.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = data.responseCode;
            responseObject.respData = data.responseData;
            responseObject.message = responseMessage[data.responseCode];
            logger.info("userJoinedLeague API :- Response - %j", responseObject);
            response.json(responseObject);
            return;

        } else {
            let responseData = data.responseData;
            UserProfile.get(requestObject.userId, function (err, profile) {

                if (profile) {
                    let depositAmt = parseFloat(profile.depositAmt);
                    let bonusAmt = parseFloat(profile.bonusAmt);
                    let winningAmt = parseFloat(profile.winningAmt);
                    let balance = depositAmt + bonusAmt + winningAmt;
                    responseData.balance = parseFloat(balance).toFixed(2);

                }

                responseObject.respCode = data.responseCode;
                responseObject.respData = responseData;
                logger.info("addUserTeam API :- Response - %j", responseObject);
                response.json(responseObject);
            });


        }


    });
}

module.exports = userJoinedLeague;

// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        requestObject.tourId = "";

        requestObject.createBy = "";

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        userJoinedLeague(request, response);
    })();
}