var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addLeagueTemplate",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.addLeagueTemplate);
route.post("/updateLeagueTemplate",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.updateLeagueTemplate);
route.post("/findLeagueTemplateByProperty",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.findLeagueTemplateByProperty);
route.post("/findLeagueTemplateList",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.findLeagueTemplateList);
// Routes
module.exports = route;
