var addLeagueTemplateService = require("./add_league_template");
var findLeagueTemplateByPropertyService = require("./find_league_template_by_property");
var findLeagueTemplateListService = require("./find_league_template_list");
var updateLeagueTemplateService = require("./update_league_template");
// Require

module.exports.addLeagueTemplate = addLeagueTemplateService;
module.exports.findLeagueTemplateByProperty = findLeagueTemplateByPropertyService;
module.exports.findLeagueTemplateList = findLeagueTemplateListService;
module.exports.updateLeagueTemplate = updateLeagueTemplateService
// Export
