var league_templateMaster = require("../models/league_template_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateLeagueTemplate(requestObject, callback) {
	var updateObject = new Object();

	if(typeof requestObject.templateName !== 'undefined' && requestObject.templateName !== null)
		updateObject.templateName = requestObject.templateName;
	if(typeof requestObject.remarks !== 'undefined' && requestObject.remarks !== null)
		updateObject.remarks = requestObject.remarks;
	if(typeof requestObject.data !== 'undefined' && requestObject.data !== null)
		updateObject.data = requestObject.data;
	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();
	logger.info('updateLeagueTemplate ',updateObject);
	var responseObject = new Object();
	var query = {templateId: requestObject.templateId};
	league_templateMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateLeagueTemplate;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.templateName = "";
	requestObject.remarks = "";
	requestObject.data = "";
	requestObject.status = "";
	console.log(requestObject);

	updateLeagueTemplate(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}