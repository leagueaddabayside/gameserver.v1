var league_templateMaster = require("../models/league_template_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addLeagueTemplate(requestObject, callback) {
	var newLeagueTemplate = new league_templateMaster({
		templateName : requestObject.templateName,
		remarks : requestObject.remarks,
		data : requestObject.data,
		status : 'ACTIVE',
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newLeagueTemplate.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.DUPLICATE_LEAGUE_TEMPLATE;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addLeagueTemplate;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.templateName = "";
	requestObject.remarks = "";
	requestObject.data = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addLeagueTemplate(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}