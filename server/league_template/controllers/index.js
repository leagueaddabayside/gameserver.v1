var addLeagueTemplateApi = require("./add_league_template");
var findLeagueTemplateByPropertyApi = require("./find_league_template_by_property");
var findLeagueTemplateListApi = require("./find_league_template_list");
var updateLeagueTemplateApi = require("./update_league_template");
// Require

module.exports.addLeagueTemplate = addLeagueTemplateApi;
module.exports.findLeagueTemplateByProperty = findLeagueTemplateByPropertyApi;
module.exports.findLeagueTemplateList = findLeagueTemplateListApi;
module.exports.updateLeagueTemplate = updateLeagueTemplateApi
// Export
