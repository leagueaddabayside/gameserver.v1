require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var LeagueTemplateMasterSchema = new mongoose.Schema({
	templateId: {type: Number, unique: true},
	templateName: {type: String, unique: true},
	remarks: {type: String},
	data: {type: Object},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("LeagueTemplateMaster", LeagueTemplateMasterSchema, "league_template_master");

LeagueTemplateMasterSchema.plugin(autoIncrement.plugin, {
	model: "LeagueTemplateMaster",
	field: "templateId",
	startAt: 1,
	incrementBy: 1
});