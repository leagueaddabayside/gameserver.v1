/**
 * Created by sumit on 4/27/2017.
 */

var teamNames = {
    x: {displayTeamName: 'X'},
    y: {displayTeamName: 'Y'},
    srh: {displayTeamName: 'HYD'},
    mi: {displayTeamName: 'MUM'},
    kkr: {displayTeamName: 'KOL'},
    rcb: {displayTeamName: 'BAN'},
    gl: {displayTeamName: 'GUJ'},
    kxip: {displayTeamName: 'PNJ'},
    dd: {displayTeamName: 'DEL'},
    rps: {displayTeamName: 'PUN'},
    afg: {displayTeamName: 'AFG'},
    aus: {displayTeamName: 'AUS'},
    ban: {displayTeamName: 'BAN'},
    eng: {displayTeamName: 'ENG'},
    hk: {displayTeamName: 'HK'},
    ind: {displayTeamName: 'IND'},
    ire: {displayTeamName: 'IRE'},
    nam: {displayTeamName: 'NAM'},
    ned: {displayTeamName: 'NED'},
    nz: {displayTeamName: 'NZ'},
    omn: {displayTeamName: 'OMN'},
    pak: {displayTeamName: 'PAK'},
    rsa: {displayTeamName: 'RSA'},
    sct: {displayTeamName: 'SCT'},
    sl: {displayTeamName: 'SL'},
    tbc: {displayTeamName: 'TBC'},
    uae: {displayTeamName: 'UAE'},
    wi: {displayTeamName: 'WI'},
    zim: {displayTeamName: 'ZIM'}
};

module.exports.teamNames = teamNames;
