var _ = require('lodash');
var moment = require('moment');

function encryptMobileNumber(mobileNum) {
    var mobileNum = mobileNum + "";
    var length = mobileNum.length;

    var encryptMobileNum = _.repeat('*', length - 2);
    encryptMobileNum = encryptMobileNum + mobileNum.substr(length - 2);
    return encryptMobileNum;
}

function encryptEmailId(emailId) {
    var preEmailId = emailId.split('@')[0];
    var postEmailId = emailId.split('@')[1];
    var length = preEmailId.length;
    var encryptEmailId = _.repeat('*', length - 2);
    encryptEmailId = preEmailId.substr(0, 1) + encryptEmailId
        + preEmailId.substr(length - 1) + '@' + postEmailId;
    return encryptEmailId;
}

function getPlayerShortName(playerName) {
    let arr = playerName.trim().split(' ');
    let sName = '';
    if (arr.length == 1) {
        sName = arr[0];
    } else {
        for (let i = 0, length = arr.length; i < length; i++) {
            if (i == length - 1) {
                sName += ' '+arr[i];
            } else {
                sName += arr[i].charAt(0).toUpperCase();
            }
        }
    }
    return sName;
}

function checkDuplicateTeam(team1Arr,team2Arr) {
   let teamArr = [];
    team1Arr.forEach(function(player){
        let pObj = {};
        pObj.playerId = player.playerId;
        pObj.isCaptain = player.isCaptain;
        pObj.isViceCaptain = player.isViceCaptain;
        teamArr.push(pObj);
    });
    team2Arr.forEach(function(player){
        let pObj = {};
        pObj.playerId = player.playerId;
        pObj.isCaptain = player.isCaptain;
        pObj.isViceCaptain = player.isViceCaptain;
        teamArr.push(pObj);
    });

    let playerArr = _.uniqWith(teamArr, _.isEqual);
    console.log(playerArr.length);
    let isEqualTeam = false;
    if(playerArr.length == 11){
        isEqualTeam = true;
    }
    return isEqualTeam;
}


function getDateFromTimestampInSeconds(timestamp) {
    return moment(timestamp * 1000).toDate();
}

module.exports.encryptMobileNumber = encryptMobileNumber;
module.exports.encryptEmailId = encryptEmailId;
module.exports.getDateFromTimestampInSeconds = getDateFromTimestampInSeconds;
module.exports.getPlayerShortName = getPlayerShortName;
module.exports.checkDuplicateTeam = checkDuplicateTeam;

// Unit Test Case
if (require.main === module) {
    console.log(encryptMobileNumber('23423424'));
    console.log(encryptEmailId('sumit.singla@gaussnetworks.com'));
}