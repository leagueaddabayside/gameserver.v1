var logger = require("./logger").gameLogs;
var config = require('../../bin/config').mongo;

var mongoose = require( 'mongoose');
var autoIncrement = require( 'mongoose-auto-increment');
var connection = mongoose.connect('mongodb://'+config.host+'/fantasy_sports');

console.log("mongo conn:",connection);

autoIncrement.initialize(connection);
var db = mongoose.connection;
db.on("error", function(err){
    logger.error(err);
});