/**
 * Created by sumit on 1/16/2017.
 */
'use strict';


//var config = require("../config/config");

var bunyan = require('bunyan');
var pathModule = require('path');


let gameLogs = new bunyan({
    src: true,
    name: 'GameEngine',
    streams: [/*{
        level: 'trace',
        stream: process.stdout
    },*/
        {
            type: 'rotating-file',
            level: 'trace',
            path: pathModule.join(__dirname, '../../logs/common-trace.log'),  // log trace and above to a file,
            period: '1d',   // daily rotation
            count: 30,        // keep 3 back copies
        },
        {
            type: 'rotating-file',
            level: 'error',
            path: pathModule.join(__dirname, '../../logs/common-error.log'),  // log ERROR and above to a file,
            period: '1d',   // daily rotation
            count: 30,        // keep 3 back copies
        }]
});


let requestLogger = new bunyan({
    src: true,
    name: 'requestLogger',
    streams: [/*{
        level: 'trace',
        stream: process.stdout
    }*/,
        {
            type: 'rotating-file',
            level: 'trace',
            path: pathModule.join(__dirname, '../../logs/request-logger.log'),  // log trace and above to a file,
            period: '1d',   // daily rotation
            count: 30,        // keep 3 back copies
        }]
});

module.exports.gameLogs =  gameLogs;
module.exports.requestLogs = requestLogger;

//	Unit Test Case
if (require.main === module) {
    gameLogs.info({"two" : 2},"hello test",{one : '1'});
}