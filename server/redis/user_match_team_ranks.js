/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redisConnection');

exports.set = setUserMatchData;
exports.get = getUserMatchData;

function setUserMatchData(userId,matchId, userMatchData, cb) {
    redis.hmset(key(userId,matchId) + userId, userMatchData, cb);
}

function getUserMatchData(userId,matchId, cb) {
    redis.hgetall(key(userId,matchId) + userId, cb);
}
function key(userId,matchId) {
    return 'um:' + userId +':'+matchId;
}
/*
redis.hget('profile:' + userId, 'email', function(err, email) {
    if (err) {
        handleError(err);
    }
    else {
        console.log('User email:', email);
    }
});*/
// Unit Test Case
if (require.main === module) {
    (function() {
        setUserMatchData(2,11,{teamIds : '10,11'}, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });

        getUserMatchData(2,11, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });
    })();
}