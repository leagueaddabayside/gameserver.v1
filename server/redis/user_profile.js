/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redisConnection');

exports.get = getUserProfile;


function getUserProfile(userId, cb) {
    redis.hgetall( userId, cb);
}

/*
redis.hget('profile:' + userId, 'email', function(err, email) {
    if (err) {
        handleError(err);
    }
    else {
        console.log('User email:', email);
    }
});*/
