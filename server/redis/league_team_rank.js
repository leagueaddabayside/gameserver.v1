/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redisConnection');
var _ = require('lodash');
const keyValidity = 345600; //4days

exports.addLeagueTeamPoints = addLeagueTeamPoints;

function addLeagueTeamPoints(leagueId, joinedId, teamPoints, cb) {
    // redis.zincrby(key(game), diff, player, cb);
    redis.zadd(key(leagueId), teamPoints, joinedId, cb);
    redis.expire(key(leagueId), keyValidity);
}

function addLeaguePoints(leagueId, joinedUserArr, cb) {
    // redis.zincrby(key(game), diff, player, cb);
    redis.zadd(key(leagueId), joinedUserArr, cb);
    redis.expire(key(leagueId), keyValidity);
}
exports.addLeaguePoints = addLeaguePoints;


exports.rank = getRank;
exports.rankUser = getRankByUser;

function getRank(leagueId, limit, cb) {
    redis.zrevrange(key(leagueId), 0, limit, "WITHSCORES", function (err, ret) {
        if (err) {
            cb(err);
        }
        else {
            var index = 0, prevValue = -1;
            let rankMap = new Map();
            var rowCount = 0;
            //console.log(ret);
            for (var i = 0; i < ret.length; i += 2) {
                rowCount++;
               //console.log(i,ret[i],parseInt(ret[i]));
                var obj = {leagueJoinedId: parseInt(ret[i]), teamPoints: parseFloat(ret[i + 1])};
                if (prevValue != ret[i + 1]) {
                    index = rowCount;
                }
                prevValue = ret[i + 1];
                obj.rank = index;
                //console.log(obj);
                rankMap.set(obj.leagueJoinedId,obj);
            }
            cb(null, rankMap);
        }
    });
}

function getRankByUser(leagueId, leagueJoinedId, cb) {
    let joinData = {};
    redis.ZREVRANK(key(leagueId), leagueJoinedId, function (err, rankResp) {
        //console.log('rankResp',rankResp);
        if (typeof rankResp !== "undefined" && rankResp !== null){
            joinData.rank = parseInt(rankResp)+1;
            redis.zscore(key(leagueId), leagueJoinedId, function (err, scoreResp) {
                //console.log('scoreResp',scoreResp);
                if (typeof scoreResp !== "undefined" && scoreResp !== null){
                    joinData.points = scoreResp;
                    redis.zrevrangebyscore(key(leagueId), scoreResp,scoreResp, function (err, scoreRangeResp) {
                        //console.log('scoreRangeResp',scoreRangeResp,joinData.rank);
                        for(let i=0;i < scoreRangeResp.length;i++){
                            if(scoreRangeResp[i] == leagueJoinedId){
                                break;
                            }else{
                                if(joinData.rank !== 0){
                                    joinData.rank --;
                                }
                            }
                        }
                        cb(null,joinData);
                    });
                }else{
                    cb(null,joinData);
                }
            });

        }else{
            cb(null,joinData);
        }
    });

}


function key(leagueId) {
    //console.log('leagueId',leagueId);
    return 'league:' + leagueId;
}

if (require.main === module) {
    (function () {

        /*setInterval(function () {
            var player = 'player' + Math.floor(Math.random() * 100);
            addLeagueTeamPoints(845, player, Math.floor(Math.random() * 10), function (err) {
                if (err) {
                    throw err;
                }
            });
        }, 1e2);*/

        var leagueId = 11;


       /* addLeagueTeamPoints(leagueId,169,65,function(){

        })*/

        getRank(leagueId,-1,function(err,resp){
            console.log('rank',resp);
        });

        getRankByUser(leagueId,'11',function(err,resp){
            console.log('getRankByUser',resp);
        });

        addLeaguePoints(leagueId, [0,11,2,5,2,1,0,14], function(){

        });



      /*  redis.zrevrange(key(leagueId), 0, -1, "WITHSCORES", function (err, ret) {
            console.log('zrange:\n', ret);
        });*/

       /* redis.zrevrank(key(leagueId), 'l2', function (err, ret) {
            console.log('zrank:\n', ret);
        });*/

        /*redis.zrevrangebyscore(key(leagueId), '1000','100', function (err, ret) {
            console.log('zrevrangebyscore:\n', ret);
        });

        redis.zscore(key(leagueId), '227', function (err, ret) {
            console.log('zscore:\n', ret);
        });

        redis.ZREVRANK(key(leagueId), '169', function (err, ret) {
            console.log('ZREVRANK:\n', ret);
        });*/

        /*        rank(leagueId, 10, function (err, ranks) {
                    if (err) {
                        throw err;
                    }
                    console.log('ranking:\n', ranks);
                    var arr = _.slice(ranks, 4, 6)

                    console.log('ranking:\n', arr);
                });*/
    })();
}