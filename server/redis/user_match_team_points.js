/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redisConnection');
const keyValidity = 345600; //4days

exports.set = setUserMatchTeamData;
exports.get = getUserMatchTeamData;
exports.getTeamPoints = getUserMatchTeamPoints;

function setUserMatchTeamData(matchId, userMatchTeamData, cb) {
    redis.hmset(key(matchId), userMatchTeamData, cb);
    redis.expire(key(matchId), keyValidity);
}

function getUserMatchTeamData(matchId, cb) {
    redis.hgetall(key(matchId), cb);
}

function getUserMatchTeamPoints(matchId,userId,teamId, cb) {
    redis.hget(key(matchId),userId+':'+ teamId, cb);
}

function key(matchId) {
    return 'teams:' + matchId ;
}
/*
redis.hget('profile:' + userId, 'email', function(err, email) {
    if (err) {
        handleError(err);
    }
    else {
        console.log('User email:', email);
    }
});*/
// Unit Test Case
if (require.main === module) {
    (function() {
        let obj = {};
        let key = 12+':'+1;
        obj[key] = 123;
       /* setUserMatchTeamData(2,obj, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });*/

        getUserMatchTeamData(91, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });

        getUserMatchTeamPoints(2,12,1, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });
    })();
}