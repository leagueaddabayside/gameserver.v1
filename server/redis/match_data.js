/**
 * Created by sumit on 3/3/2017.
 */
var redis = require('./redisConnection');

exports.set = setMatchData;
exports.get = getMatchData;

function setMatchData(matchId, matchData, cb) {
    redis.hmset(key(matchId), matchData, cb);
}

function getMatchData(matchId, cb) {
    redis.hgetall(key(matchId), cb);
}
function key(matchId) {
    return 'match:' + matchId ;
}
/*
redis.hget('profile:' + userId, 'email', function(err, email) {
    if (err) {
        handleError(err);
    }
    else {
        console.log('User email:', email);
    }
});*/
// Unit Test Case
if (require.main === module) {
    (function() {
        setUserMatchData(2,11,{teamIds : '10,11'}, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });

        getUserMatchData(2,11, function(err, profile) {
            if (err) {
                throw err;
            }
            console.log('loaded user profile:', profile);
        });
    })();
}