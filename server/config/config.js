'use strict';

module.exports = {
    app: {
        title: 'Cricket Fantasy',
        description: 'Full-Stack JavaScript App with MongoDB, Express, and Node.js',
    },
    port: process.env.PORT || 3000,
    sessionSecret: 'secret',
    uploads: {},
    push: {
        domain: 'https://android.googleapis.com/gcm/send/',
        ttl: 60,
        server_key: 'AAAA3AYP4yQ:APA91bFpqg9JOsYB7kaNQL-7HHJGiD3IDRCzc5gkiUPDAbQoYqQg9fDJoJYkRyamo6AbiMjNHidc805gMfsWWfaeXbieZ72DAbFid99TK8aSLiRmEpUok1kSgLJSqZc_2b5WyAYOq8DHBO7Ygsy8_u2-6LJq_lkFeg'
    }
};
