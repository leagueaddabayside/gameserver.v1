var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addLeagueConfig",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.addLeagueConfig);
route.post("/updateLeagueConfig",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.updateLeagueConfig);
route.post("/findLeagueConfigByProperty",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.findLeagueConfigByProperty);
route.post("/findLeagueConfigList",middleware.getAdminIdFromToken, obj.findLeagueConfigList);
route.post("/createMatchDeafultLeague",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.createMatchDeafultLeague);
route.post("/createMatchSpecialLeague",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.createMatchSpecialLeagueApi);
route.post("/findLeagueConfigListReport",middleware.getAdminIdFromToken, obj.findLeagueConfigListReportApi);


// Routes
module.exports = route;
