var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var responseMessage = require("../../utils/response_message");
var leagueConfigService = require('./index');
var leagueService = require('../../league_master/services/index');

async = require("async");
var moment = require("moment");
var _ = require("lodash");

function createMatchLeagueConfigs(requestObject, responseCb) {

    logger.info('createMatchLeagueConfigs:', requestObject);
    var responseObject = new Object();
    // 1st para in async.eachSeries() is the array of items
    async.eachSeries(requestObject.templateList,
        // 2nd param is the function that each item is passed to
        function (item, callback) {
            //console.log('create match league config', item);
            item.matchId = requestObject.matchId;
            item.tourId = requestObject.tourId;
            createMatchLeagueConfig(item, function (error, responseObject) {
                callback(error, responseObject);
                return;
            })

        },
        // 3rd param is the function to call when everything's done
        function (err) {
            // All tasks are done now
            logger.info('createMatchLeagues final done');
            responseObject.responseCode = responseCode.SUCCESS;
            responseCb(null, responseObject);

        }
    );
}

function createMatchLeagueConfig(requestObject, responseCb) {
    var leagueConfigRequestObject = new Object();
    leagueConfigRequestObject.tourId = requestObject.tourId;
    leagueConfigRequestObject.matchId = requestObject.matchId;
    leagueConfigRequestObject.templateId = requestObject.templateId;

    leagueConfigService.findLeagueConfigByProperty(leagueConfigRequestObject, function (error, leagueConfigResponseObject) {
        if (error) {
            logger.error(error);
            responseCb(error);
            return;
        }
        logger.info('leagueConfigResponseObject', leagueConfigResponseObject);
        var leagueConfigObject = makeLeagueConfigRequestData(requestObject);

        if (leagueConfigResponseObject.responseData && leagueConfigResponseObject.responseData.configId) {

            if(requestObject.configStatus == 'ACTIVE'){
                leagueService.createMatchLeagueService(leagueConfigResponseObject.responseData,function(err,leagueRes){
                    responseCb(err, leagueRes);
                    return;
                })
            }else{
                responseCb(error, leagueConfigResponseObject);
                return;
            }




            /*leagueConfigService.updateLeagueConfig(playerObj, function (error, responseObject) {
             responseObject.responseCode = responseCode.SUCCESS;
             responseCb(null, responseObject);
             return;
             });*/

        } else {
            leagueConfigObject.status = requestObject.configStatus;
            logger.info('addLeagueConfig', leagueConfigObject);
            leagueConfigService.addLeagueConfig(leagueConfigObject, function (error, responseObject) {
                logger.info('addLeagueConfig responseObject',responseObject);
                if(requestObject.configStatus == 'ACTIVE'){
                    leagueService.createMatchLeagueService(responseObject.responseData,function(err,leagueRes){
                        responseCb(err, leagueRes);
                        return;
                    })
                }else{
                    responseCb(error, responseObject);
                    return;
                }


            });
        }

    })

}




function makeLeagueConfigRequestData(requestObject) {
    var leagueConfigObject = {};
    leagueConfigObject.tourId = requestObject.tourId;
    leagueConfigObject.matchId = requestObject.matchId;
    leagueConfigObject.templateId = requestObject.templateId;
    if(requestObject.data){
        var templateData = requestObject.data;
        if(templateData.chipType === 'real'){
            leagueConfigObject.title = 'Win Rs.'+templateData.prizeDetails.winningAmt;
            leagueConfigObject.winnersCount = templateData.prizeDetails.prizeListCount;
            leagueConfigObject.prizeInfo = templateData.prizeDetails.prizePoolList;
            leagueConfigObject.totalPoolAmount = templateData.prizeDetails.totalPrizePoolAmt;
            leagueConfigObject.winningAmt = templateData.prizeDetails.winningAmt;

        }else{
            leagueConfigObject.title = 'Practice League';
            leagueConfigObject.winnersCount = 0;

        }
        leagueConfigObject.startDate = requestObject.startDate;
        leagueConfigObject.leagueCount =0;
        leagueConfigObject.maxLeagueCount = templateData.maxLeagueCreate;
        leagueConfigObject.maxTeams = templateData.maxTeamRegister;
        leagueConfigObject.entryFee = templateData.entryAmt;
        leagueConfigObject.minTeams = templateData.thresholdTeam;
        leagueConfigObject.chipType = templateData.chipType;
        leagueConfigObject.commisionRate = templateData.commisionRate;

        if(templateData.isMultiTeamAllowed){
            leagueConfigObject.isMultiEntry = true;
        }else{
            leagueConfigObject.isMultiEntry = false;

        }
        if(templateData.isNotConfirmedLeague){
            leagueConfigObject.leagueType = 'NOT_CONFIRMED';
        }else{
            leagueConfigObject.leagueType = 'CONFIRMED';
        }


    }
    return leagueConfigObject;


}

module.exports = createMatchLeagueConfigs;