var league_configMaster = require("../models/league_config_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateLeagueConfig(requestObject, callback) {

	logger.info('updateLeagueConfig requestObject',requestObject);

	var updateObject = new Object();
	if(typeof requestObject.currentTeams !== 'undefined' && requestObject.currentTeams !== null)
		updateObject.currentTeams = requestObject.currentTeams;
	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	logger.info('updateLeagueConfig',updateObject);
	var responseObject = new Object();
	var query = {configId: requestObject.configId};
	league_configMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateLeagueConfig;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.currentTeams = "";
	requestObject.status = "";
	console.log(requestObject);

	updateLeagueConfig(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}