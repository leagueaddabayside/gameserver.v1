var logger = require("../../utils/logger").gameLogs;
var leagueConfigService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueService = require('../../league_master/services/index');
var tourMatchService = require('../../tour_matches/services/index');
var async = require("async");
var moment = require("moment");

function createMatchSchedulesLeague(request, cb) {
    var requestObject = request.body;
    //console.log("addLeagueConfig API :- Request - %j", requestObject);

    var responseObject = new Object();
    var status = ['ACTIVE'];
    tourMatchService.findTourMatchesList({status: status}, function (err, matchListReponseData) {
        if (err) {
            responseObject.respCode = responseCode.MONGO_ERROR;
            responseObject.message = responseMessage[responseCode.MONGO_ERROR];
            cb(err, responseObject);
            return;
        }
        // console.log('matchListReponseData', matchListReponseData);
        let matchList = matchListReponseData.responseData;
        let matchArr = [];
        for (let i = 0, length = matchList.length; i < length; i++) {
            let matchRow = matchList[i];
            if (matchRow.status === 'ACTIVE') {
                if (typeof matchRow.startTime !== "undefined" && matchRow.startTime !== null) {
                    let currentTime = new Date();
                    if (moment(matchRow.matchStartTime).isBefore(currentTime)) {
                        logger.info('update match status from active to running', matchRow);
                        matchRow.status = 'RUNNING';
                        tourMatchService.updateTourMatches({
                            matchId: matchRow.matchId,
                            status: 'RUNNING'
                        }, function (err, resp) {
                            logger.info(err, resp);
                        })
                    }
                }
            } else {
                matchArr.push(matchRow.matchId);
            }

        }

        //console.log('matchArr', matchArr);
        leagueConfigService.findLeagueConfigList({
            matchArr: matchArr,
            status: 'APPROVED',
            startDate: new Date()
        }, function (err, configReponseData) {

            logger.info('configReponseData', configReponseData);
            // 1st para in async.eachSeries() is the array of items
            async.eachSeries(configReponseData.responseData,
                // 2nd param is the function that each item is passed to
                function (item, callback) {
                    logger.info('create match league config', item);


                    var requestObj = {};
                    requestObj.configId = item.configId;

                    leagueService.createMatchLeagueService(requestObj, function (error, data) {
                        if (error) {
                            responseObject.respCode = data.responseCode;
                            responseObject.message = responseMessage[data.responseCode];
                            callback(error, data);
                            return;
                        }
                        responseObject.respCode = data.responseCode;
                        responseObject.responseData = data.responseData;
                        callback(error, data);
                        return;

                    });

                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    logger.info('findLeagueConfigList final done');
                    responseObject.respCode = responseCode.SUCCESS;
                    logger.info("createMatchSchedulesLeague API :- Response - %j", responseObject);
                    cb(null, responseObject);

                }
            );


        });


    });
}

module.exports = createMatchSchedulesLeague;

// Unit Test Case
if (require.main === module) {
    (function () {
        var requestObject = new Object();
        //requestObject.status = "ACTIVE";

        createMatchSchedulesLeague(requestObject, function (error, responseObject) {
            console.log("Response Code - ", responseObject);

        });
    })();
}