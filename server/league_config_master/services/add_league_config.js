var league_configMaster = require("../models/league_config_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addLeagueConfig(requestObject, callback) {
	var newLeagueConfig = new league_configMaster({
		title : requestObject.title,
		tourId : requestObject.tourId,
		templateId : requestObject.templateId,
		chipType : requestObject.chipType,
		commisionRate : requestObject.commisionRate,
		totalPoolAmount : requestObject.totalPrizeAmount,
		winningAmt : requestObject.winningAmt,
		startDate : requestObject.startDate,
		leagueCount : requestObject.leagueCount,
		maxLeagueCount : requestObject.maxLeagueCount,
		matchId : requestObject.matchId,
		maxTeams : requestObject.maxTeams,
		entryFee : requestObject.entryFee,
		winnersCount : requestObject.winnersCount,
		minTeams : requestObject.minTeams,
		isMultiEntry : requestObject.isMultiEntry,
		prizeInfo : requestObject.prizeInfo,
		leagueType : requestObject.leagueType,
		status : requestObject.status
	});

	var responseObject = new Object();
	newLeagueConfig.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addLeagueConfig;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.title = "";
	requestObject.tourId = "";
	requestObject.startDate = "";
	requestObject.leagueCount = "";
	requestObject.maxLeagueCount = "";
	requestObject.matchId = "";
	requestObject.maxTeams = "";
	requestObject.entryFee = "";
	requestObject.winnersCount = "";
	requestObject.minTeams = "";
	requestObject.isMultiEntry = "";
	requestObject.prizeInfo = "";
	requestObject.leagueType = "";
	requestObject.status = "";
	console.log(requestObject);

	addLeagueConfig(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}