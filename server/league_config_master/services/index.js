var addLeagueConfigService = require("./add_league_config");
var findLeagueConfigByPropertyService = require("./find_league_config_by_property");
var findLeagueConfigListService = require("./find_league_config_list");
var updateLeagueConfigService = require("./update_league_config");
var createMatchLeaguesService = require("./create_match_league_config");
var createMatchScheduleLeaguesService = require("./create_match_scheduled_league");

// Require

module.exports.addLeagueConfig = addLeagueConfigService;
module.exports.findLeagueConfigByProperty = findLeagueConfigByPropertyService;
module.exports.findLeagueConfigList = findLeagueConfigListService;
module.exports.updateLeagueConfig = updateLeagueConfigService;
module.exports.createMatchLeaguesService = createMatchLeaguesService
module.exports.createMatchScheduleLeaguesService = createMatchScheduleLeaguesService

// Export
