var league_configMaster = require("../models/league_config_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueConfigByProperty(requestObject, callback) {
	var query = league_configMaster.findOne({});
	if (typeof requestObject.configId !== "undefined" && requestObject.configId !== null)
		query.where("configId").equals(requestObject.configId);
	if (typeof requestObject.templateId !== "undefined" && requestObject.templateId !== null)
		query.where("templateId").equals(requestObject.templateId);
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.configId = currentRow.configId;
			currentObj.title = currentRow.title;
			currentObj.tourId = currentRow.tourId;
			currentObj.totalPoolAmount = currentRow.totalPoolAmount;
			currentObj.winningAmt = currentRow.winningAmt;
			currentObj.startDate = currentRow.startDate;
			currentObj.leagueCount = currentRow.leagueCount;
			currentObj.maxLeagueCount = currentRow.maxLeagueCount;
			currentObj.matchId = currentRow.matchId;
			currentObj.maxTeams = currentRow.maxTeams;
			currentObj.entryFee = currentRow.entryFee;
			currentObj.winnersCount = currentRow.winnersCount;
			currentObj.minTeams = currentRow.minTeams;
			currentObj.isMultiEntry = currentRow.isMultiEntry;
			currentObj.prizeInfo = currentRow.prizeInfo;
			currentObj.leagueType = currentRow.leagueType;
			currentObj.status = currentRow.status;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findLeagueConfigByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.configId = "configId";
	//requestObject.matchId = "matchId";

	findLeagueConfigByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}