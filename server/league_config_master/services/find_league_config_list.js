var league_configMaster = require("../models/league_config_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueConfigList(requestObject, callback) {
	var query = league_configMaster.find({});

	if (typeof requestObject.matchArr !== "undefined" && requestObject.matchArr !== null)
		query.where("matchId").in(requestObject.matchArr);

	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").in(requestObject.matchId);

	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);

	if (typeof requestObject.leagueType !== "undefined" && requestObject.leagueType !== null)
		query.where("leagueType").equals(requestObject.leagueType);

	if (typeof requestObject.startDate !== "undefined" && requestObject.startDate !== null)
		query.where("startDate").lte(requestObject.startDate);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var league_configArr = data;
			var league_configList = [];
			for (var i=0, length=league_configArr.length; i<length; i++) {
				var currentRow = league_configArr[i];
				var currentObj = {};
				currentObj.configId = currentRow.configId;
				currentObj.title = currentRow.title;
				currentObj.tourId = currentRow.tourId;
				currentObj.chipType = currentRow.chipType;
				currentObj.totalPoolAmount = currentRow.totalPoolAmount;
				currentObj.winningAmt = currentRow.winningAmt;
				currentObj.startDate = currentRow.startDate;
				currentObj.leagueCount = currentRow.leagueCount;
				currentObj.maxLeagueCount = currentRow.maxLeagueCount;
				currentObj.matchId = currentRow.matchId;
				currentObj.maxTeams = currentRow.maxTeams;
				currentObj.entryFee = currentRow.entryFee;
				currentObj.winnersCount = currentRow.winnersCount;
				currentObj.minTeams = currentRow.minTeams;
				currentObj.isMultiEntry = currentRow.isMultiEntry;
				currentObj.prizeInfo = currentRow.prizeInfo;
				currentObj.leagueType = currentRow.leagueType;
				currentObj.status = currentRow.status;
				league_configList.push(currentObj);
			}
			responseObject.responseData = league_configList;
		callback(null, responseObject);
	});
}

module.exports = findLeagueConfigList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findLeagueConfigList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}