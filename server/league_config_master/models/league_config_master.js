require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var LeagueConfigMasterSchema = new mongoose.Schema({
	configId: {type: Number, unique: true},
	templateId : Number,
	title: {type: String},
	chipType: {type: String},
	commisionRate : {type : Number},
	totalPoolAmount : Number,
	winningAmt : Number,
	tourId: {type: Number},
	startDate: {type: Date},
	leagueCount: {type: Number},
	maxLeagueCount: {type: Number},
	matchId: {type: Number},
	maxTeams: {type: Number},
	entryFee: {type: Number},
	winnersCount: {type: Number},
	minTeams: {type: Number},
	isMultiEntry: {type: Boolean},
	prizeInfo: {type: Array},
	leagueType: {type: String, enum: ['CONFIRMED','NOT_CONFIRMED']},
	status: {type: String, enum: ['APPROVED','ACTIVE', 'CANCELLED', 'FILLED', 'DONE']},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("LeagueConfigMaster", LeagueConfigMasterSchema, "league_config_master");

LeagueConfigMasterSchema.plugin(autoIncrement.plugin, {
	model: "LeagueConfigMaster",
	field: "configId",
	startAt: 1,
	incrementBy: 1
});