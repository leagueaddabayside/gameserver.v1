var logger = require("../../utils/logger").gameLogs;
var league_configService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueTemplateService = require('../../league_template/services/index');
var tourMatchService = require('../../tour_matches/services/index');
async = require("async");

function createMatchDeafultLeague(request, response, next) {
    var requestObject = request.body;
    //console.log("addLeagueConfig API :- Request - %j", requestObject);

    var responseObject = new Object();

    leagueTemplateService.findLeagueTemplateList({isDefaultLeague : true,status : 'ACTIVE'}, function (err, templateListResponse) {
        if (err) {
            responseObject.respCode = templateListResponse;
            responseObject.message = responseMessage[templateListResponse];
            response.json(responseObject);
            return;
        } else {
            logger.info('templateListResponse', templateListResponse);
            let templateList = templateListResponse.responseData;

            for (let i=0, length=templateList.length; i<length; i++) {
                templateList[i].startDate = new Date();
                templateList[i].configStatus = 'ACTIVE';

            }

            var status = ['ACTIVE','PENDING'];
            tourMatchService.findTourMatchesList({tourId: requestObject.tourId,status : status}, function (err, matchListReponseData) {

                logger.info('findTourMatchesList',matchListReponseData);
                // 1st para in async.eachSeries() is the array of items
                async.eachSeries(matchListReponseData.responseData,
                    // 2nd param is the function that each item is passed to
                    function (item, callback) {
                        logger.info('create match league config', item);


                        var requestObj = {};
                        requestObj.templateList = templateList;
                        requestObj.matchId = item.matchId;
                        requestObj.tourId = requestObject.tourId;


                        league_configService.createMatchLeaguesService(requestObj, function (error, data) {
                            if (error) {
                                responseObject.respCode = data.responseCode;
                                responseObject.message = responseMessage[data.responseCode];
                            } else {
                                responseObject.respCode = data.responseCode;
                                responseObject.responseData = data.responseData;
                            }
                            callback(error, data);
                            return;

                        });

                    },
                    // 3rd param is the function to call when everything's done
                    function (err) {
                        // All tasks are done now
                        logger.info('findTourMatchesList final done');
                        responseObject.respCode = responseCode.SUCCESS;
                        logger.info("addLeagueConfig API :- Response - %j", responseObject);
                        response.json(responseObject);

                    }
                );


            });

        }
    });
}

module.exports = createMatchDeafultLeague;

// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        requestObject.title = "";

        requestObject.status = "ACTIVE";

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        createMatchDeafultLeague(request, response);
    })();
}