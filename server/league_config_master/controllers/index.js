var addLeagueConfigApi = require("./add_league_config");
var findLeagueConfigByPropertyApi = require("./find_league_config_by_property");
var findLeagueConfigListApi = require("./find_league_config_list");
var updateLeagueConfigApi = require("./update_league_config");
var createMatchDeafultLeagueApi = require("./create_match_defaut_leagues");
var createMatchSpecialLeagueApi = require("./create_match_special_leagues");
var findLeagueConfigListReportApi = require("./find_league_config_list_report");

// Require

module.exports.addLeagueConfig = addLeagueConfigApi;
module.exports.findLeagueConfigByProperty = findLeagueConfigByPropertyApi;
module.exports.findLeagueConfigList = findLeagueConfigListApi;
module.exports.updateLeagueConfig = updateLeagueConfigApi;
module.exports.createMatchDeafultLeague = createMatchDeafultLeagueApi
module.exports.createMatchSpecialLeagueApi = createMatchSpecialLeagueApi
module.exports.findLeagueConfigListReportApi = findLeagueConfigListReportApi

// Export
