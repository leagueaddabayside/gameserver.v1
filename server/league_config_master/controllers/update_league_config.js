var logger = require("../../utils/logger").gameLogs;
var league_configService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateLeagueConfig(request, response, next) {
	var requestObject = request.body;
	//console.log("updateLeagueConfig API :- Request - %j", requestObject);

	var responseObject = new Object();
	league_configService.updateLeagueConfig(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateLeagueConfig API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateLeagueConfig;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.title = "";
		requestObject.tourId = "";
		requestObject.startDate = "";
		requestObject.leagueCount = "";
		requestObject.maxLeagueCount = "";
		requestObject.matchId = "";
		requestObject.maxTeams = "";
		requestObject.entryFee = "";
		requestObject.winnersCount = "";
		requestObject.currentTeams = "";
		requestObject.isMultiEntry = "";
		requestObject.prizeInfo = "";
		requestObject.leagueType = "";
		requestObject.status = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateLeagueConfig(request, response);
	})();
}