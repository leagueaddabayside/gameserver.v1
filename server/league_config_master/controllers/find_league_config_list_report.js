var logger = require("../../utils/logger").gameLogs;
var league_configService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueService = require("../../league_master/services/index");

function findLeagueConfigListReport(request, response, next) {
    var requestObject = request.body;
    //console.log("findLeagueConfigList API :- Request - %j", requestObject);

    var responseObject = new Object();
    let configDataMap = new Map();
    league_configService.findLeagueConfigList(requestObject, function (error, data) {
        if (data.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = data.responseCode;
            responseObject.message = responseMessage[data.responseCode];
            logger.info("findLeagueConfigList API :- Response - %j", responseObject);
            response.json(responseObject);
            return;
        } else {

            let configList = data.responseData;
            if (configList && configList.length > 0) {
                configList.forEach(function (configRow) {

                    let configObj = {};
                    configObj.configId = configRow.configId;
                    configObj.title = configRow.title;
                    configObj.winningAmt = configRow.winningAmt;
                    configObj.startDate = configRow.startDate;
                    configObj.status = configRow.status;
                    configObj.leagueType = configRow.leagueType;
                    configObj.isMultiEntry = configRow.isMultiEntry;
                    configObj.maxLeagueCount = configRow.maxLeagueCount;
                    configObj.maxTeams = configRow.maxTeams;
                    configObj.entryFee = configRow.entryFee;
                    configObj.minTeams = configRow.minTeams;
                    configObj.winnersCount = configRow.winnersCount;
                    configObj.chipType = configRow.chipType;
                    configObj.leagueCount = 0;
                    configObj.filledCount = 0;
                    configObj.cancelCount = 0;
                    configObj.emptyCount = 0;
                    configObj.partialCount = 0;
                    configDataMap.set(configRow.configId, configObj);
                });

                leagueService.findLeagueList(requestObject, function (error, leagueResp) {
                    if (leagueResp.responseCode !== responseCode.SUCCESS) {
                        responseObject.respCode = data.responseCode;
                        responseObject.message = responseMessage[data.responseCode];
                        logger.info("findLeagueConfigList API :- Response - %j", responseObject);
                        response.json(responseObject);
                        return;
                    }

                    let leagueList = leagueResp.responseData || [];
                    leagueList.forEach(function (leagueRow) {

                        let configId = leagueRow.configId;
                        let leagueCount = 1;
                        let filledCount = 0;
                        let cancelCount = 0;
                        let emptyCount = 0;
                        let partialCount = 0;

                        if(leagueRow.currentTeams === 0){
                            emptyCount = 1;
                        }else if(leagueRow.currentTeams === leagueRow.maxTeams){
                            filledCount = 1;
                        }else if(leagueRow.currentTeams >= leagueRow.minTeams){
                            partialCount = 0;
                        }else{
                            emptyCount = 1;
                        }
                    if(configDataMap.has(configId)){

                        let configRow = configDataMap.get(configId);
                        //console.log(configRow);
                        configRow.leagueCount += leagueCount;
                        configRow.filledCount += filledCount;
                        configRow.cancelCount += cancelCount;
                        configRow.emptyCount += emptyCount;
                        configRow.partialCount += partialCount;

                    }

                    });
                    responseObject.respCode = data.responseCode;
                    responseObject.respData = [...configDataMap.values()];

                    //logger.info("findLeagueConfigList API :- Response - %j", responseObject);
                    response.json(responseObject);
                });

            }else{
                responseObject.respCode = data.responseCode;
                responseObject.respData = [];
                response.json(responseObject);
            }



        }

    });
}

module.exports = findLeagueConfigListReport;

// Unit Test Case
if (require.main === module) {
    (function () {
        var request = {};
        var response = {
            json: function (result) {
                console.log(JSON.stringify(result, null, 2));
            }
        };

        var requestObject = new Object();
        //requestObject.status = "ACTIVE";

        console.log("Request Data - " + requestObject);
        request.body = requestObject;
        findLeagueConfigListReport(request, response);
    })();
}