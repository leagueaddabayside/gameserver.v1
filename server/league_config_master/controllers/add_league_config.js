var logger = require("../../utils/logger").gameLogs;
var league_configService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addLeagueConfig(request, response, next) {
	var requestObject = request.body;
	//console.log("addLeagueConfig API :- Request - %j", requestObject);

	var responseObject = new Object();
	league_configService.addLeagueConfig(requestObject, function(error, data) {
	if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("addLeagueConfig API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addLeagueConfig;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.title = "";
		requestObject.tourId = 2;
		requestObject.startDate = "";
		requestObject.leagueCount = 5;
		requestObject.maxLeagueCount = 10;
		requestObject.matchId = 10;
		requestObject.maxTeams = 20;
		requestObject.entryFee = 10;
		requestObject.winnersCount = 1;
		requestObject.minTeams = 2;
		requestObject.isMultiEntry = true;
		requestObject.prizeInfo = [];
		requestObject.leagueType = "CONFIRMED";
		requestObject.status = "ACTIVE";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addLeagueConfig(request, response);
	})();
}