var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addTourPlayers",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.addTourPlayers);
route.post("/updateTourPlayers",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.updateTourPlayers);
route.post("/findTourPlayersByProperty",middleware.getAdminIdFromToken, obj.findTourPlayersByProperty);
route.post("/findTourPlayersList",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.findTourPlayersList);
	route.post("/updateTourPlayersDisplayOrder",middleware.getAdminIdFromToken, obj.updateTourPlayersDisplayOrder);
route.post("/fetchTourMatchPlayers",middleware.auditTrailLog,middleware.getTourMatchInfo, obj.fetchTourMatchPlayers);
route.post("/fetchTeamMatchPlayers",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getTourMatchInfo, obj.fetchTeamMatchPlayersApi);

// Routes
module.exports = route;
