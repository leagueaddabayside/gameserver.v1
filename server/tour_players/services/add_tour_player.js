var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addTourPlayers(requestObject, callback) {
	var newTourPlayers = new tour_playersMaster({
		tourId : requestObject.tourId,
		name : requestObject.name,
		shortName : requestObject.shortName,
		fullName : requestObject.fullName,
		key : requestObject.key,
		identifyRoles : {
			keeper : requestObject.keeper,
			batsman : requestObject.batsman,
			bowler : requestObject.bowler
		},
		formatType : {
			ONE_DAY : requestObject.ONE_DAY,
			TEST : requestObject.TEST,
			T20 : requestObject.T20
		},
		points : requestObject.points,
		credit : requestObject.credit,
		createBy : requestObject.createBy,
		status : requestObject.status,
		team : requestObject.team,
	});

	var responseObject = new Object();
	newTourPlayers.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addTourPlayers;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourId = "";
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.keeper = "";
	requestObject.batsman = "";
	requestObject.bowler = "";
	requestObject.points = "";
	requestObject.credit = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addTourPlayers(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}