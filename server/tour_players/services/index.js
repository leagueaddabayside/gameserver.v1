var addTourPlayersService = require("./add_tour_player");
var findTourPlayersByPropertyService = require("./find_tour_player_by_property");
var findTourPlayersListService = require("./find_tour_player_list");
var updateTourPlayersService = require("./update_player_tour");
var updateTourPlayersDisplayOrderService = require("./update_tour_player_display_order");
var updateTourPlayerPointsService = require("./update_player_tour_points");

// Require

module.exports.addTourPlayers = addTourPlayersService;
module.exports.findTourPlayersByProperty = findTourPlayersByPropertyService;
module.exports.findTourPlayersList = findTourPlayersListService;
module.exports.updateTourPlayers = updateTourPlayersService;
module.exports.updateTourPlayersDisplayOrder = updateTourPlayersDisplayOrderService;
module.exports.updateTourPlayerPointsService = updateTourPlayerPointsService;

// Export
