var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addMultiTourPlayers(requestObject, callback) {

	var responseObject = new Object();

	tour_playersMaster.create(requestObject, function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});


}

module.exports = addMultiTourPlayers;

// Unit Test Case
if (require.main === module) {
	var requestObject = [{tourId: 1, name: 'R.Ashwin', shortName: 'Ashwin',key : '',identifyRoles: {keeper: false,batsman: true,bowler : true}, points: 11.5, credit: 7.5, status: 'ACTIVE'},
		{tourId: 1, name: 'M.S.Dhoni', shortName: 'Dhoni',key : '',identifyRoles: {keeper: true,batsman: false,bowler : false}, points: 11.5, credit: 7.5, status: 'ACTIVE'},
		{tourId: 1, name: 'R.Jadeja', shortName: 'Jadeja',key : '',identifyRoles: {keeper: false,batsman: false,bowler : true}, points: 11.5, credit: 7.5, status: 'ACTIVE'} ];

	console.log(requestObject);

	addMultiTourPlayers(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);

	});
}