var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var async = require("async");

function updateTourPlayersDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.tour_playersList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {playerId : currentRow.playerId};
		tour_playersMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateTourPlayersDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var tour_playersList = [ {
		playerId : 1,
		displayOrder : 1
	}, {
		playerId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.tour_playersList = tour_playersList;
	console.log(requestObject);

	updateTourPlayers(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}