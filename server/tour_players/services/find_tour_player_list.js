var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var utility = require("../../utils/utility");
var TEAM_NAMES = require("../../utils/team_names").teamNames;

function findTourPlayersList(requestObject, callback) {
	var query = tour_playersMaster.find({});
	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.teamKey !== "undefined" && requestObject.teamKey !== null)
		query.where("team.key").equals(requestObject.teamKey);

	if (typeof requestObject.teamsArr !== "undefined" && requestObject.teamsArr !== null)
		query.where("team.name").in(requestObject.teamsArr);

	if (typeof requestObject.formatType !== "undefined" && requestObject.formatType !== null){
		query.where("formatType."+requestObject.formatType).equals(true);
	}

	query.sort("name");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var tour_playersArr = data;
			var tour_playersList = [];
			for (var i=0, length=tour_playersArr.length; i<length; i++) {
				var currentRow = tour_playersArr[i];
				var currentObj = {};
				currentObj.playerId = currentRow.playerId;
				currentObj.tourId = currentRow.tourId;
				currentObj.name = currentRow.name;
				if(currentRow.fullName){
					currentObj.shortName = utility.getPlayerShortName(currentRow.fullName);
				}else{
					currentObj.shortName = currentRow.shortName;
				}

				currentObj.key = currentRow.key;
				currentObj.keeper = currentRow.identifyRoles.keeper;
				currentObj.batsman = currentRow.identifyRoles.batsman;
				currentObj.bowler = currentRow.identifyRoles.bowler;
				currentObj.ONE_DAY = currentRow.formatType.ONE_DAY;
				currentObj.TEST = currentRow.formatType.TEST;
				currentObj.T20 = currentRow.formatType.T20;
				currentObj.points = currentRow.points;
				currentObj.credit = currentRow.credit;
				currentObj.status = currentRow.status;
				currentObj.team = currentRow.team;

				let teamInfo = TEAM_NAMES[currentRow.team.card_name.toLowerCase()];
				if(teamInfo){
					currentObj.aliasTeamName = teamInfo.displayTeamName;
				}else{
					currentObj.aliasTeamName = currentRow.team.card_name;
				}

				tour_playersList.push(currentObj);
			}
			responseObject.responseData = tour_playersList;
		callback(null, responseObject);
	});
}

module.exports = findTourPlayersList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findTourPlayersList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}