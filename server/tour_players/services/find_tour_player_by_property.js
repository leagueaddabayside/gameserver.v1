var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findTourPlayersByProperty(requestObject, callback) {
	var query = tour_playersMaster.findOne({});
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.playerId !== "undefined" && requestObject.playerId !== null)
		query.where("playerId").equals(requestObject.playerId);
	if (typeof requestObject.key !== "undefined" && requestObject.key !== null)
		query.where("key").equals(requestObject.key);
	if (typeof requestObject.keeper !== "undefined" && requestObject.keeper !== null)
		query.where("identifyRoles.keeper").equals(requestObject.keeper);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.playerId = currentRow.playerId;
			currentObj.tourId = currentRow.tourId;
			currentObj.name = currentRow.name;
			currentObj.shortName = currentRow.shortName;
			currentObj.key = currentRow.key;
			currentObj.keeper = currentRow.identifyRoles.keeper;
			currentObj.batsman = currentRow.identifyRoles.batsman;
			currentObj.bowler = currentRow.identifyRoles.bowler;
			currentObj.ONE_DAY = currentRow.formatType.ONE_DAY;
			currentObj.TEST = currentRow.formatType.TEST;
			currentObj.T20 = currentRow.formatType.T20;
			currentObj.points = currentRow.points;
			currentObj.credit = currentRow.credit;
			currentObj.status = currentRow.status;
			currentObj.team = currentRow.team;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findTourPlayersByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tourId = "tourId";
	//requestObject.key = "key";
	//requestObject.identifyRoles.keeper = "identifyRoles.keeper";

	findTourPlayersByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}