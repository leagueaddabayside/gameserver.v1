var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateTourPlayers(requestObject, callback) {
	var updateObject = new Object();
	if(typeof requestObject.name !== 'undefined' && requestObject.name !== null)
		updateObject.name = requestObject.name;
	if(typeof requestObject.shortName !== 'undefined' && requestObject.shortName !== null)
		updateObject.shortName = requestObject.shortName;
	if(typeof requestObject.fullName !== 'undefined' && requestObject.fullName !== null)
		updateObject.fullName = requestObject.fullName;
	if(typeof requestObject.keeper !== 'undefined' && requestObject.keeper !== null )
		updateObject["identifyRoles.keeper"] = requestObject.keeper;
	if(typeof requestObject.batsman !== 'undefined' && requestObject.batsman !== null )
		updateObject["identifyRoles.batsman"] = requestObject.batsman;
	if(typeof requestObject.bowler !== 'undefined' && requestObject.bowler !== null )
		updateObject["identifyRoles.bowler"] = requestObject.bowler;
	if(typeof requestObject.ONE_DAY !== 'undefined' && requestObject.ONE_DAY !== null )
		updateObject["formatType.ONE_DAY"] = requestObject.ONE_DAY;
	if(typeof requestObject.T20 !== 'undefined' && requestObject.T20 !== null )
		updateObject["formatType.T20"] = requestObject.T20;
	if(typeof requestObject.TEST !== 'undefined' && requestObject.TEST !== null )
		updateObject["formatType.TEST"] = requestObject.TEST;
	if(typeof requestObject.credit !== 'undefined' && requestObject.credit !== null )
		updateObject.credit = requestObject.credit;
	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null )
		updateObject.status = requestObject.status;
	if(typeof requestObject.team !== 'undefined' && requestObject.team !== null )
		updateObject.team = requestObject.team;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {playerId: requestObject.playerId};
	tour_playersMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateTourPlayers;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.keeper = "";
	requestObject.batsman = "";
	requestObject.bowler = "";
	console.log(requestObject);

	updateTourPlayers(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}