var tour_playersMaster = require("../models/tour_players_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var async = require("async");

function updateTourPlayerPoints(requestObject, callback) {

    async.eachSeries(requestObject.players, function (player, cb) {
        var query = tour_playersMaster.findOne({playerId: player.playerId});

        query.exec(function (error, playerResponse) {

            if (error) {
                logger.error(error);
                cb(error);
                return;
            }
            playerResponse.updateAt = new Date();
            logger.info('updateTourPlayerPoints',playerResponse.runningPoints);
            logger.info('totalPoints',player.totalPoints,requestObject.matchId);

            let runningPointsArr = playerResponse.runningPoints || [];
            let isMatchFound = false;
            let totalPoints = 0;
            for (let i = 0; i < runningPointsArr.length; i++) {
                let matchPointObj = runningPointsArr[i];
                if (matchPointObj.matchId == requestObject.matchId) {
                    isMatchFound = true;
                    matchPointObj.points = player.totalPoints;
                }
                totalPoints += matchPointObj.points || 0;
            }

            if (!isMatchFound) {
                runningPointsArr.push({points: player.totalPoints, matchId: requestObject.matchId});
                totalPoints += player.totalPoints;
            }
            logger.info('playerId=',player.playerId,totalPoints,runningPointsArr);
         /*   playerResponse.points = totalPoints;
            playerResponse.runningPoints = runningPointsArr;

*/
            var query = {playerId: playerResponse.playerId};
            var updateObject = new Object();
            updateObject.updateAt = new Date();
            updateObject.points = totalPoints;
            updateObject.runningPoints = runningPointsArr;
            logger.info('updateTourPlayerPoints updateObject',updateObject);
            tour_playersMaster.findOneAndUpdate(query, updateObject, function (error, data) {
                if (error) {
                    logger.error(error);
                    cb(error);
                    return;
                }
                cb(null);
            });

/*
            playerResponse.save(function (error, data) {
                if (error) {
                    logger.error(error);
                    cb(error);
                    return;
                }
                cb(null);
            });*/
        })


    }, function (err) {
        logger.error(err);
        callback(null)
    });


}

module.exports = updateTourPlayerPoints;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.name = "";
    requestObject.shortName = "";
    requestObject.keeper = "";
    requestObject.batsman = "";
    requestObject.bowler = "";
    logger.info(requestObject);

    updateTourPlayers(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}