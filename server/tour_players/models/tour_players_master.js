require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var TourPlayersMasterSchema = new mongoose.Schema({
	playerId: {type: Number, unique: true},
	tourId: {type: Number},
	name: {type: String},
	fullName : {type: String},
	shortName: {type: String},
	key: {type: String},
	identifyRoles: {
		keeper: {type: Boolean},
		batsman: {type: Boolean},
		bowler: {type: Boolean}
	},
	formatType: {
		ONE_DAY: {type: Boolean},
		TEST: {type: Boolean},
		T20: {type: Boolean}
	},
	points: {type: Number},
	runningPoints: [],
	credit: {type: Number},
	team: {},
	displayOrder: {type: Number},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("TourPlayersMaster", TourPlayersMasterSchema, "tour_players_master");

TourPlayersMasterSchema.plugin(autoIncrement.plugin, {
	model: "TourPlayersMaster",
	field: "playerId",
	startAt: 1,
	incrementBy: 1
});