var logger = require("../../utils/logger").gameLogs;
var tourPlayersService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function fetchTourMatchPlayers(request, response, next) {
	var param = request.body;
	//console.log("findTourMatchesByProperty API :- Request - %j", requestObject);

	var matchInfoTeam = param.matchInfo.teams;
	var responseObject = new Object();

	if(!(matchInfoTeam && matchInfoTeam.a)){
		responseObject.respCode = responseCode.MATCH_DOES_NOT_EXIST;
		responseObject.message = responseMessage[responseCode.MATCH_DOES_NOT_EXIST];
		response.json(responseObject);
		return;
	}

	var teamArr = [];
	teamArr.push(matchInfoTeam.a.name);
	teamArr.push(matchInfoTeam.b.name);

	var requestObject = {tourId : param.matchInfo.tourId,status : 'ACTIVE',teamsArr:teamArr,formatType :param.matchInfo.formatType };
	tourPlayersService.findTourPlayersList(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;

			var tourPlayerList = data.responseData;
			let playerList = [];
			for (var i=0, length=tourPlayerList.length; i<length; i++) {
				let currentRow = tourPlayerList[i];
				let playerObj = {};
				playerObj.playerName = currentRow.name;
				playerObj.shortName = currentRow.shortName;
				playerObj.batter = currentRow.batsman;
				playerObj.bowler = currentRow.bowler;
				playerObj.wicketKeeper = currentRow.keeper;
				playerObj.playerCredit = currentRow.credit;
				playerObj.scoredPoints = currentRow.points;
				playerObj.teamName = currentRow.team.card_name;
				playerObj.aliasTName = currentRow.aliasTeamName;
				playerObj.playerId = currentRow.playerId;
				playerObj.isSelected = false;
				playerObj.isCaptain = false;
				playerObj.isViceCaptain = false;
				playerObj.isBonus = false;
				playerList.push(playerObj);
			}
			var reponseData = {};
			reponseData.playersArr = playerList;
			reponseData.matchInfo = param.matchInfo;
			responseObject.respData = reponseData;
		}

		//logger.info("fetchTourMatchPlayers API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchTourMatchPlayers;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();


		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchTourMatchPlayers(request, response);
	})();
}