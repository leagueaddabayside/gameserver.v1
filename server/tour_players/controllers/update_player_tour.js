var logger = require("../../utils/logger").gameLogs;
var tour_playersService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateTourPlayers(request, response, next) {
	var requestObject = request.body;
	//console.log("updateTourPlayers API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_playersService.updateTourPlayers(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateTourPlayers API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateTourPlayers;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.tourId = "";
		requestObject.name = "";
		requestObject.shortName = "";
		requestObject.key = "";
		requestObject.keeper = "";
		requestObject.batsman = "";
		requestObject.bowler = "";
		requestObject.points = "";
		requestObject.credit = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateTourPlayers(request, response);
	})();
}