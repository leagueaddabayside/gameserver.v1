var addTourPlayersApi = require("./add_tour_player");
var findTourPlayersByPropertyApi = require("./find_tour_player_by_property");
var findTourPlayersListApi = require("./find_tour_player_list");
var updateTourPlayersApi = require("./update_player_tour");
var updateTourPlayersDisplayOrderApi = require("./update_tour_player_display_order");
var fetchTourMatchPlayersApi = require("./fetch_tour_match_players");
var fetchTeamMatchPlayersApi = require("./fetch_team_match_players");

// Require

module.exports.addTourPlayers = addTourPlayersApi;
module.exports.findTourPlayersByProperty = findTourPlayersByPropertyApi;
module.exports.findTourPlayersList = findTourPlayersListApi;
module.exports.updateTourPlayers = updateTourPlayersApi;
module.exports.updateTourPlayersDisplayOrder = updateTourPlayersDisplayOrderApi;
module.exports.fetchTourMatchPlayers = fetchTourMatchPlayersApi
module.exports.fetchTeamMatchPlayersApi = fetchTeamMatchPlayersApi

// Export
