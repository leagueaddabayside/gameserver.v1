var logger = require("../../utils/logger").gameLogs;
var tourPlayersService = require("../services/index");
var userTeamService = require("../../user_match_teams/services/index");

var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function fetchTeamMatchPlayers(request, response, next) {
	var param = request.body;
	logger.info("fetchTeamMatchPlayers API :- Request - %j", param);

	var matchInfoTeam = param.matchInfo.teams;

	var teamArr = [];
	teamArr.push(matchInfoTeam.a.name);
	teamArr.push(matchInfoTeam.b.name);

	var requestObject = {tourId : param.matchInfo.tourId,status : 'ACTIVE',teamsArr:teamArr,formatType :param.matchInfo.formatType};
	var responseObject = new Object();


	var userTeamObj = {};
	userTeamObj.teamId = param.teamId;
	userTeamObj.userId = param.userId;
	userTeamObj.matchId = param.matchId;

	userTeamService.getTeamInfoPlayersApi(userTeamObj, function(error, teamPlayersData) {
		if(teamPlayersData.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = teamPlayersData.responseCode;
			responseObject.message = responseMessage[teamPlayersData.responseCode];
			response.json(responseObject);
			return;
		} else {
			let playerMap = new Map();
			logger.info("teamPlayersData API :- Request - %j", teamPlayersData);

			for(var i=0;i<teamPlayersData.responseData.length;i++){
				let pObj = teamPlayersData.responseData[i];
				playerMap.set(pObj.playerId, pObj);
			}
			logger.info("playerMap API :- Request - %j", playerMap);

			tourPlayersService.findTourPlayersList(requestObject, function(error, data) {
				if(data.responseCode !== responseCode.SUCCESS) {
					responseObject.respCode = data.responseCode;
					responseObject.message = responseMessage[data.responseCode];
				} else {
					responseObject.respCode = data.responseCode;

					var tourPlayerList = data.responseData;
					let playerList = [];
					for (var i=0, length=tourPlayerList.length; i<length; i++) {
						let currentRow = tourPlayerList[i];

						if(playerMap.has(currentRow.playerId)){
							let playerObj = playerMap.get(currentRow.playerId);
							playerList.push(playerObj);
						}else{
							let playerObj = {};
							playerObj.playerName = currentRow.name;
							playerObj.shortName = currentRow.shortName;
							playerObj.batter = currentRow.batsman;
							playerObj.bowler = currentRow.bowler;
							playerObj.wicketKeeper = currentRow.keeper;
							playerObj.playerCredit = currentRow.credit;
							playerObj.scoredPoints = currentRow.points;
							playerObj.teamName = currentRow.team.card_name;
							playerObj.aliasTName = currentRow.aliasTeamName;
							playerObj.playerId = currentRow.playerId;
							playerObj.isSelected = false;
							playerObj.isCaptain = false;
							playerObj.isViceCaptain = false;
							playerObj.isBonus = false;
							playerList.push(playerObj);
						}

					}
					var reponseData = {};
					reponseData.playersArr = playerList;
					reponseData.matchInfo = param.matchInfo;
					responseObject.respData = reponseData;
				}

				//logger.info("fetchTourMatchPlayers API :- Response - %j", responseObject);
				response.json(responseObject);
			});

		}

	});

}

module.exports = fetchTeamMatchPlayers;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				logger.info(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();


		logger.info("Request Data - " + requestObject);
		request.body = requestObject;
		fetchTeamMatchPlayers(request, response);
	})();
}