var logger = require("../../utils/logger").gameLogs;
var tour_playersService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateTourPlayersDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateTourPlayersDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_playersService.updateTourPlayersDisplayOrder(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateTourPlayersDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateTourPlayersDisplayOrder;