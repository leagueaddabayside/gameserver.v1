/**
 * Created by sumit on 1/16/2017.
 */
var dummy = require('../dummy/index'); 
var tours = require('../tours/index');
var tourPlayers = require('../tour_players/index');
var tourMatches = require('../tour_matches/index');
var leagueConfig = require('../league_config_master/index');
var leagueMaster = require('../league_master/index');
var leagueTemplate = require('../league_template/index');
var cricketApi = require('../cricket_api/index');
var userTeam = require('../user_match_teams/index');
var userJoinedLeague = require('../user_joined_leagues/index');
var scoreTemplate = require('../score_template/index');
var gameTxn = require('../game_txn_master/index');

var schedular = require('../schedular/index');

module.exports = function(app) {
    app.use('/',tours);
    app.use('/',tourPlayers);
    app.use('/',tourMatches);
    app.use('/',leagueConfig);
    app.use('/',leagueMaster);
    app.use('/',leagueTemplate);
    app.use('/',cricketApi);
    app.use('/',userTeam);
    app.use('/',userJoinedLeague);
    app.use('/',dummy);
    app.use('/',scoreTemplate);
    app.use('/',gameTxn);
}
