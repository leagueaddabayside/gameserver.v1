var GameTxn = require("../models/game_txn_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addGameTxn(requestObject, callback) {
	var gameTxn = new GameTxn({
		txnDate : new Date(),
		userId : requestObject.userId,
		matchId : requestObject.matchId,
		tourId : requestObject.tourId,
		leagueId : requestObject.leagueId,
		teamId : requestObject.teamId,
		configId : requestObject.configId,
		amount : requestObject.amount,
		remarks : requestObject.remarks,
		txnType : requestObject.txnType,
		status : 'PENDING'
	});

	var responseObject = new Object();
	gameTxn.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addGameTxn;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourName = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.venue = "";
	requestObject.startDate = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addGameTxn(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}