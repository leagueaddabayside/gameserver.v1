var GameTxn = require("../models/game_txn_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function findGameTxnList(requestObject, callback) {
    var query = GameTxn.find({});

    if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
        query.where("status").in(requestObject.status);
    if (typeof requestObject.txnType !== "undefined" && requestObject.txnType !== null)
        query.where("txnType").in(requestObject.txnType);

    query.sort("txnId");

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        responseObject.responseCode = responseCode.SUCCESS;
        var txnArr = data;
        var txnList = [];
        for (var i = 0, length = txnArr.length; i < length; i++) {
            var currentRow = txnArr[i];
            var currentObj = {};
            currentObj.txnId = currentRow.txnId;
            currentObj.txnDate = currentRow.txnDate;
            currentObj.userId = currentRow.userId;
            currentObj.matchId = currentRow.matchId;
            currentObj.tourId = currentRow.tourId;
            currentObj.leagueId = currentRow.leagueId;
            currentObj.amount = currentRow.amount;
            currentObj.txnType = currentRow.txnType;
            if (typeof currentRow.txnDate !== "undefined" && currentRow.txnDate !== null)
                currentObj.txnDate = moment(currentRow.txnDate).unix();
            currentObj.status = currentRow.status;
            txnList.push(currentObj);
        }
        responseObject.responseData = txnList;
        callback(null, responseObject);
    });
}

module.exports = findGameTxnList;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.status = "ACTIVE";

    findGameTxnList(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}