var GameTxn = require("../models/game_txn_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function updateGameTxn(requestObject, callback) {
    var updateObject = new Object();

    if (typeof requestObject.status !== 'undefined' && requestObject.status !== null)
        updateObject.status = requestObject.status;
    if (typeof requestObject.tpTxnId !== 'undefined' && requestObject.tpTxnId !== null)
        updateObject.tpTxnId = requestObject.tpTxnId;
    if (typeof requestObject.tpTxnDate !== 'undefined' && requestObject.tpTxnDate !== null)
        updateObject.tpTxnDate = requestObject.tpTxnDate;

    updateObject.updateAt = new Date();

    var responseObject = new Object();
    var query = {txnId: requestObject.txnId};
    GameTxn.findOneAndUpdate(query, updateObject, function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        responseObject.responseCode = responseCode.SUCCESS;
        responseObject.responseData = data;
        callback(null, responseObject);
    });
}

module.exports = updateGameTxn;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.tourId = 2;
    requestObject.activeDate = "2017-01-30";
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    console.log(requestObject);

    updateGameTxn(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}