var RakeTxn = require("../models/rake_txn_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addRakeTxn(requestObject, callback) {
	var rakeTxn = new RakeTxn({
		txnDate : new Date(),
		matchId : requestObject.matchId,
		tourId : requestObject.tourId,
		leagueId : requestObject.leagueId,
		configId : requestObject.configId,
		winningAmt : requestObject.winningAmt,
		joinLeagueAmt : requestObject.joinLeagueAmt,
		rakeAmount : requestObject.rakeAmount,
		remarks : requestObject.remarks,
		txnType : requestObject.txnType,
		updateAt : new Date()
	});

	var responseObject = new Object();
	rakeTxn.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addRakeTxn;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourName = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.venue = "";
	requestObject.startDate = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addRakeTxn(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}