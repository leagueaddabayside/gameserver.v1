var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/findGameTxnList",middleware.getAdminIdFromToken, obj.findGameTxnList);
// Routes
module.exports = route;
