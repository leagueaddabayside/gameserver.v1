require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var GameTxnSchema = new mongoose.Schema({
    txnId: {type: Number, unique: true},
    txnDate: {type: Date, default: Date.now},
    userId: {type: Number},
    matchId: {type: Number},
    tourId: {type: Number},
    leagueId: {type: Number},
    teamId : {type : Number},
    configId: {type: Number},
    amount: {type: Number},
    remarks: {type: String},
    tpTxnId: {type: String},
    tpTxnDate: {type: Date},
    txnType: {type: String, enum: ['JOIN_LEAGUE', 'REFUND_LEAGUE', 'WINNING_LEAGUE']},
    status: {type: String, enum: ['PENDING', 'SUCCESS', 'FAILED']},
    settlementDate: {type: Date},
    settlementStatus: {type: String, enum: ['PENDING', 'DONE']},
    updateAt: {type: Date}
});

module.exports = mongoose.model("GameTxn", GameTxnSchema, "game_txn_master");

GameTxnSchema.plugin(autoIncrement.plugin, {
    model: "GameTxn",
    field: "txnId",
    startAt: 1,
    incrementBy: 1
});