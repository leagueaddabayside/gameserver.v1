require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var RakeTxnSchema = new mongoose.Schema({
    rakeId: {type: Number, unique: true},
    txnDate: {type: Date, default: Date.now},
    matchId: {type: Number},
    tourId: {type: Number},
    leagueId: {type: Number},
    configId: {type: Number},
    winningAmt : {type: Number},
    joinLeagueAmt : {type: Number},
    rakeAmount: {type: Number},
    remarks: {type: String},
    txnType: {type: String, enum: ['CREDIT', 'DEBIT']},
    updateAt: {type: Date}
});

module.exports = mongoose.model("RakeTxn", RakeTxnSchema, "rake_txn_master");

RakeTxnSchema.plugin(autoIncrement.plugin, {
    model: "RakeTxn",
    field: "rakeId",
    startAt: 1,
    incrementBy: 1
});