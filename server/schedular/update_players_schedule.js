/**
 * Created by sumit on 3/8/2017.
 */

var cron = require('cron');
var constant = require('../utils/constant');
var leagueConfigService = require('../league_config_master/services/index');
var tourMatchService = require('../tour_matches/services/index');
var cricketApiService = require('../cricket_api/services/index');
var cricketApiRequest = require('../cricket_api/cricket_request/index');
var logger = require("../utils/logger").gameLogs;

var cronJob = cron.job("0 0 */" + constant.UPDATE_MATCH_PLAYERS_SCHEDULE + " * * *", function () {


        // perform operation e.g. GET request http.get() etc.
    logger.info('UPDATE_MATCH_PLAYERS_SCHEDULE', new Date());

    cricketApiRequest.getCricketApiTokenApi({},function(err,cricketAPIToken){
        //cricketAPIToken
        if(err){
            logger.error(err);
        }
        cricketApiService.uploadSportsTourPlayersListService({cricketAPIToken : cricketAPIToken}, function (err, updatePlayerResp) {
            logger.info('uploadSportsTourPlayersListService', updatePlayerResp, new Date());

        })
    });



});
logger.info('update match player schedular start');
cronJob.start();