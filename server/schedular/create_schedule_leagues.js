/**
 * Created by sumit on 3/8/2017.
 */

var cron = require('cron');
var constant = require('../utils/constant');
var leagueConfigService = require('../league_config_master/services/index');
var tourMatchService = require('../tour_matches/services/index');
var logger = require("../utils/logger").gameLogs;

var cronJob = cron.job("0 */" + constant.UPDATE_LEAGUE_SCHEDULE_LEAGUE + " * * * *", function () {
    // perform operation e.g. GET request http.get() etc.
    logger.info('createMatchScheduleLeaguesService', new Date());

    leagueConfigService.createMatchScheduleLeaguesService({}, function (err, scheduleLeagueResponse) {
        logger.info('scheduleLeagueResponse', scheduleLeagueResponse, new Date());
        tourMatchService.matchLeaguePrizeUpdateService({},function(err, leagueUpdate){
            logger.info('matchLeaguePrizeUpdateService', leagueUpdate, new Date());
        });
    })


});
logger.info('update match point schedular start');
cronJob.start();