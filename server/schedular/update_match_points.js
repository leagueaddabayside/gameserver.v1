/**
 * Created by sumit on 3/8/2017.
 */

var cron = require('cron');
var constant = require('../utils/constant');
var reposneCode = require('../utils/response_code');
var logger = require("../utils/logger").gameLogs;

var tourMatchService = require('../tour_matches/services/index');
async = require("async");
var cricketApiService = require('../cricket_api/services/index');
var userTeamService = require('../user_match_teams/services/index');
var joinedLeagueService = require('../user_joined_leagues/services/index');
var cricketApiRequest = require('../cricket_api/cricket_request/index');

var cronJob = cron.job("0 */" + constant.UPDATE_MATCH_POINT_SCHEDULAR + " * * * *", function () {


    logger.info('update match points schedular', new Date());
    var status = ['RUNNING', 'UNDER_REVIEW'];
    tourMatchService.findTourMatchesList({
        status: status,
        isLeaguePrizeUpdate: true,
        isRankUpdated: false
    }, function (err, matchListResponse) {

        var matchList = matchListResponse.responseData;

        async.eachSeries(matchList,
            // 2nd param is the function that each item is passed to
            function (matchRow, callback) {

                cricketApiRequest.fetchSportsMatcheLiveDataRequest({key: matchRow.key}, function (err, apiMatchData) {

                    if(err){
                        logger.error('live data not found for matchKey',matchRow.key);
                        callback(null);
                        return;
                    }
                    let funData = {};
                    funData.apiMatchData = apiMatchData;
                    funData.matchRow = matchRow;
                    setTimeout(updateMatchPoints, constant.LIVE_SCORE_MATCH_POINT_DELAY, funData);
                    callback(null);
                    return;
                })


            },
            // 3rd param is the function to call when everything's done
            function (err) {
                // All tasks are done now
                logger.info('update match points schedular done', err, new Date());

            }
        );
    })


});
logger.info('update match point schedular start');
cronJob.start();


function updateMatchPoints(funData) {
    // perform operation e.g. GET request http.get() etc.
    logger.info('update match points schedular', new Date());

    let matchRow = funData.matchRow;

    // console.log('uploadSportsTour',item);

    cricketApiService.calculateMatchPlayersPointService({
        matchId: matchRow.matchId,
        apiMatchData: funData.apiMatchData
    }, function (err, resp) {
        if (err) {
            logger.error('Error in calculateMatchPlayersPointService', err);
            return;
        }

        logger.info('calculateMatchPlayersPointService resp', resp, resp.responseCode);
        if (resp.responseCode !== reposneCode.MATCH_REVIEW_STATUS_POST_VALIDATED) {
            logger.info('calculateMatchPlayersPointService resp', resp.responseCode);
            userTeamService.updateTeamPointsRedisApi({matchId: matchRow.matchId}, function (err, resp) {
                if (err) {
                    logger.error('updateTeamPointsRedisApi', err);
                    return;
                }

                joinedLeagueService.updateLeagueTeamRanksRedisApi({matchId: matchRow.matchId}, function (err, resp) {
                    if (err) {
                        logger.error('updateLeagueTeamRanksRedisApi', err);
                        return;
                    }
                    return;
                });
            });
        } else {
            logger.info('matchRow ', matchRow.status, matchRow.matchPreviewStatus);
            if (matchRow.status == 'UNDER_REVIEW' && matchRow.matchPreviewStatus == 'post-match-validated') {
                if (!matchRow.isRankUpdated) {

                    logger.info('update redis point before prize update');
                    userTeamService.updateTeamPointsRedisApi({matchId: matchRow.matchId}, function (err, resp) {
                        if (err) {
                            logger.error('updateTeamPointsRedisApi', err);
                            return;
                        }

                        joinedLeagueService.updateLeagueTeamRanksRedisApi({matchId: matchRow.matchId}, function (err, resp) {
                            if (err) {
                                logger.error('updateLeagueTeamRanksRedisApi', err);
                                return;
                            }

                            logger.info('matchLeagueWinningRankUpdateService ', matchRow.matchId);
                            //update rank  , teampoint and prize info
                            tourMatchService.matchLeagueWinningRankUpdateService({matchId: matchRow.matchId}, function (err, updateRankResp) {
                                if (err) {
                                    logger.error('matchLeagueWinningRankUpdateService', err);
                                    return;
                                }

                                //update user team points
                                userTeamService.updateMatchUserTeamPoints({matchId: matchRow.matchId}, function (err, updatePointsResp) {

                                    if (err) {
                                        logger.error('updateMatchUserTeamPoints', err);
                                        return;
                                    }

                                    tourMatchService.updateTourMatches({
                                        isRankUpdated: true,
                                        matchId: matchRow.matchId
                                    }, function (err, updateResponse) {
                                        logger.info('updateTourMatches isRankUpdated', updateResponse);
                                        return;
                                    });
                                });


                            });

                        });
                    });

                }


            }

        }


    });


}