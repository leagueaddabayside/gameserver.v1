var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

var middleware = require('../middleware/index');
route.post("/addTourMatches",middleware.getAdminIdFromToken, obj.addTourMatches);
route.post("/updateTourMatches",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.updateTourMatches);
route.post("/findTourMatchesByProperty",middleware.getAdminIdFromToken, obj.findTourMatchesByProperty);
route.post("/findTourMatchesList",middleware.getAdminIdFromToken, obj.findTourMatchesList);
route.post("/updateTourMatchesDisplayOrder",middleware.getAdminIdFromToken, obj.updateTourMatchesDisplayOrder);
route.post("/fetchMatchList",middleware.getUserIdFromToken,middleware.getTourListMap, obj.fetchTourMatchesList);
route.post("/fetchTourMatchInfo",middleware.getUserIdFromToken,middleware.getUserMatchTeamInfo, obj.fetchTourMatchInfo);
route.post("/fetchTourMatchScoreCard",middleware.validateServerAuthentication, obj.fetchTourMatchInfoScoreCardApi);
route.post("/dstributeMatchWinning",middleware.auditTrailLog,middleware.getAdminIdFromToken, obj.dstributeMatchWinningApi);

// Routes
module.exports = route;
