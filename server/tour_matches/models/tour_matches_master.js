require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var TourMatchesMasterSchema = new mongoose.Schema({
    matchId: {type: Number, unique: true},
    tourId: {type: Number},
    name: {type: String},
    relatedName: {type: String},
    title: {type: String},
    guruLink: {type: String},
    venue: {type: String},
    shortName: {type: String},
    formatType: {type: String, enum: ['T20', 'TEST', 'ONE_DAY']},
    key: {type: String},
    startTime: {type: Date},
    endTime: {type: Date},
    teams: {type: {}},
    players: [{
        playerId: Number,
        key: String,
        name: String,
        isPlayingEleven: Boolean,
        startingPoint : Number,
        totalPoints : Number,
        firstInnings: {
            currentPoints : Number,
            pointBreakup : {
                runs : Number,
                fours : Number,
                sixes : Number,
                st_rate : Number,
                fifties_hundred : Number,
                wickets : Number,
                maidens : Number,
                ec_rate : Number,
                bonus_pos : Number,
                bonus_neg : Number,
                catch : Number,
                runout : Number
            },
            fours: Number,
            sixes: Number,
            runs: Number,
            ballFaced: Number,
            batsmanDots: Number,
            stRate : Number,
            dismissed: Boolean,
            bowlerBalls: Number,
            bowlerRuns: Number,
            overs: Number,
            wickets: Number,
            maidenOvers: Number,
            bowlerDots: Number,
            ecRate : Number,
            extras: Number,
            catches: Number,
            runOuts: Number,
            stumbeds: Number,
        },
        secondInnings: {
            currentPoints : Number,
            pointBreakup : {
                runs : Number,
                fours : Number,
                sixes : Number,
                st_rate : Number,
                fifties_hundred : Number,
                wickets : Number,
                maidens : Number,
                ec_rate : Number,
                bonus_pos : Number,
                bonus_neg : Number,
                catch : Number,
                runout : Number
            },
            fours: Number,
            sixes: Number,
            runs: Number,
            ballFaced: Number,
            batsmanDots: Number,
            dismissed: Boolean,
            bowlerBalls: Number, 
            bowlerRuns: Number,
            overs: Number,
            wickets: Number,
            maidenOvers: Number,
            bowlerDots: Number,
            extras: Number,
            catches: Number,
            runOuts: Number,
            stumbeds: Number,
        },
        isSecondStart : Boolean,
        bowler: Boolean,
        batsman: Boolean,
        keeper: Boolean
    }],
    status: {
        type: String,
        enum: ['PENDING', 'REJECTED', 'LOCKED', 'ACTIVE', 'RUNNING', 'UNDER_REVIEW', 'ABANDONED', 'CLOSED', 'HIDE']
    },
    matchPreviewStatus : {type: String},
    isRankUpdated : {type : Boolean, default: false},
    isLeaguePrizeUpdate : {type : Boolean, default: false},
    displayOrder: {type: Number},
    createBy: Number,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date}
});

module.exports = mongoose.model("TourMatchesMaster", TourMatchesMasterSchema, "tour_matches");

TourMatchesMasterSchema.plugin(autoIncrement.plugin, {
    model: "TourMatchesMaster",
    field: "matchId",
    startAt: 1,
    incrementBy: 1
});