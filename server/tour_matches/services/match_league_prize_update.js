var logger = require("../../utils/logger").gameLogs;
var leagueConfigService = require("../../league_config_master/services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueService = require('../../league_master/services/index');
var tourMatchService = require('../../tour_matches/services/index');
var userJoinedLeagueService = require('../../user_joined_leagues/services/index');

async = require("async");

function matchLeaguePrizeUpdate(request, cb) {
    var requestObject = request.body;
    //console.log("addLeagueConfig API :- Request - %j", requestObject);

    var responseObject = new Object();
    var status = ['RUNNING', 'ABANDONED'];
    tourMatchService.findTourMatchesList({
        status: status
    }, function (err, matchListReponseData) {
        if (err) {
            responseObject.respCode = responseCode.MONGO_ERROR;
            responseObject.message = responseMessage[responseCode.MONGO_ERROR];
            cb(responseObject);
            return;
        }
        // logger.info('matchListReponseData', matchListReponseData);
        let matchList = matchListReponseData.responseData;
        let matchKeyMap = new Map();
        for (let i = 0, length = matchList.length; i < length; i++) {
            let matchRow = matchList[i];
            if(matchRow.status == 'RUNNING' && matchRow.isLeaguePrizeUpdate == false){
                matchKeyMap.set(matchList[i].matchId, matchList[i]);
            }else if(matchRow.status == 'ABANDONED'){
                matchKeyMap.set(matchList[i].matchId, matchList[i]);
            }
        }
        logger.info('matchKeyMap', matchKeyMap);

        async.eachSeries([...matchKeyMap.values()], function (matchObj, matchCb) {
            logger.info('matchKeyMap matchObj', matchObj);
            let leagueRequestObject = {
                matchId: matchObj.matchId,
                statusArr : []
            };

            if(matchObj.status == 'RUNNING'){
                leagueRequestObject.statusArr = ['FILLING', 'FILLED'];
            }else if(matchObj.status == 'ABANDONED'){
                leagueRequestObject.statusArr = ['FILLING', 'FILLED','FREEZE'];
            }

            //logger.info('leagueRequestObject', leagueRequestObject);

            leagueService.findLeagueList(leagueRequestObject, function (err, leagueReponseData) {

                // logger.info('leagueReponseData', leagueReponseData);
                // 1st para in async.eachSeries() is the array of items
                async.eachSeries(leagueReponseData.responseData,
                    // 2nd param is the function that each item is passed to
                    function (leagueObject, callback) {
                        logger.info('leagueObject', leagueObject);
                        if (matchObj && matchObj.status === 'ABANDONED') {
                            logger.info('matchObj ABANDONED', matchObj);
                            if (leagueObject.chipType === 'skill') {
                                leagueService.updateLeague({
                                    leagueId: leagueObject.leagueId,
                                    status: 'CANCELLED'
                                }, function (err, updateResponse) {
                                    callback(err, updateResponse);
                                });

                            } else if (leagueObject.chipType === 'real') {
                                userJoinedLeagueService.userCancelLeaguesApi({
                                    leagueId: leagueObject.leagueId,
                                    matchObj: matchObj
                                }, callback);
                                //cancell  leagues

                                //update cancel status
                            }

                        }

                        if (matchObj && matchObj.status === 'RUNNING') {
                            logger.info('matchObj Running', matchObj.matchId);

                            if (leagueObject.leagueType === 'CONFIRMED') {
                                logger.info('leagueType CONFIRMED', leagueObject.leagueId);
                                //update status freezeCANCELLED

                                if (leagueObject.currentTeams == 0) {
                                    leagueService.updateLeague({
                                        leagueId: leagueObject.leagueId,
                                        status: 'CANCELLED'
                                    }, function (err, updateResponse) {
                                        callback(err, updateResponse);
                                    });
                                } else {
                                    leagueService.updateLeague({
                                        leagueId: leagueObject.leagueId,
                                        status: 'FREEZE'
                                    }, function (err, updateResponse) {
                                        callback(err, updateResponse);
                                    });
                                }


                            } else if (leagueObject.leagueType === 'NOT_CONFIRMED') {
                                logger.info('leagueType NOT_CONFIRMED', leagueObject.leagueId);
                                handleNotConfirmedLeague(leagueObject, matchObj, callback)
                            }
                        }

                    },
                    // 3rd param is the function to call when everything's done
                    function (err) {
                        // All tasks are done now
                        if (err) {
                            logger.info('err final done', err);
                            responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                            matchCb(null, responseObject);
                            return;
                        }

                        tourMatchService.updateTourMatches({
                            isLeaguePrizeUpdate: true,
                            matchId: matchObj.matchId
                        }, function (err, updateResponse) {
                            responseObject.respCode = responseCode.SUCCESS;
                            matchCb(null, responseObject);
                            return;
                        });
                    }
                );


            });

        }, function (err) {
            if (err) {
                logger.info('err final done', err);
                responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                cb(null, responseObject);
                return;
            }
            responseObject.respCode = responseCode.SUCCESS;
            cb(null, responseObject);
        })

    });
}


function handleNotConfirmedLeague(leagueObject, matchObj, cb) {


    leagueConfigService.findLeagueConfigByProperty({configId: leagueObject.configId}, function (err, leagueConfigResponse) {
        let leagueConfigObj = leagueConfigResponse.responseData;
        logger.info('leagueConfigObj,leagueObject', leagueConfigObj.configId);
        logger.info('leagueConfigObj,leagueObject', leagueObject.currentTeams, leagueObject.maxTeams, leagueConfigObj.minTeams);

        if (leagueObject.currentTeams == leagueObject.maxTeams) {
            logger.info('update status freeze league filled', leagueObject.leagueId);

            leagueService.updateLeague({
                leagueId: leagueObject.leagueId,
                status: 'FREEZE'
            }, function (err, updateResponse) {
                cb(err, updateResponse);
            });
        } else if (leagueObject.currentTeams >= leagueConfigObj.minTeams) {
            logger.info('update status prize revise league filled', leagueObject.leagueId);

            if (leagueObject.chipType === 'skill') {
                leagueService.updateLeague({
                    leagueId: leagueObject.leagueId,
                    status: 'FREEZE'
                }, function (err, updateResponse) {
                    cb(err, updateResponse);
                });
            } else if (leagueObject.chipType === 'real') {

                let winningAmt = leagueObject.winningAmt;
                let entryFee = leagueObject.entryFee;
                let currentTeams = leagueObject.currentTeams;
                let currentAmountPool = entryFee * currentTeams;
                let totalAmountPool = entryFee * leagueObject.maxTeams;
                let winningRate = ((winningAmt / totalAmountPool) * 100);
                let newWiiningAmount = currentAmountPool * winningRate * .01;
                let prizeInfo = leagueConfigObj.prizeInfo;
                logger.info('revise winning winningAmt,newWiiningAmount,currentAmountPool,winningRate,totalAmountPool', winningAmt, newWiiningAmount, currentAmountPool, winningRate, totalAmountPool);
                prizeInfo.forEach(function (value) {
                    let prizeAmount = value.prizeAmt;
                    let winningPercentage = (prizeAmount / winningAmt) * 100;
                    value.prizeAmt = newWiiningAmount * winningPercentage * .01;
                });


                //update status freeze

                //prize revise
                leagueService.updateLeague({
                    leagueId: leagueObject.leagueId,
                    prizeInfo: prizeInfo,
                    winningAmt: newWiiningAmount,
                    status: 'FREEZE'
                }, function (err, updateResponse) {
                    cb(err, updateResponse);
                });

            }

        } else if (leagueObject.currentTeams < leagueConfigObj.minTeams) {
            logger.info('update status CANCELLED league', leagueObject.leagueId);

            if (leagueObject.chipType === 'skill') {
                leagueService.updateLeague({
                    leagueId: leagueObject.leagueId,
                    status: 'CANCELLED'
                }, function (err, updateResponse) {
                    cb(error, updateResponse);
                });
            } else if (leagueObject.chipType === 'real') {
                //cancell  leagues
                //update cancel status
                userJoinedLeagueService.userCancelLeaguesApi({leagueId: leagueObject.leagueId, matchObj: matchObj}, cb);
            }
        } else {
            logger.info('unhandled error occured', leagueObject.leagueId);
        }
    });
}
module.exports = matchLeaguePrizeUpdate;

// Unit Test Case
if (require.main === module) {
    (function () {
        var requestObject = new Object();
        //requestObject.status = "ACTIVE";

        matchLeaguePrizeUpdate(requestObject, function (error, responseObject) {
            logger.info("Response Code - ", responseObject);

        });
    })();
}