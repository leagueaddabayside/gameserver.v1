var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addTourMatches(requestObject, callback) {
	var newTourMatches = new tour_matchesMaster({
		tourId : requestObject.tourId,
		name : requestObject.name,
		relatedName : requestObject.relatedName,
		title : requestObject.title,
		guruLink : requestObject.guruLink,
		venue : requestObject.venue,
		shortName : requestObject.shortName,
		formatType : requestObject.formatType,
		key : requestObject.key,
		startTime : requestObject.startTime,
		teams : requestObject.teams,
		players : requestObject.players,
		status : requestObject.status
	});

	var responseObject = new Object();
	newTourMatches.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addTourMatches;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourId = "";
	requestObject.name = "";
	requestObject.relatedName = "";
	requestObject.title = "";
	requestObject.guruLink = "";
	requestObject.venue = "";
	requestObject.shortName = "";
	requestObject.formatType = "";
	requestObject.key = "";
	requestObject.startTime = "";
	requestObject.teams = "";
	requestObject.players = "";
	console.log(requestObject);

	addTourMatches(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}