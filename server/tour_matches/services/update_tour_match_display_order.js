var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var async = require("async");

function updateTourMatchesDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.tour_matchesList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {matchId : currentRow.matchId};
		tour_matchesMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateTourMatchesDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var tour_matchesList = [ {
		matchId : 1,
		displayOrder : 1
	}, {
		matchId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.tour_matchesList = tour_matchesList;
	console.log(requestObject);

	updateTourMatches(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}