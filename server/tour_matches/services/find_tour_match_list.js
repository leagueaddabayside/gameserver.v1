var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function findTourMatchesList(requestObject, callback) {
	var query = tour_matchesMaster.find({});
	query.sort("startDate");
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);

	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);

	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").in(requestObject.status);

	if (typeof requestObject.isLeaguePrizeUpdate !== "undefined" && requestObject.isLeaguePrizeUpdate !== null)
		query.where("isLeaguePrizeUpdate").equals(requestObject.isLeaguePrizeUpdate);

	if (typeof requestObject.isRankUpdated !== "undefined" && requestObject.isRankUpdated !== null)
		query.where("isRankUpdated").equals(requestObject.isRankUpdated);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var tour_matchesArr = data;
			var tour_matchesList = [];
			for (var i=0, length=tour_matchesArr.length; i<length; i++) {
				var currentRow = tour_matchesArr[i];
				var currentObj = {};
				currentObj.matchId = currentRow.matchId;
				currentObj.tourId = currentRow.tourId;
				currentObj.name = currentRow.name;
				currentObj.relatedName = currentRow.relatedName;
				currentObj.title = currentRow.title;
				currentObj.guruLink = currentRow.guruLink;
				currentObj.venue = currentRow.venue;
				currentObj.shortName = currentRow.shortName;
				currentObj.formatType = currentRow.formatType;
				currentObj.key = currentRow.key;
				if (typeof currentRow.startTime !== "undefined" && currentRow.startTime !== null){
					currentObj.startTime = moment(currentRow.startTime).unix();
					currentObj.matchStartTime = currentRow.startTime;
				}


				currentObj.teams = currentRow.teams;
				//currentObj.players = currentRow.players;
				currentObj.status = currentRow.status;
				currentObj.matchPreviewStatus = currentRow.matchPreviewStatus;
				currentObj.isRankUpdated = currentRow.isRankUpdated;
				currentObj.isLeaguePrizeUpdate = currentRow.isLeaguePrizeUpdate;
				tour_matchesList.push(currentObj);
			}
			responseObject.responseData = tour_matchesList;
		callback(null, responseObject);
	});
}

module.exports = findTourMatchesList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findTourMatchesList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}