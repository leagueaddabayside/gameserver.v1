var addTourMatchesService = require("./add_tour_match");
var findTourMatchesByPropertyService = require("./find_tour_match_by_property");
var findTourMatchesListService = require("./find_tour_match_list");
var updateTourMatchesService = require("./update_tour_match");
var updateTourMatchesDisplayOrderService = require("./update_tour_match_display_order");
var fetchTourMatchesListService = require("./fetch_tour_match_list");
var matchLeaguePrizeUpdateService = require("./match_league_prize_update");
var matchLeagueWinningRankUpdateService = require("./match_league_winning_rank_update");
var matchLeagueDistributeWinningService = require("./match_league_distribute_winning");

// Require

module.exports.addTourMatches = addTourMatchesService;
module.exports.findTourMatchesByProperty = findTourMatchesByPropertyService;
module.exports.findTourMatchesList = findTourMatchesListService;
module.exports.updateTourMatches = updateTourMatchesService;
module.exports.updateTourMatchesDisplayOrder = updateTourMatchesDisplayOrderService;
module.exports.fetchTourMatchesListService = fetchTourMatchesListService;
module.exports.matchLeaguePrizeUpdateService = matchLeaguePrizeUpdateService;
module.exports.matchLeagueWinningRankUpdateService = matchLeagueWinningRankUpdateService;
module.exports.matchLeagueDistributeWinningService = matchLeagueDistributeWinningService;

// Export
