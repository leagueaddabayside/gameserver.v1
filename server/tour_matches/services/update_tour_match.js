var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateTourMatches(requestObject, callback) {
	var updateObject = new Object();
	if(typeof requestObject.name !== 'undefined' && requestObject.name !== null)
		updateObject.name = requestObject.name;

	if(typeof requestObject.title !== 'undefined' && requestObject.title !== null)
		updateObject.title = requestObject.title;

	if(typeof requestObject.teams !== 'undefined' && requestObject.teams !== null)
		updateObject.teams = requestObject.teams;

	if(typeof requestObject.venue !== 'undefined' && requestObject.venue !== null)
		updateObject.venue = requestObject.venue;

	if(typeof requestObject.relatedName !== 'undefined' && requestObject.relatedName !== null)
		updateObject.relatedName = requestObject.relatedName;
	if(typeof requestObject.shortName !== 'undefined' && requestObject.shortName !== null)
		updateObject.shortName = requestObject.shortName;
	if(typeof requestObject.guruLink !== 'undefined' && requestObject.guruLink !== null)
		updateObject.guruLink = requestObject.guruLink;
	if(typeof requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	if(typeof requestObject.startTime !== 'undefined' && requestObject.startTime !== null)
		updateObject.startTime = requestObject.startTime;

	if(typeof requestObject.players !== 'undefined' && requestObject.players !== null)
		updateObject.players = requestObject.players;
	if(typeof requestObject.matchPreviewStatus !== 'undefined' && requestObject.matchPreviewStatus !== null)
		updateObject.matchPreviewStatus = requestObject.matchPreviewStatus;

	if(typeof requestObject.isRankUpdated !== 'undefined' && requestObject.isRankUpdated !== null)
		updateObject.isRankUpdated = requestObject.isRankUpdated;

	if(typeof requestObject.isLeaguePrizeUpdate !== 'undefined' && requestObject.isLeaguePrizeUpdate !== null)
		updateObject.isLeaguePrizeUpdate = requestObject.isLeaguePrizeUpdate;

	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {matchId: requestObject.matchId};
	tour_matchesMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateTourMatches;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.matchId = 567;
	requestObject.players = [];


	updateTourMatches(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}