var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addMultiTourMatches(requestObject, callback) {

	var responseObject = new Object();

	tour_matchesMaster.create(requestObject, function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});


}

module.exports = addMultiTourMatches;

// Unit Test Case
if (require.main === module) {
	var requestObject = [{tourId: 2, name: 'IND Vs Aus 7 match', relatedName: '7st OD1',title : '',shortName : 'IND Vs Aus',teams: [{code: 'IND',name: 'INDIA'},{code: 'AUS', name: 'AUSTRALIA'}], venue: 'Austrailia', startDate: '', status: 'ACTIVE'},
		{tourId: 2, name: 'IND Vs Aus 8 match', relatedName: '8nd OD1',title : '',shortName : 'IND Vs Aus',teams: [{code: 'IND',name: 'INDIA'},{code: 'AUS', name: 'AUSTRALIA'}], venue: 'Austrailia', startDate: '', status: 'ACTIVE'},
		{tourId: 2, name: 'IND Vs Aus 9 match', relatedName: '9rd OD1',title : '',shortName : 'IND Vs Aus',teams: [{code: 'IND',name: 'INDIA'},{code: 'AUS', name: 'AUSTRALIA'}], venue: 'Austrailia', startDate: '', status: 'ACTIVE'} ];

	console.log(requestObject);

	addMultiTourMatches(requestObject, function(error, responseObject) {
		console.log("Response Code - " , responseObject);

	});
}