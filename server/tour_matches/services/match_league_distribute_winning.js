var logger = require("../../utils/logger").gameLogs;
var leagueConfigService = require("../../league_config_master/services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueService = require('../../league_master/services/index');
var tourMatchService = require('../../tour_matches/services/index');
var userJoinedLeagueService = require('../../user_joined_leagues/services/index');

async = require("async");

function matchLeagueDistributeWinning(requestObject, cb) {
    //logger.info("addLeagueConfig API :- Request - %j", requestObject);

    var responseObject = new Object();
    tourMatchService.findTourMatchesByProperty({
        matchId: requestObject.matchId
    }, function (err, matchListReponseData) {
        if (err) {
            responseObject.responseCode = responseCode.MONGO_ERROR;
            cb(err,responseObject);
            return;
        }
        // logger.info('matchListReponseData', matchListReponseData);
        let matchObj = matchListReponseData.responseData;

        if(matchObj.status !== 'UNDER_REVIEW'){
            logger.info('status should be under review',matchObj.status);
            responseObject.responseCode = responseCode.MATCH_STATUS_UNDER_REVIEW;
            cb(err,responseObject);
            return;
        }

        logger.info('matchObj', matchObj.matchId);

        if(!matchObj.isRankUpdated){
            logger.info('rank not updated');
            responseObject.responseCode = responseCode.RANK_NOT_UPDATED;
            cb(err,responseObject);
            return;
        }


        let leagueRequestObject = {
            matchArr: [matchObj.matchId],
            statusArr: ['FREEZE']
        };
        //logger.info('leagueRequestObject', leagueRequestObject);

        leagueService.findLeagueList(leagueRequestObject, function (err, leagueReponseData) {
            // logger.info('leagueReponseData', leagueReponseData);
            if (err) {
                responseObject.responseCode = responseCode.MONGO_ERROR;
                cb(null,responseObject);
                return;
            }

            if(!leagueReponseData.responseData){
                responseObject.responseCode = responseCode.SUCCESS;
                cb(null,responseObject);
                return;
            }
            if(leagueReponseData.responseData && leagueReponseData.responseData.length == 0){
                responseObject.responseCode = responseCode.SUCCESS;
                cb(null,responseObject);
                return;
            }


            // 1st para in async.eachSeries() is the array of items
            async.eachSeries(leagueReponseData.responseData,
                // 2nd param is the function that each item is passed to
                function (leagueObject, callback) {
                    logger.info('leagueObject', leagueObject);


                    if (matchObj && matchObj.status === 'UNDER_REVIEW') {
                        logger.info('matchObj Running', matchObj.matchId);

                        if (leagueObject.chipType === 'real') {
                            logger.info('leagueType NOT_CONFIRMED', leagueObject.leagueId);
                            handleRealTypeLeague(leagueObject,matchObj, callback)

                            logger.info('leagueType CONFIRMED', leagueObject.leagueId);

                        } else if (leagueObject.chipType === 'skill') {
                            logger.info('leagueType NOT_CONFIRMED', leagueObject.leagueId);
                            leagueService.updateLeague({
                                leagueId: leagueObject.leagueId,
                                status: 'DONE'
                            }, function (err, updateResponse) {
                                callback(err, updateResponse);
                            });
                        }else{
                            logger.info('chip type not found leagueType NOT_CONFIRMED', leagueObject.leagueId);

                            callback(null);
                        }
                    }

                },
                // 3rd param is the function to call when everything's done
                function (err) {
                    // All tasks are done now
                    if (err) {
                        logger.info('err final done', err);
                        responseObject.responseCode = responseCode.SOME_INTERNAL_ERROR;
                        cb(null, responseObject);
                        return;
                    }
                    responseObject.responseCode = responseCode.SUCCESS;
                    cb(null, responseObject);
                    return;

                }
            );


        });


    });
}


function handleRealTypeLeague(leagueObject,matchObj, cb) {


    leagueConfigService.findLeagueConfigByProperty({configId: leagueObject.configId}, function (err, leagueConfigResponse) {
        let leagueConfigObj = leagueConfigResponse.responseData;
        logger.info('leagueConfigObj,leagueObject', leagueObject);
        logger.info('leagueConfigObj,leagueObject', leagueObject.currentTeams, leagueObject.maxTeams, leagueConfigObj.minTeams);

        userJoinedLeagueService.userWinningLeaguesApi({leagueId: leagueObject.leagueId,matchObj :matchObj,leagueObject : leagueObject}, cb);

    });
}
module.exports = matchLeagueDistributeWinning;

// Unit Test Case
if (require.main === module) {
    (function () {
        var requestObject = new Object();
        requestObject.matchId = 3;

        matchLeagueDistributeWinning(requestObject, function (error, responseObject) {
            logger.info("Response Code - ", responseObject);

        });
    })();
}