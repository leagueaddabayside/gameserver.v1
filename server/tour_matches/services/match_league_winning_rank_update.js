var logger = require("../../utils/logger").gameLogs;
var leagueConfigService = require("../../league_config_master/services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var leagueService = require('../../league_master/services/index');
var tourMatchService = require('../../tour_matches/services/index');
var userMatchTeamService = require('../../user_match_teams/services/index');
var utility = require("../../utils/utility");

var userJoinedLeagueService = require('../../user_joined_leagues/services/index');
const leagueTeamRankRedis = require('../../redis/league_team_rank');

const async = require("async");

function matchLeagueWinningRankUpdate(requestObject, cb) {
    logger.info("matchLeagueWinningRankUpdate API :- Request - %j", requestObject);

    var responseObject = new Object();

    let leagueRequestObject = {
        matchId: requestObject.matchId,
        statusArr: ['FREEZE']
    };
    //logger.info('leagueRequestObject', leagueRequestObject);

    leagueService.findLeagueList(leagueRequestObject, function (err, leagueReponseData) {
        // logger.info('leagueReponseData', leagueReponseData);
        async.eachSeries(leagueReponseData.responseData,
            function (leagueObject, callback) {
                logger.info('leagueObject', leagueObject);

                if (leagueObject.maxTeams == 2) { //in case of headsup league
                    userMatchTeamService.findUserMatchTeamListApi({leagueId: leagueObject.leagueId}, function (err, leagueTeamRespone) {
                        let userTeamArr = leagueTeamRespone.responseData;
                        if(userTeamArr.length == 2){
                            let firstTeam = userTeamArr[0];
                            let secondTeam = userTeamArr[1];
                            let firstTeamPlayers = getTeamPlayers(firstTeam,leagueObject.leagueId);
                            let secondTeamPlayers = getTeamPlayers(secondTeam,leagueObject.leagueId);

                            let isEqualTeam = utility.checkDuplicateTeam(firstTeamPlayers, secondTeamPlayers);
                            if (isEqualTeam) {
                                leagueObject.isEqualTeam = true;
                            } else {
                                leagueObject.isEqualTeam = false;
                            }
                        }

                        handleLeaguePrizeRank(leagueObject, callback);

                    });
                } else {
                    handleLeaguePrizeRank(leagueObject, callback);

                }


            },
            // 3rd param is the function to call when everything's done
            function (err) {
                // All tasks are done now
                if (err) {
                    logger.info('err final done', err);
                    responseObject.respCode = responseCode.SOME_INTERNAL_ERROR;
                    cb(null, responseObject);
                    return;
                }

                responseObject.respCode = responseCode.SUCCESS;
                cb(null, responseObject);
                return;
            }
        );


    });

}


function handleLeaguePrizeRank(leagueObject, cb) {

    let prizeInfo = leagueObject.prizeInfo;
    let rankPrizeMap = new Map();

    if (prizeInfo && prizeInfo.length > 0) {
        prizeInfo.forEach(function (value) {

            let prizeAmount = value.prizeAmt;
            let startRank = value.startRank;
            let endRank = value.toRank;
            if (endRank) {
                for (let rank = startRank; rank <= endRank; rank++) {
                    rankPrizeMap.set(rank, prizeAmount);
                }
            } else {
                rankPrizeMap.set(startRank, prizeAmount);

            }

        });
    }
    let rankUserMap = new Map();
    let winnersCount = leagueObject.winnersCount;

    let leagueRankPointsMap = new Map();

    leagueTeamRankRedis.rank(leagueObject.leagueId, -1, function (err, resp) {
        logger.info('getLeagueRankPointsRedis resp', resp);

        if (resp) {
            leagueRankPointsMap = resp;

            userJoinedLeagueService.findUserJoinedListApi({leagueId: leagueObject.leagueId}, function (err, userJoinedLeagueResponse) {

                let joinedLeagueArr = userJoinedLeagueResponse.responseData;

                let joinedUserLength = joinedLeagueArr.length;
                if (winnersCount > joinedUserLength) {  //because joined user less than no of winners
                    winnersCount = joinedUserLength;
                }

                for (let i = 0, length = joinedLeagueArr.length; i < length; i++) {
                    let currentRow = joinedLeagueArr[i];
                    if (leagueRankPointsMap.has(currentRow.leagueJoinedId)) {
                        let rank = leagueRankPointsMap.get(currentRow.leagueJoinedId).rank;
                        if (rank <= winnersCount) {
                            if (rankUserMap.has(rank)) {
                                rankUserMap.get(rank).push(currentRow);
                            } else {
                                let arr = [];
                                arr.push(currentRow);
                                rankUserMap.set(rank, arr);
                            }

                        }

                    }

                }

                let rankWinningAmountMap = new Map();

                for (let j = winnersCount; j > 0; j--) {
                    if (rankUserMap.has(j)) {
                        let winAmt = rankPrizeMap.get(j);
                        let noOfWinners = rankUserMap.get(j).length;
                        let newWinAmt = (winAmt / noOfWinners).toFixed(2);
                        rankWinningAmountMap.set(j, newWinAmt);
                    } else {
                        let winAmt = rankPrizeMap.get(j);
                        let nextWinAmt = rankPrizeMap.get(j - 1);
                        let totalWinAmt = winAmt + nextWinAmt;
                        rankPrizeMap.set(j - 1, totalWinAmt);
                    }
                }
                logger.info('rankWinningAmountMap', rankWinningAmountMap);
                async.eachSeries(joinedLeagueArr, function (joinedLeagueRow, callback) {

                    if (leagueRankPointsMap.has(joinedLeagueRow.leagueJoinedId)) {
                        let points = leagueRankPointsMap.get(joinedLeagueRow.leagueJoinedId).teamPoints;
                        let rank = leagueRankPointsMap.get(joinedLeagueRow.leagueJoinedId).rank;
                        let prizeAmount = 0;
                        if (rankWinningAmountMap.has(rank)) {
                            prizeAmount = rankWinningAmountMap.get(rank);
                        }

                        let updateJoinedRequestObj = {};
                        updateJoinedRequestObj.winningType = 'WINNING';
                        if(leagueObject.maxTeams == 2 && leagueObject.isEqualTeam){
                            prizeAmount = leagueObject.entryFee;
                            updateJoinedRequestObj.winningType = 'WINNING_REFUND';
                        }


                        updateJoinedRequestObj.points = points;
                        updateJoinedRequestObj.rank = rank;
                        updateJoinedRequestObj.winningAmt = prizeAmount;
                        if (prizeAmount == 0) {
                            updateJoinedRequestObj.winningStatus = 'NOT_WINNER';
                        } else {
                            updateJoinedRequestObj.winningStatus = 'PENDING';
                        }
                        updateJoinedRequestObj.leagueJoinedId = joinedLeagueRow.leagueJoinedId;
                        userJoinedLeagueService.updateUserJoinedDataApi(updateJoinedRequestObj, function (err, updateJoinedRowResponse) {
                            callback(err);
                        });

                    } else {
                        callback(true);
                    }


                }, function (err, joinedLeagueResp) {
                    cb(err, joinedLeagueResp);
                });
            });


        } else {
            cb(err, resp);
        }

    })

}

function getTeamPlayers(teamData, leagueId) {
    //get team id from joined league
    //get players from team id
    let teamId = 0;
    let players = [];
    for (let i = 0; i < teamData.joinedLeagues.length; i++) {
        let joinedRow = teamData.joinedLeagues[i];
        if (joinedRow.leagueId == leagueId) {
            teamId = joinedRow.teamId;
        }
    }

    for (let i = 0; i < teamData.teams.length; i++) {
        let teamRow = teamData.teams[i];
        if (teamRow.teamId == teamId) {
            players = teamRow.players;
        }
    }

    return players;

}

module.exports = matchLeagueWinningRankUpdate;

// Unit Test Case
if (require.main === module) {
    (function () {
        var requestObject = new Object();
        requestObject.matchId = 7;

        matchLeagueWinningRankUpdate(requestObject, function (error, responseObject) {
            logger.info("Response Code - ", responseObject);

        });
    })();
}