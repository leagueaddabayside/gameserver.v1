var tour_matchesMaster = require("../models/tour_matches_master");
var UserMatchTeamsService = require("../../user_match_teams/services/index");
var matchService = require('./index');
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var TEAM_NAMES = require("../../utils/team_names").teamNames;

function fetchTourMatchesList(requestObject, callback) {
	var query = tour_matchesMaster.find({});

	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);

	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").in(requestObject.status);

	query.sort("startTime");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var tour_matchesArr = data;
			var fetchMatchResponse = {};
			var tour_matchesList = [];
			var tourNextTimeObj = {};
			var isFirstActiveMatchTime = false;
			var matchArr = [];
			for (var i=0, length=tour_matchesArr.length; i<length; i++) {
				var currentRow = tour_matchesArr[i];
				if(requestObject.tourMap[currentRow.tourId]){
					var currentObj = {};
					currentObj.tourShortName = requestObject.tourMap[currentRow.tourId];
					matchArr.push(currentRow.matchId);
					currentObj.matchId = currentRow.matchId;
					currentObj.tourId = currentRow.tourId;
					currentObj.name = currentRow.name;
					currentObj.relatedName = currentRow.relatedName;
					currentObj.title = currentRow.title;
					currentObj.guruLink = currentRow.guruLink;
					currentObj.venue = currentRow.venue;
					currentObj.shortName = currentRow.shortName;
					currentObj.formatType = currentRow.formatType;
					currentObj.key = currentRow.key;

					if (typeof currentRow.startTime !== "undefined" && currentRow.startTime !== null){
						currentObj.startTime = moment(currentRow.startTime).unix();
						if(currentRow.status === 'ACTIVE'){
							let currentTime = new Date();
							if (moment(currentRow.startTime).isBefore(currentTime)){
								currentRow.status = 'RUNNING';
								matchService.updateTourMatches({matchId : currentRow.matchId , status : 'RUNNING'},function(err,resp){
									logger.info(err,resp);
								})
							}
						}
					}


					let aTeamInfo = TEAM_NAMES[currentRow.teams.a.key.toLowerCase()];
					if (aTeamInfo) {
						currentRow.teams.a.aliasTName = aTeamInfo.displayTeamName;
					} else {
						currentRow.teams.a.aliasTName = currentRow.teams.a.key.toUpperCase();
					}
					let bTeamInfo = TEAM_NAMES[currentRow.teams.b.key.toLowerCase()];
					if (bTeamInfo) {
						currentRow.teams.b.aliasTName = bTeamInfo.displayTeamName;
					} else {
						currentRow.teams.b.aliasTName = currentRow.teams.b.key.toUpperCase();
					}
					currentObj.teams = currentRow.teams;
					currentObj.leagueCount = 0;
					currentObj.totalTeams = 0;
					//currentObj.players = currentRow.players;
					currentObj.status = currentRow.status;

					if(!isFirstActiveMatchTime){
						if(currentObj.status === 'ACTIVE'){
							isFirstActiveMatchTime = true;
							tourNextTimeObj[-1] = currentObj.startTime;
						}
					}
					if(!tourNextTimeObj[currentRow.tourId]){
						if(currentObj.status === 'ACTIVE'){
							tourNextTimeObj[currentRow.tourId] = currentObj.startTime;
						}
					}

					tour_matchesList.push(currentObj);
				}

			}


		UserMatchTeamsService.findUserMatchTeamListApi({matchArr : matchArr,userId : requestObject.userId},function(error,userMatchArr){
		logger.info('findUserMatchTeamListApi',userMatchArr);
		if(userMatchArr.responseData){

			let userMatchMap = userMatchArr.responseData;
			for (var i=0, length=tour_matchesList.length; i < length; i++) {
				var tourMatchRow = tour_matchesList[i];
				if(userMatchMap.has(tourMatchRow.matchId)){

					var userMatchRow = userMatchMap.get(tourMatchRow.matchId);
					logger.info('userMatchRow',userMatchRow);

					if(userMatchRow.leagueCount){
						tourMatchRow.leagueCount = userMatchRow.leagueCount;
					}
					if(userMatchRow.totalTeams){
						tourMatchRow.totalTeams = userMatchRow.totalTeams;
					}
					logger.info('tourMatchRow',tourMatchRow);

				}
			}

		}


			fetchMatchResponse.matchList = tour_matchesList;
			fetchMatchResponse.nextTimeTour = tourNextTimeObj;
			responseObject.responseData = fetchMatchResponse;
			callback(null, responseObject);

		});


	});
}

module.exports = fetchTourMatchesList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	fetchTourMatchesList(requestObject, function(error, responseObject) {
		logger.info("Response Code - " + responseObject.responseCode);
		if (error)
			logger.info("Error - " + error);
		else
			logger.info("Response Data - " + responseObject.responseData);
	});
}