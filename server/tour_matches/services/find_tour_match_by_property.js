var tour_matchesMaster = require("../models/tour_matches_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var TEAM_NAMES = require("../../utils/team_names").teamNames;

function findTourMatchesByProperty(requestObject, callback) {
    var query = tour_matchesMaster.findOne({});
    if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
        query.where("matchId").equals(requestObject.matchId);
    if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
        query.where("tourId").equals(requestObject.tourId);
    if (typeof requestObject.key !== "undefined" && requestObject.key !== null)
        query.where("key").equals(requestObject.key);


    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var currentRow = data;
        var currentObj = {};
        if (data) {
            currentObj.matchId = currentRow.matchId;
            currentObj.tourId = currentRow.tourId;
            currentObj.name = currentRow.name;
            currentObj.relatedName = currentRow.relatedName;
            currentObj.title = currentRow.title;
            currentObj.guruLink = currentRow.guruLink;
            currentObj.venue = currentRow.venue;
            currentObj.shortName = currentRow.shortName;
            currentObj.formatType = currentRow.formatType;
            currentObj.key = currentRow.key;
            currentObj.startTime = currentRow.startTime;
            currentObj.teams = currentRow.teams;


            let aTeamInfo = TEAM_NAMES[currentRow.teams.a.key.toLowerCase()];
            if (aTeamInfo) {
                currentRow.teams.a.aliasTName = aTeamInfo.displayTeamName;
            } else {
                currentRow.teams.a.aliasTName = currentRow.teams.a.key.toUpperCase();
            }
            let bTeamInfo = TEAM_NAMES[currentRow.teams.b.key.toLowerCase()];
            if (bTeamInfo) {
                currentRow.teams.b.aliasTName = bTeamInfo.displayTeamName;
            } else {
                currentRow.teams.b.aliasTName = currentRow.teams.b.key.toUpperCase();
            }

            currentObj.players = currentRow.players;
            currentObj.status = currentRow.status;
            currentObj.matchPreviewStatus = currentRow.matchPreviewStatus;
            currentObj.isRankUpdated = currentRow.isRankUpdated;
            currentObj.isLeaguePrizeUpdate = currentRow.isLeaguePrizeUpdate;
        }

        responseObject.responseData = currentObj;
        responseObject.responseCode = responseCode.SUCCESS;
        callback(null, responseObject);
    });
}

module.exports = findTourMatchesByProperty;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.matchId = "matchId";
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";

    findTourMatchesByProperty(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}