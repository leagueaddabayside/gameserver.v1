var logger = require("../../utils/logger").gameLogs;
var tour_matchesService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function fetchTourMatchInfo(request, response, next) {
	var requestObject = request.body;
	//console.log("findTourMatchesByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_matchesService.findTourMatchesByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;

			var matchInfo = data.responseData;
			if (typeof matchInfo.startTime !== "undefined" && matchInfo.startTime !== null)
				matchInfo.startTime = moment(matchInfo.startTime).unix();

			matchInfo.totalTeams = requestObject.totalTeams;
			matchInfo.leagueCount = requestObject.leagueCount;

			responseObject.respData = matchInfo;
		}

		//logger.info("findTourMatchesByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = fetchTourMatchInfo;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.matchId = "matchId";
		//requestObject.tourId = "tourId";
		//requestObject.name = "name";
		//requestObject.relatedName = "relatedName";
		//requestObject.title = "title";
		//requestObject.guruLink = "guruLink";
		//requestObject.venue = "venue";
		//requestObject.shortName = "shortName";
		//requestObject.formatType = "formatType";
		//requestObject.key = "key";
		//requestObject.startTime = "startTime";
		//requestObject.teams = "teams";
		//requestObject.players = "players";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchTourMatchInfo(request, response);
	})();
}