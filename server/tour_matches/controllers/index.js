var addTourMatchesApi = require("./add_tour_match");
var findTourMatchesByPropertyApi = require("./find_tour_match_by_property");
var findTourMatchesListApi = require("./find_tour_match_list");
var fetchTourMatchesListApi = require("./fetch_tour_match_list");
var updateTourMatchesApi = require("./update_tour_match");
var updateTourMatchesDisplayOrderApi = require("./update_tour_match_display_order");
var fetchTourMatchInfoApi = require("./fetch_tour_match_info");
var fetchTourMatchInfoScoreCardApi = require("./fetch_tour_match_scorecard");
var dstributeMatchWinningApi = require("./distibute_match_winning");

// Require

module.exports.addTourMatches = addTourMatchesApi;
module.exports.findTourMatchesByProperty = findTourMatchesByPropertyApi;
module.exports.findTourMatchesList = findTourMatchesListApi;
module.exports.fetchTourMatchesList = fetchTourMatchesListApi;
module.exports.updateTourMatches = updateTourMatchesApi;
module.exports.updateTourMatchesDisplayOrder = updateTourMatchesDisplayOrderApi;
module.exports.fetchTourMatchInfo = fetchTourMatchInfoApi;
module.exports.fetchTourMatchInfoScoreCardApi = fetchTourMatchInfoScoreCardApi;
module.exports.dstributeMatchWinningApi = dstributeMatchWinningApi;

// Export
