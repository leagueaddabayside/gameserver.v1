var logger = require("../../utils/logger").gameLogs;
var tour_matchesService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateTourMatchesDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateTourMatchesDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_matchesService.updateTourMatchesDisplayOrder(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("updateTourMatchesDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateTourMatchesDisplayOrder;