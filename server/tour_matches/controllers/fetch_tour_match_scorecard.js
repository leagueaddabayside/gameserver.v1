var logger = require("../../utils/logger").gameLogs;
var tour_matchesService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");
var moment = require("moment"); 

function fetchTourMatchScoreCard(request, response, next) {
	var requestObject = request.body;
	//console.log("findTourMatchesByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_matchesService.findTourMatchesByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;

			var matchInfo = data.responseData;
			var scoreCardPlayers= [];
			let matchDisplayName = '';
			if(matchInfo && matchInfo.name){

				matchDisplayName = matchInfo.shortName +' - '+matchInfo.relatedName;
				var players = matchInfo.players;
				for (var i=0, length=players.length; i<length; i++) {
					var currentPlayer = players[i];

					let playerScoreObj = getPlayerPoints(currentPlayer.firstInnings);
					playerScoreObj.name = currentPlayer.name;
					playerScoreObj.currentPoints += currentPlayer.startingPoint;
					playerScoreObj.startingPoint = currentPlayer.startingPoint;

					scoreCardPlayers.push(playerScoreObj);

					if(currentPlayer.isSecondStart){
						let playerScoreObj = getPlayerPoints(currentPlayer.secondInnings);
						playerScoreObj.name = currentPlayer.name;
						playerScoreObj.startingPoint = 0;
						scoreCardPlayers.push(playerScoreObj);
					}

				}
			}

			responseObject.respData = {scoreCardPlayers:scoreCardPlayers , matchDisplayName : matchDisplayName };
		}

		//logger.info("findTourMatchesByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}


function getPlayerPoints(firstInnings){
	var pointsBreakup = firstInnings.pointBreakup;

	var playerScoreObj = {};
	playerScoreObj.runout = pointsBreakup.runout;
	playerScoreObj.catch = pointsBreakup.catch;
	playerScoreObj.bonus_neg = pointsBreakup.bonus_neg;
	playerScoreObj.bonus_pos = pointsBreakup.bonus_pos;
	playerScoreObj.ec_rate = pointsBreakup.ec_rate;
	playerScoreObj.maidens = pointsBreakup.maidens;
	playerScoreObj.wickets = pointsBreakup.wickets;
	playerScoreObj.fifties_hundred = pointsBreakup.fifties_hundred;
	playerScoreObj.st_rate = pointsBreakup.st_rate;
	playerScoreObj.sixes = pointsBreakup.sixes;
	playerScoreObj.fours = pointsBreakup.fours;
	playerScoreObj.runs = pointsBreakup.runs;
	playerScoreObj.sixes = pointsBreakup.sixes;
	playerScoreObj.sixes = pointsBreakup.sixes;
	playerScoreObj.currentPoints = firstInnings.currentPoints;
	return playerScoreObj;
};

module.exports = fetchTourMatchScoreCard;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.matchId = "matchId";
		//requestObject.tourId = "tourId";
		//requestObject.name = "name";
		//requestObject.relatedName = "relatedName";
		//requestObject.title = "title";
		//requestObject.guruLink = "guruLink";
		//requestObject.venue = "venue";
		//requestObject.shortName = "shortName";
		//requestObject.formatType = "formatType";
		//requestObject.key = "key";
		//requestObject.startTime = "startTime";
		//requestObject.teams = "teams";
		//requestObject.players = "players";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		fetchTourMatchScoreCard(request, response);
	})();
}