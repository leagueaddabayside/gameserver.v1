var logger = require("../../utils/logger").gameLogs;
var tour_matchesService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateTourMatches(request, response, next) {
	var requestObject = request.body;
	//console.log("updateTourMatches API :- Request - %j", requestObject);

	var responseObject = new Object();
	tour_matchesService.updateTourMatches(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateTourMatches API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateTourMatches;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.tourId = "";
		requestObject.name = "";
		requestObject.relatedName = "";
		requestObject.title = "";
		requestObject.guruLink = "";
		requestObject.venue = "";
		requestObject.shortName = "";
		requestObject.formatType = "";
		requestObject.key = "";
		requestObject.startTime = "";
		requestObject.teams = "";
		requestObject.players = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateTourMatches(request, response);
	})();
}