var logger = require("../../utils/logger").gameLogs;
var userTeamService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findOpponentTeamInfo(request, response, next) {
	var requestObject = request.body;
	//console.log("findOpponentTeamInfo API :- Request - %j", requestObject);

	var responseObject = new Object();
	userTeamService.findOpponentTeamInfoApi(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("findOpponentTeamInfo API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findOpponentTeamInfo;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();


		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findOpponentTeamInfo(request, response);
	})();
}