var logger = require("../../utils/logger").gameLogs;
var userTeamService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findUserTeamByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findTourPlayersByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	userTeamService.findUserTeamByPropertyApi(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("findUserTeamByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findUserTeamByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.tourId = "tourId";
		//requestObject.name = "name";
		//requestObject.shortName = "shortName";
		//requestObject.key = "key";
		//requestObject.identifyRoles.keeper = "identifyRoles.keeper";
		//requestObject.identifyRoles.batsman = "identifyRoles.batsman";
		//requestObject.identifyRoles.bowler = "identifyRoles.bowler";
		//requestObject.points = "points";
		//requestObject.credit = "credit";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findUserTeamByProperty(request, response);
	})();
}