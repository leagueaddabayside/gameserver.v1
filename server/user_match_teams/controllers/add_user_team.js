var logger = require("../../utils/logger").gameLogs;
var userTeamService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addUserTeam(request, response, next) {
	var requestObject = request.body;
	logger.info("addUserTeam API :- Request - %j", requestObject);

	var responseObject = new Object();
	userTeamService.addUserTeamApi(requestObject, function(error, data) {
	if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		//logger.info("addUserTeam API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addUserTeam;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				logger.info(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.tourId = "";
		requestObject.name = "";
		requestObject.shortName = "";
		requestObject.key = "";
		requestObject.keeper = "";
		requestObject.batsman = "";
		requestObject.bowler = "";
		requestObject.points = "";
		requestObject.credit = "";
		requestObject.createBy = "";

		logger.info("Request Data - " + requestObject);
		request.body = requestObject;
		addUserTeam(request, response);
	})();
}