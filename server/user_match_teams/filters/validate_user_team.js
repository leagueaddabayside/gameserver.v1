/**
 * Created by sumit on 4/20/2017.
 */
var logger = require('../../utils/logger.js').gameLogs;
const responseMessage = require('../../utils/response_message');
const responseCode = require('../../utils/response_code');
const tourPlayersService = require('../../tour_players/services/index');
var moment = require("moment");


function validateUserTeam(request, response, next) {
    var params = request.body;

    var matchInfoTeam = params.matchInfo.teams;
    var responseObject = new Object();

    if (!(matchInfoTeam && matchInfoTeam.a)) {
        responseObject.respCode = responseCode.MATCH_DOES_NOT_EXIST;
        responseObject.message = responseMessage[responseCode.MATCH_DOES_NOT_EXIST];
        response.json(responseObject);
        return;
    }

    var teamArr = [];
    teamArr.push(matchInfoTeam.a.name);
    teamArr.push(matchInfoTeam.b.name);

    var requestObject = {
        tourId: params.matchInfo.tourId,
        status: 'ACTIVE',
        teamsArr: teamArr,
        formatType: params.matchInfo.formatType
    };
    tourPlayersService.findTourPlayersList(requestObject, function (error, data) {
        if (data.responseCode !== responseCode.SUCCESS) {
            responseObject.respCode = data.responseCode;
            responseObject.message = responseMessage[data.responseCode];
            response.json(responseObject);
            return;
        }
        responseObject.respCode = data.responseCode;
        var tourPlayerList = data.responseData;
        let playerMap = new Map();
        for (var i = 0, length = tourPlayerList.length; i < length; i++) {
            let currentRow = tourPlayerList[i];
            playerMap.set(currentRow.playerId, currentRow);
        }

        let teamPlayers = params.players || [];
        let totalPoints = 0;
        let wkPlayers = 0;
        let batter = 0;
        let bowler = 0;
        let allRounder = 0;
        let bonusPlayers = 0;
        let captain = 0;
        let viceCaptain = 0;
        let isValidTeam = true;
        //console.log(teamPlayers);
        let team1Name = '';
        let team2Name = '';
        let team1Players = 0;
        let team2Players = 0;
        let playerSet = new Set();
        for (let k = 0; k < teamPlayers.length; k++) {
            let currentPlayer = teamPlayers[k];
            if (playerMap.has(currentPlayer.playerId)) {
                playerSet.add(currentPlayer.playerId);

                let player = playerMap.get(currentPlayer.playerId);
                let teamName = player.team.card_name.toUpperCase();
                if(team1Name == '' || team1Name == teamName){
                    team1Name = teamName;
                    team1Players++;
                }else if(team2Name == '' || team2Name == teamName){
                    team2Name = teamName;
                    team2Players++;
                }else{
                    logger.trace('Team does not exist',player.team,team1Name,team2Name);
                    isValidTeam = false;
                }

                if (currentPlayer.isBonus == true) {
                    bonusPlayers++;
                } else {
                    if (player.batsman == true && player.bowler == true) {
                        allRounder++;
                    } else if (player.batsman == true) {
                        batter++;
                    } else if (player.bowler == true) {
                        bowler++;
                    } else if (player.keeper == true) {
                        wkPlayers++;
                    }else{
                        logger.trace('player has no role',player);
                        isValidTeam = false;
                    }
                    totalPoints += player.credit;
                }

                if (currentPlayer.isCaptain == true) {
                    captain++;
                }
                if (currentPlayer.isViceCaptain == true) {
                    viceCaptain++;
                }

                if (currentPlayer.isCaptain == true && currentPlayer.isViceCaptain == true) {
                    logger.trace('same player captain and vice captain',currentPlayer);
                    isValidTeam = false;
                }

                currentPlayer.playerCredit = player.credit;
                currentPlayer.wicketKeeper = player.keeper;
                currentPlayer.bowler = player.bowler;
                currentPlayer.batter = player.batsman;
                currentPlayer.shortName = player.shortName;
                currentPlayer.playerName = player.name;
            } else {
                logger.trace('player is not exist',currentPlayer);
                isValidTeam = false;
            }
        }

        if (isValidTeam) {
            if (totalPoints > 100) {
                logger.trace('totalPoints validation failed',totalPoints);
                isValidTeam = false;
            }

            if (bonusPlayers > 1) {
                logger.trace('bonusPlayers validation failed',bonusPlayers);
                isValidTeam = false;
            }

            if (wkPlayers != 1) {
                logger.trace('wkPlayers validation failed',wkPlayers);
                isValidTeam = false;
            }

            if (allRounder < 1 || allRounder > 3) {
                logger.trace('allRounder validation failed',allRounder);
                isValidTeam = false;
            }

            if (batter < 3 || batter > 5) {
                logger.trace('batter validation failed',batter);
                isValidTeam = false;
            }

            if (bowler < 3 || bowler > 5) {
                logger.trace('bowler validation failed',bowler);
                isValidTeam = false;
            }
            if(team1Players < 4 || team1Players > 7){
                logger.trace('team1Players validation failed',team1Players);
                isValidTeam = false;
            }
            if(team2Players < 4 || team2Players > 7){
                logger.trace('team2Players validation failed',team2Players);
                isValidTeam = false;
            }

            if(captain != 1){
                logger.trace('captain validation failed',captain);
                isValidTeam = false;
            }

            if(viceCaptain != 1){
                logger.trace('viceCaptain validation failed',viceCaptain);
                isValidTeam = false;
            }

            if(playerSet.size != 11){
                logger.trace('team size validation failed',playerSet);
                isValidTeam = false;
            }

        }

        if (isValidTeam) {
            logger.info(isValidTeam);
            next();
        } else {
            logger.error(responseObject);
            responseObject.respCode = responseCode.INVALID_USER_TEAM;
            responseObject.message = responseMessage[responseCode.INVALID_USER_TEAM];
            response.json(responseObject);
            return;
        }

    });


}

module.exports = validateUserTeam;
