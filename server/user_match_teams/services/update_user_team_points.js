var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateUserTeamPointsApi(requestObject, callback) {
    var responseObject = new Object();

    var query = UserMatchTeams.findOne({userId: requestObject.userId, matchId: requestObject.matchId});

    query.exec(function (error, teamResponse) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        if (teamResponse) {

            if (typeof requestObject.userTeamObj !== "undefined" && requestObject.userTeamObj !== null) {
                let userTeamObj = requestObject.userTeamObj;
                for (var i = 0; i < teamResponse.teams.length; i++) {
                    var child = teamResponse.teams[i];
                    child.teamPoints = userTeamObj[child.teamId] || 0;
                }
                teamResponse.save(function (error, data) {
                    if (error) {
                        logger.error(error);
                        responseObject.responseCode = responseCode.MONGO_ERROR;
                        callback(error, responseObject);
                        return;
                    }
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseObject.responseData = data;
                    callback(null, responseObject);
                });
            }


        } else {
            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData = data;
            callback(null, responseObject);
        }

    })


}

module.exports = updateUserTeamPointsApi;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.name = "";
    requestObject.shortName = "";
    requestObject.keeper = "";
    requestObject.batsman = "";
    requestObject.bowler = "";
    console.log(requestObject);

    updateUserTeamPointsApi(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}