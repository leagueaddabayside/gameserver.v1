var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var userTeamService = require("../services/index");
var CONSTANT = require("../../utils/constant");
var moment = require("moment");
var utility = require("../../utils/utility");


function addUserTeam(requestObject, callback) {
	var responseObject = new Object();

	let matchInfo = requestObject.matchInfo;
	let currentTime = moment(new Date()).unix();
	if (matchInfo.status !== 'ACTIVE' || matchInfo.startTime < currentTime) {
		responseObject.responseCode = responseCode.MATCH_NOT_ALLOWED_TO_JOIN_LEAGUE;
		callback(true, responseObject);
		return;
	}

	var query = UserMatchTeams.findOne({userId : requestObject.userId,matchId : requestObject.matchId});

	query.exec(function(error,teamResponse){
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
	if(teamResponse){

		var totalTeams = teamResponse.totalTeams;
		var newTeamCount = totalTeams+1;

		if(newTeamCount > CONSTANT.MAX_USER_TEAM_ALLOWED){
			responseObject.responseCode = responseCode.MAX_TEAM_ALLOWED_ERROR;
			callback(error, responseObject);
			return;
		}

		let teamObj ={teamId : newTeamCount,players : requestObject.players,teamPoints : 0};
		teamResponse.totalTeams = newTeamCount;
		let teams = teamResponse.teams;

		for(let i=0;i< teams.length;i++){
			let currentTeam = teams[i];

			let isEqualTeam = utility.checkDuplicateTeam(currentTeam.players,requestObject.players);
			if(isEqualTeam){
				responseObject.responseCode = responseCode.DUPLICATE_TEAM;
				callback(error, responseObject);
				return;
			}
		}

		teamResponse.teams.push(teamObj);

		logger.info('update team',teamResponse);
		teamResponse.save(function(error, data) {
			if (error) {
				logger.error(error);
				responseObject.responseCode = responseCode.MONGO_ERROR;
				callback(error, responseObject);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			callback(null, responseObject);
		});

	}else{
		let teamObj =[{teamId : 1,players : requestObject.players,teamPoints : 0}];

		var newUserTeam = new UserMatchTeams({
			tourId : requestObject.tourId,
			userId : requestObject.userId,
			matchId : requestObject.matchId,
			screenName : requestObject.profile.screenName,
			totalTeams : 1,
			leagueCount : 0,
			teams : teamObj
		});

		newUserTeam.save(function(error, data) {
			if (error) {
				logger.error(error);
				responseObject.responseCode = responseCode.MONGO_ERROR;
				callback(error, responseObject);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			callback(null, responseObject);
		});

	}


	})


}

module.exports = addUserTeam;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.tourId = "";
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.key = "";
	requestObject.keeper = "";
	requestObject.batsman = "";
	requestObject.bowler = "";
	requestObject.points = "";
	requestObject.credit = "";
	requestObject.createBy = "";
	logger.info(requestObject);

	addUserTeam(requestObject, function(error, responseObject) {
		logger.info("Response Code - " + responseObject.responseCode);
		if (error)
			logger.info("Error - " + error);
		else
			logger.info("Response Data - " + responseObject.responseData);
	});
}