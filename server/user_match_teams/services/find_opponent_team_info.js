var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findOpponentTeamInfoApi(requestObject, callback) {
    var query = UserMatchTeams.findOne({});
    let matchInfo = requestObject.matchInfo;

    var players = matchInfo.players;
    let matchPlayerPointMap = new Map();
    for (var i = 0, length = players.length; i < length; i++) {
        var currentRow = players[i];
        var totalPoints = currentRow.totalPoints || 0;
        matchPlayerPointMap.set(currentRow.playerId, totalPoints);
    }
    if (typeof requestObject.opponentId !== "undefined" && requestObject.opponentId !== null)
        query.where("userId").equals(requestObject.opponentId);
    if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
        query.where("tourId").equals(requestObject.tourId);
    if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
        query.where("matchId").equals(requestObject.matchId);

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var currentRow = data;
        var currentObj = {};
        if (data) {
            currentObj.userId = currentRow.userId;
            currentObj.matchId = currentRow.matchId;
            currentObj.tourId = currentRow.tourId;
            currentObj.screenName = currentRow.screenName;

            if (typeof requestObject.teamId !== "undefined" && requestObject.teamId !== null) {
                for (var i = 0; i < currentRow.teams.length; i++) {
                    var child = currentRow.teams[i];
                    if (child.teamId == requestObject.teamId) {
                        let currentTeam = {};
                        currentTeam.teamId = child.teamId;
                        currentTeam.teamPoints = child.teamPoints;
                        if(requestObject.matchTeamPoints){
                            let teamKey = currentRow.userId + ':' + currentTeam.teamId;
                            let teamPoints = requestObject.matchTeamPoints[teamKey];
                            if(teamPoints){
                                currentTeam.teamPoints = teamPoints;

                            }
                        }
                        for (let i = 0, length = child.players.length; i < length; i++) {
                            let currentPlayer = child.players[i];
                            let currentPoints = 0;
                            if(matchPlayerPointMap.has(currentPlayer.playerId)){
                                currentPoints = matchPlayerPointMap.get(currentPlayer.playerId);

                                //console.log('currentPoints currentPlayer',currentPoints,currentPlayer);
                                if (currentPlayer.isCaptain) {
                                    currentPoints = currentPoints * 2;
                                } else if (currentPlayer.isViceCaptain) {
                                    currentPoints = currentPoints * 1.5;
                                }
                            }
                            currentPlayer.playerPoints = currentPoints;
                        }
                        currentTeam.players = child.players;

                        currentObj.opponentTeam = currentTeam;
                    }
                }
            }

        }

        responseObject.responseData = currentObj;
        responseObject.responseCode = responseCode.SUCCESS;
        callback(null, responseObject);
    });
}

module.exports = findOpponentTeamInfoApi;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    findOpponentTeamInfoApi(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}