var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function getTeamInfoPlayers(requestObject, callback) {
	var query = UserMatchTeams.findOne({});

	if (typeof requestObject.userId !== "undefined" && requestObject.userId !== null)
		query.where("userId").equals(requestObject.userId);
	if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
		query.where("tourId").equals(requestObject.tourId);
	if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
		query.where("matchId").equals(requestObject.matchId);

	if (typeof requestObject.teamId !== "undefined" && requestObject.teamId !== null)
		query.where("teams.teamId").equals(requestObject.teamId);
	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}

		var currentObj = {};

		if(data){
			for(var i=0;i<data.teams.length;i++){
				var child = data.teams[i];
				if(child.teamId == requestObject.teamId){
					currentObj.teamPlayers = child.players;
				}
			}

			if(currentObj.teamPlayers){
				responseObject.responseData = currentObj.teamPlayers;
				responseObject.responseCode = responseCode.SUCCESS;

			}else{
				responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;

			}

		}else{
			responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;

		}

		callback(null, responseObject);
	});
}

module.exports = getTeamInfoPlayers;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.tourId = "tourId";
	//requestObject.key = "key";
	//requestObject.identifyRoles.keeper = "identifyRoles.keeper";

	findUserTeamByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}