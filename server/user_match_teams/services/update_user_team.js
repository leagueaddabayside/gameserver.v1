var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var utility = require("../../utils/utility");

function updateUserTeamApi(requestObject, callback) {
	var responseObject = new Object();

	let matchInfo = requestObject.matchInfo;
	let currentTime = moment(new Date()).unix();
	if (matchInfo.status !== 'ACTIVE' || matchInfo.startTime < currentTime) {
		responseObject.responseCode = responseCode.MATCH_NOT_ALLOWED_TO_JOIN_LEAGUE;
		callback(true, responseObject);
		return;
	}

	var query = UserMatchTeams.findOne({userId : requestObject.userId,matchId : requestObject.matchId});

	query.exec(function(error,teamResponse){
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		if(teamResponse){

			if (typeof requestObject.teamId !== "undefined" && requestObject.teamId !== null){

				let teams = teamResponse.teams;

				for(let i=0;i< teams.length;i++){
					let currentTeam = teams[i];

					let isEqualTeam = utility.checkDuplicateTeam(currentTeam.players,requestObject.players);
					if(isEqualTeam){
						responseObject.responseCode = responseCode.DUPLICATE_TEAM;
						callback(error, responseObject);
						return;
					}
				}



				for(var i=0;i<teamResponse.teams.length;i++){
					var child = teamResponse.teams[i];
					if(child.teamId == requestObject.teamId){
						child.players = requestObject.players;
						logger.info('update team',teamResponse);
						teamResponse.save(function(error, data) {
							if (error) {
								logger.error(error);
								responseObject.responseCode = responseCode.MONGO_ERROR;
								callback(error, responseObject);
								return;
							}
							responseObject.responseCode = responseCode.SUCCESS;
							responseObject.responseData = data;
							callback(null, responseObject);
						});

					}
				}
			}



		}else{
			responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
			callback(null, responseObject);
		}

	})


}

module.exports = updateUserTeamApi;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.keeper = "";
	requestObject.batsman = "";
	requestObject.bowler = "";
	console.log(requestObject);

	updateUserTeamApi(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}