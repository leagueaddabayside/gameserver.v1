var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function checkuserTeamExist(requestObject, callback) {
    var query = UserMatchTeams.findOne({userId: requestObject.userId, matchId: requestObject.matchId});

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(true, responseObject);
            return;
        }
        var currentRow = data;
        var currentObj = {};
        if (data) {
            currentObj.tourId = currentRow.tourId;
            let isTeamExist = false;
            for (var i = 0; i < currentRow.teams.length; i++) {
                var child = currentRow.teams[i];
                if (child.teamId == requestObject.teamId) {
                    isTeamExist = true;
                }
            }

            if (!isTeamExist) {
                responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
                callback(true, responseObject);
                return;
            }

            let isLeagueJoined = false;
            for (var i = 0; i < currentRow.joinedLeagues.length; i++) {
                let child = currentRow.joinedLeagues[i];
                if(requestObject.isMultiEntry){
                    if (child.leagueId == requestObject.leagueId && child.teamId == requestObject.teamId) {
                        isLeagueJoined = true;
                    }
                }else{
                    if (child.leagueId == requestObject.leagueId) {
                        isLeagueJoined = true;
                    }
                }
            }

            if (isLeagueJoined) {
                responseObject.responseCode = responseCode.LEAGUE_ALREADY_JOINED;
                callback(true, responseObject);
                return;
            }
            responseObject.responseData = currentObj;
            responseObject.responseCode = responseCode.SUCCESS;
            callback(null, responseObject);
        } else {
            responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
            callback(true, responseObject);
            return;
        }


    });
}

module.exports = checkuserTeamExist;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    checkuserTeamExist(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}