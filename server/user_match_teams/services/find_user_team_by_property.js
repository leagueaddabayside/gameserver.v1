var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findUserTeamByProperty(requestObject, callback) {
    var query = UserMatchTeams.findOne({});
    let matchInfo = requestObject.matchInfo;
    let matchPlayerPointMap = new Map();
    if(matchInfo){
        var players = matchInfo.players;

        if(players){
            for (var i = 0, length = players.length; i < length; i++) {
                var currentRow = players[i];
                var totalPoints = currentRow.totalPoints || 0;
                matchPlayerPointMap.set(currentRow.playerId, totalPoints);
            }

        }
    }

    if (typeof requestObject.userId !== "undefined" && requestObject.userId !== null)
        query.where("userId").equals(requestObject.userId);
    if (typeof requestObject.tourId !== "undefined" && requestObject.tourId !== null)
        query.where("tourId").equals(requestObject.tourId);
    if (typeof requestObject.matchId !== "undefined" && requestObject.matchId !== null)
        query.where("matchId").equals(requestObject.matchId);

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var currentRow = data;
        var currentObj = {};
        if (data) {
            currentObj.userId = currentRow.userId;
            currentObj.matchId = currentRow.matchId;
            currentObj.tourId = currentRow.tourId;

            if (typeof requestObject.teamId !== "undefined" && requestObject.teamId !== null) {
                for (var i = 0; i < currentRow.teams.length; i++) {
                    var child = currentRow.teams[i];
                    if (child.teamId == requestObject.teamId) {
                        let currentTeam = {};
                        currentTeam.teamId = child.teamId;
                        currentTeam.teamPoints = child.teamPoints;
                        if(requestObject.matchTeamPoints){
                            let teamKey = requestObject.userId + ':' + currentTeam.teamId;
                            let teamPoints = requestObject.matchTeamPoints[teamKey];
                            if(teamPoints){
                                currentTeam.teamPoints = teamPoints;

                            }
                        }
                        for (let i = 0, length = child.players.length; i < length; i++) {
                            let currentPlayer = child.players[i];
                            let currentPoints = 0;
                            if(matchPlayerPointMap.has(currentPlayer.playerId)){
                                currentPoints = matchPlayerPointMap.get(currentPlayer.playerId);

                              //  console.log('currentPoints currentPlayer',currentPoints,currentPlayer);
                                if (currentPlayer.isCaptain) {
                                    currentPoints = currentPoints * 2;
                                } else if (currentPlayer.isViceCaptain) {
                                    currentPoints = currentPoints * 1.5;
                                }
                            }
                            currentPlayer.playerPoints = currentPoints;
                        }
                        currentTeam.players = child.players;

                        currentObj.currentTeam = currentTeam;
                    }
                }
            }
            var teamArray = [];
            for (var i = 0; i < currentRow.teams.length; i++) {
                var currentTeam = currentRow.teams[i];
                var teamObj = {};
                teamObj.teamId = currentTeam.teamId;
                teamObj.teamPoints = currentTeam.teamPoints;
                if(requestObject.matchTeamPoints){
                    let teamKey = requestObject.userId + ':' + currentTeam.teamId;
                    let teamPoints = requestObject.matchTeamPoints[teamKey];
                    if(teamPoints){
                        teamObj.teamPoints = teamPoints;
                    }
                }
                teamArray.push(teamObj);
            }
            currentObj.totalTeams = currentRow.totalTeams;
            currentObj.screenName = currentRow.screenName;
            currentObj.leagueCount = currentRow.leagueCount;
            currentObj.teamsArr = teamArray;
        }

        responseObject.responseData = currentObj;
        responseObject.responseCode = responseCode.SUCCESS;
        callback(null, responseObject);
    });
}

module.exports = findUserTeamByProperty;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    findUserTeamByProperty(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}