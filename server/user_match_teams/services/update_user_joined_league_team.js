var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateUserJoinedLeagueTeamApi(requestObject, callback) {
	logger.info('updateUserJoinedLeagueTeamApi',requestObject);
	var responseObject = new Object();

	var query = UserMatchTeams.findOne({userId : requestObject.userId,matchId : requestObject.matchId});

	query.exec(function(error,teamResponse){
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		if(teamResponse){

			if (typeof requestObject.newTeamId !== "undefined" && requestObject.newTeamId !== null){
				for(var i=0;i<teamResponse.joinedLeagues.length;i++){
					var child = teamResponse.joinedLeagues[i];
					if(child.teamId == requestObject.oldTeamId && child.leagueId == requestObject.leagueId){
						child.teamId = requestObject.newTeamId;
						logger.info('update team',teamResponse);
						teamResponse.save(function(error, data) {
							if (error) {
								logger.error(error);
								responseObject.responseCode = responseCode.MONGO_ERROR;
								callback(error, responseObject);
								return;
							}
							responseObject.responseCode = responseCode.SUCCESS;
							responseObject.responseData = data;
							callback(null, responseObject);
						});
						break;
					}
				}
			}



		}else{
			responseObject.responseCode = responseCode.SOME_INTERNAL_ERROR;
			callback(null, responseObject);
		}

	})


}

module.exports = updateUserJoinedLeagueTeamApi;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.keeper = "";
	requestObject.batsman = "";
	requestObject.bowler = "";
	logger.info(requestObject);

	updateUserJoinedLeagueTeamApi(requestObject, function(error, responseObject) {
		logger.info("Response Code - " + responseObject.responseCode);
		if (error)
			logger.info("Error - " + error);
		else
			logger.info("Response Data - " + responseObject.responseData);
	});
}