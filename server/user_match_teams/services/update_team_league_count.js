var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function updateTeamLeagueCount(requestObject, callback) {
    logger.info('updateLeagueCurrentTeamCount', requestObject);

    var responseObject = new Object();
    var query = UserMatchTeams.findOne({matchId: requestObject.matchId, userId: requestObject.userId});

    query.exec(function (error, userTeamResponse) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        let joinedLeagues = userTeamResponse.joinedLeagues;
        var isAlreadyJoined = false;
        for (var i = 0; i < joinedLeagues.length; i++) {
            let joinedRow = joinedLeagues[i];
            if (joinedRow.leagueId == requestObject.leagueId) {
                isAlreadyJoined = true;
            }
        }

        if (!isAlreadyJoined) {
            userTeamResponse.leagueCount = userTeamResponse.leagueCount + 1;
        }

        userTeamResponse.joinedLeagues.push({leagueId: requestObject.leagueId, teamId: requestObject.teamId});


        userTeamResponse.updateAt = new Date();
        userTeamResponse.save(function (error, data) {
            if (error) {
                logger.error(error);
                responseObject.responseCode = responseCode.MONGO_ERROR;
                callback(error, responseObject);
                return;
            }

            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData = {leagueCount : userTeamResponse.leagueCount };
            callback(null, responseObject);
        });


    });


}

module.exports = updateTeamLeagueCount;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 254;
    requestObject.userId = 311;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    logger.info(requestObject);

    updateTeamLeagueCount(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}