var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findUserMatchTeamList(requestObject, callback) {
    var query = UserMatchTeams.find({});

    if (typeof requestObject.userId !== "undefined" && requestObject.userId !== null)
        query.where("userId").equals(requestObject.userId);

    if (typeof requestObject.leagueId !== "undefined" && requestObject.leagueId !== null)
        query.where("joinedLeagues.leagueId").equals(requestObject.leagueId);

    if (typeof requestObject.matchArr !== "undefined" && requestObject.matchArr !== null)
        query.where("matchId").in(requestObject.matchArr);

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var userMatchDataArr = data;
        responseObject.responseCode = responseCode.SUCCESS;
        if (typeof requestObject.leagueId !== "undefined" && requestObject.leagueId !== null){
            responseObject.responseData = userMatchDataArr;
            callback(null, responseObject);
            return;
        }



        let userMatchDataMap = new Map();
        for (var i=0, length=userMatchDataArr.length; i<length; i++) {
            var currentRow = userMatchDataArr[i];
            var currentObj = {};
            currentObj.leagueCount = currentRow.leagueCount;
            currentObj.userId = currentRow.userId;
            currentObj.matchId = currentRow.matchId;
            currentObj.totalTeams = currentRow.totalTeams;

            userMatchDataMap.set(currentRow.matchId , currentObj);
        }
        responseObject.responseData = userMatchDataMap;
        callback(null, responseObject);
    });
}

module.exports = findUserMatchTeamList;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    findUserMatchTeamList(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}