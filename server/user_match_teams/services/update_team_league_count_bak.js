var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function updateTeamLeagueCount(requestObject, callback) {
	var updateObject = new Object();

	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {matchId: requestObject.matchId,userId : requestObject.userId};
	UserMatchTeams.findOneAndUpdate(query, { $inc: { leagueCount: 1 }}, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateTeamLeagueCount;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.matchId = 254;
	requestObject.userId = 311;
	/*requestObject.shortName = "";
	requestObject.venue = "";
	requestObject.status = "";*/
	console.log(requestObject);

	updateTeamLeagueCount(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}