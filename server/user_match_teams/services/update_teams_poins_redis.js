var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var tourMatchService = require("../../tour_matches/services/index");
var userMatchTeamRedis = require("../../redis/user_match_team_points");

function updateTeamPointsRedis(requestObject, callback) {
    var query = UserMatchTeams.find({matchId: requestObject.matchId});

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var userMatchTeamsArr = data;

        tourMatchService.findTourMatchesByProperty({matchId: requestObject.matchId}, function (err, matchResponse) {
            if (matchResponse.responseData) {
                var players = matchResponse.responseData.players;
                let matchPlayerPointMap = new Map();
                for (var i = 0, length = players.length; i < length; i++) {
                    var currentRow = players[i];
                    var totalPoints = currentRow.totalPoints || 0;
                    matchPlayerPointMap.set(currentRow.playerId, totalPoints);
                }

                var matchUserTeamObj = {};
                for (var i = 0, length = userMatchTeamsArr.length; i < length; i++) {
                    var currentTeamRow = userMatchTeamsArr[i];
                    var userTeamUpdatesPoint = [];
                    for (var j = 0; j < currentTeamRow.teams.length; j++) {
                        var currentTeam = currentTeamRow.teams[j];
                        var teamPoints = getTeamPoints(matchPlayerPointMap, currentTeam.players);

                        let key = currentTeamRow.userId + ':' + currentTeam.teamId;
                        matchUserTeamObj[key] = teamPoints;
                    }
                }
                userMatchTeamRedis.set(requestObject.matchId, matchUserTeamObj, function () {
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseObject.responseData = {};
                    callback(null, responseObject);
                });

            }
        });

    });

}

function getTeamPoints(matchPlayersMap, teamPlayers) {

    var teamPoints = 0;
    for (var i = 0, length = teamPlayers.length; i < length; i++) {
        var currentPlayer = teamPlayers[i];
        var currentPoints = 0;
        if(matchPlayersMap.has(currentPlayer.playerId)){
            var currentPoints = matchPlayersMap.get(currentPlayer.playerId);

            //console.log('currentPoints currentPlayer',currentPoints,currentPlayer);
            if (currentPlayer.isCaptain) {
                currentPoints = currentPoints * 2;
            } else if (currentPlayer.isViceCaptain) {
                currentPoints = currentPoints * 1.5;
            }
        }
        teamPoints += currentPoints;
    }
    console.log('getTeamPoints',teamPoints);
    return teamPoints;
}


module.exports = updateTeamPointsRedis;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 463;
    //requestObject.userId = 311;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    console.log(requestObject);

    updateTeamPointsRedis(requestObject, function (error, responseObject) {
        console.log("Response Code - " , responseObject);

    });
}