var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");

function updateTeamLeagueCountOnCancel(requestObject, callback) {
    logger.info('updateLeagueCurrentTeamCount', requestObject);

    var responseObject = new Object();
    var query = UserMatchTeams.findOne({matchId: requestObject.matchId, userId: requestObject.userId});

    query.exec(function (error, userTeamResponse) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }

        let joinedLeagues = userTeamResponse.joinedLeagues;
        var mySet = new Set();
        for (var i = 0; i < joinedLeagues.length; i++) {
            let joinedRow = joinedLeagues[i];
            if (joinedRow.leagueId == requestObject.leagueId && joinedRow.teamId == requestObject.teamId) {
                joinedLeagues.splice(i, 1);
            }
        }

        for (var i = 0; i < joinedLeagues.length; i++) {
            let joinedRow = joinedLeagues[i];
                mySet.add(joinedRow.leagueId);

        }

        logger.info(mySet);
        userTeamResponse.leagueCount = mySet.size;

        userTeamResponse.updateAt = new Date();
        userTeamResponse.save(function (error, data) {
            if (error) {
                logger.error(error);
                responseObject.responseCode = responseCode.MONGO_ERROR;
                callback(error, responseObject);
                return;
            }

            responseObject.responseCode = responseCode.SUCCESS;
            responseObject.responseData = data;
            callback(null, responseObject);
        });


    });


}

module.exports = updateTeamLeagueCountOnCancel;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 6;
    requestObject.userId = 16;
    requestObject.teamId = 1;
    requestObject.leagueId = 1;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    logger.info(requestObject);

    updateTeamLeagueCountOnCancel(requestObject, function (error, responseObject) {
        logger.info("Response Code - " + responseObject.responseCode);
        if (error)
            logger.info("Error - " + error);
        else
            logger.info("Response Data - " + responseObject.responseData);
    });
}