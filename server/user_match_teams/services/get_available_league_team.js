var UserMatchTeams = require("../models/user_match_teams");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var CONSTANT = require("../../utils/constant");

function getAvailableLeagueTeam(requestObject, callback) {
    var query = UserMatchTeams.findOne({userId: requestObject.userId, matchId: requestObject.matchId});

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(true, responseObject);
            return;
        }
        var currentRow = data;
        if (data) {
            let teamArr = [];
            for (var i = 0; i < currentRow.teams.length; i++) {
                var child = currentRow.teams[i];
                teamArr.push(child.teamId);
            }

            if (teamArr.length == 0) {
                responseObject.responseData ={totalTeams : 0} ;
                responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
                callback(true, responseObject);
                return;
            }

            let isLeagueJoined = false;
            for (var i = 0; i < currentRow.joinedLeagues.length; i++) {
                let child = currentRow.joinedLeagues[i];
                if (requestObject.isMultiEntry) {
                    if (child.leagueId == requestObject.leagueId) {

                        var index = teamArr.indexOf(child.teamId);
                        if (index > -1) {
                            teamArr.splice(index, 1);
                        }

                    }
                } else {
                    if (child.leagueId == requestObject.leagueId) {
                        isLeagueJoined = true;
                    }
                }
            }

            if (isLeagueJoined) {
                responseObject.responseCode = responseCode.LEAGUE_ALREADY_JOINED;
                callback(true, responseObject);
                return;
            }

            if (teamArr.length == 0) {
                if (currentRow.totalTeams === CONSTANT.MAX_USER_TEAM_ALLOWED) {
                    responseObject.responseCode = responseCode.REJOIN_LEAGUE_LIMIT_EXCEED;
                    callback(true, responseObject);
                    return;
                } else {
                    responseObject.responseData ={totalTeams : currentRow.totalTeams} ;
                    responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
                    callback(true, responseObject);
                    return;
                }

            }


            responseObject.responseData ={teamArr : teamArr,totalTeams : currentRow.totalTeams} ;
            responseObject.responseCode = responseCode.SUCCESS;
            callback(null, responseObject);
        } else {
            responseObject.responseData ={totalTeams : 0} ;
            responseObject.responseCode = responseCode.TEAM_DOES_NOT_EXIST;
            callback(true, responseObject);
            return;
        }


    });
}

module.exports = getAvailableLeagueTeam;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    //requestObject.tourId = "tourId";
    //requestObject.key = "key";
    //requestObject.identifyRoles.keeper = "identifyRoles.keeper";

    getAvailableLeagueTeam(requestObject, function (error, responseObject) {
        console.log("Response Code - " + responseObject.responseCode);
        if (error)
            console.log("Error - " + error);
        else
            console.log("Response Data - " + responseObject.responseData);
    });
}