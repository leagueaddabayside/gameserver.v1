var UserMatchTeams = require("../models/user_match_teams");
var UserMatchTeamsService = require("../services/index");

var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var moment = require("moment");
var tourMatchService = require("../../tour_matches/services/index");
var userMatchTeamRedis = require("../../redis/user_match_team_points");

function updateMatchUserTeamPoints(requestObject, callback) {
    var query = UserMatchTeams.find({matchId: requestObject.matchId});

    var responseObject = new Object();
    query.exec(function (error, data) {
        if (error) {
            logger.error(error);
            responseObject.responseCode = responseCode.MONGO_ERROR;
            callback(error, responseObject);
            return;
        }
        var userMatchTeamsArr = data;

        tourMatchService.findTourMatchesByProperty({matchId: requestObject.matchId}, function (err, matchResponse) {
            if (matchResponse.responseData) {
                var players = matchResponse.responseData.players;
                let matchPlayerPointMap = new Map();
                for (var i = 0, length = players.length; i < length; i++) {
                    var currentRow = players[i];
                    var totalPoints = currentRow.totalPoints || 0;
                    matchPlayerPointMap.set(currentRow.playerId, totalPoints);
                }


                async.eachSeries(userMatchTeamsArr,function(currentTeamRow,teamCB){
                    let userTeamObj = {};
                    for (var j = 0; j < currentTeamRow.teams.length; j++) {
                        var currentTeam = currentTeamRow.teams[j];
                        var teamPoints = getTeamPoints(matchPlayerPointMap, currentTeam.players);

                        userTeamObj[currentTeam.teamId] = teamPoints;
                    }
                    let updateTeamPointsObj = {};
                    updateTeamPointsObj.userTeamObj = userTeamObj;
                    updateTeamPointsObj.userId = currentTeamRow.userId;
                    updateTeamPointsObj.matchId = currentTeamRow.matchId;

                    UserMatchTeamsService.updateUserTeamPointsApi(updateTeamPointsObj,function(err,resp){
                        teamCB(null);
                    });

                },function(err){
                    responseObject.responseCode = responseCode.SUCCESS;
                    responseObject.responseData = {};
                    callback(null, responseObject);
                });


            }
        });

    });

}

function getTeamPoints(matchPlayersMap, teamPlayers) {

    var teamPoints = 0;
    for (var i = 0, length = teamPlayers.length; i < length; i++) {
        var currentPlayer = teamPlayers[i];
        var currentPoints = 0;
        if(matchPlayersMap.has(currentPlayer.playerId)){
            var currentPoints = matchPlayersMap.get(currentPlayer.playerId);

            //console.log('currentPoints currentPlayer',currentPoints,currentPlayer);
            if (currentPlayer.isCaptain) {
                currentPoints = currentPoints * 2;
            } else if (currentPlayer.isViceCaptain) {
                currentPoints = currentPoints * 1.5;
            }
        }
        teamPoints += currentPoints;
    }
    console.log('getTeamPoints',teamPoints);
    return teamPoints;
}


module.exports = updateMatchUserTeamPoints;

// Unit Test Case
if (require.main === module) {
    var requestObject = new Object();
    requestObject.matchId = 463;
    //requestObject.userId = 311;
    /*requestObject.shortName = "";
     requestObject.venue = "";
     requestObject.status = "";*/
    console.log(requestObject);

    updateMatchUserTeamPoints(requestObject, function (error, responseObject) {
        console.log("Response Code - " , responseObject);

    });
}