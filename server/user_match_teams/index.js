var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');
var validateUserTeam = require('./filters/validate_user_team');

route.post("/saveTeam",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getUserInfo,middleware.getTourMatchInfo,validateUserTeam, obj.addUserTeamApi);
route.post("/updateUserTeam",middleware.auditTrailLog,middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getTourMatchInfo,validateUserTeam, obj.updateUserTeamApi);
route.post("/getTeamInfo",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getPlayerMatchTeamRedisPoints,middleware.getTourMatchInfo, obj.findUserTeamByPropertyApi);
route.post("/findOpponentTeamInfo",middleware.auditTrailLog,middleware.getUserIdFromToken,middleware.getPlayerMatchTeamRedisPoints,middleware.getTourMatchInfo, obj.findOpponentTeamInfoApi);

// Routes
module.exports = route;
