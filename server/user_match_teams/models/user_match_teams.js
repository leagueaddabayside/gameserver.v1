require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var UserMatchTeamsSchema = new mongoose.Schema({
    id: {type: Number, unique: true},
    userId: {type: Number},
    matchId: {type: Number},
    tourId: {type: Number},
    teams: [{teamId: Number, players: [] , teamPoints : Number}],
    totalTeams: Number,
    leagueCount : Number,
    joinedLeagues : [{leagueId : Number,teamId : Number}],
    screenName : String,
    createBy: Number,
    createAt: {type: Date, default: Date.now},
    updateAt: {type: Date}
});

module.exports = mongoose.model("UserMatchTeams", UserMatchTeamsSchema, "user_match_teams");

UserMatchTeamsSchema.plugin(autoIncrement.plugin, {
    model: "UserMatchTeams",
    field: "id",
    startAt: 1,
    incrementBy: 1
});