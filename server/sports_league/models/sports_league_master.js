require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var SportsLeagueMasterSchema = new mongoose.Schema({
	sportsLeagueId: {type: Number, unique: true},
	name: {type: String, unique: true},
	shortName: {type: String},
	displayName: {type: String},
	tpLeagueId: {type: Number, unique: true},
	key: {type: String},
	status: {type: String, enum: ['ACTIVE', 'INACTIVE']},
	displayOrder: {type: Number},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("SportsLeagueMaster", SportsLeagueMasterSchema, "sports_league_master");

SportsLeagueMasterSchema.plugin(autoIncrement.plugin, {
	model: "SportsLeagueMaster",
	field: "sportsLeagueId",
	startAt: 1,
	incrementBy: 1
});