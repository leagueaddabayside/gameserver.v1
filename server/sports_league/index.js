var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();

route.post("/addSportsLeague", obj.addSportsLeague);
route.post("/updateSportsLeague", obj.updateSportsLeague);
route.post("/findSportsLeagueByProperty", obj.findSportsLeagueByProperty);
route.post("/findSportsLeagueList", obj.findSportsLeagueList);
	route.post("/updateSportsLeagueDisplayOrder", obj.updateSportsLeagueDisplayOrder);
// Routes
module.exports = route;
