var logger = require("../../utils/logger").gameLogs;
var sportsLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findSportsLeagueByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findSportsLeagueByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	sportsLeagueService.findSportsLeagueByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("findSportsLeagueByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findSportsLeagueByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.name = "name";
		//requestObject.shortName = "shortName";
		//requestObject.displayName = "displayName";
		//requestObject.key = "key";
		//requestObject.tpLeagueId = "tpLeagueId";
		//requestObject.status = "status";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findSportsLeagueByProperty(request, response);
	})();
}