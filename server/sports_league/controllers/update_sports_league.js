var logger = require("../../utils/logger").gameLogs;
var sportsLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateSportsLeague(request, response, next) {
	var requestObject = request.body;
	//console.log("updateSportsLeague API :- Request - %j", requestObject);

	var responseObject = new Object();
	sportsLeagueService.updateSportsLeague(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateSportsLeague API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateSportsLeague;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.name = "";
		requestObject.shortName = "";
		requestObject.displayName = "";
		requestObject.key = "";
		requestObject.tpLeagueId = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		updateSportsLeague(request, response);
	})();
}