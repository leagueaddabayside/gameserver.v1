var addSportsLeagueApi = require("./add_sports_league");
var findSportsLeagueByPropertyApi = require("./find_sports_league_by_property");
var findSportsLeagueListApi = require("./find_sports_league_list");
var updateSportsLeagueApi = require("./update_sports_league");
var updateSportsLeagueDisplayOrderApi = require("./update_sports_league_display_order");
// Require

module.exports.addSportsLeague = addSportsLeagueApi;
module.exports.findSportsLeagueByProperty = findSportsLeagueByPropertyApi;
module.exports.findSportsLeagueList = findSportsLeagueListApi;
module.exports.updateSportsLeague = updateSportsLeagueApi;
module.exports.updateSportsLeagueDisplayOrder = updateSportsLeagueDisplayOrderApi
// Export
