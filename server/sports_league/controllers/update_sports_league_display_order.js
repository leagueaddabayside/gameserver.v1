var logger = require("../../utils/logger").gameLogs;
var sportsLeagueService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function updateSportsLeagueDisplayOrder(request, response, next) {
	var requestObject = request.body;
	//console.log("updateSportsLeagueDisplayOrder API :- Request - %j", requestObject);

	var responseObject = new Object();
	sportsLeagueService.updateSportsLeagueDisplayOrder(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("updateSportsLeagueDisplayOrder API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = updateSportsLeagueDisplayOrder;