var sportsLeagueMaster = require("../models/sports_league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findSportsLeagueList(requestObject, callback) {
	var query = sportsLeagueMaster.find({});
	query.sort("displayOrder");

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var sportsLeagueArr = data;
			var sportsLeagueList = [];
			for (var i=0, length=sportsLeagueArr.length; i<length; i++) {
				var currentRow = sportsLeagueArr[i];
				var currentObj = {};
				currentObj.name = currentRow.name;
				currentObj.shortName = currentRow.shortName;
				currentObj.displayName = currentRow.displayName;
				currentObj.key = currentRow.key;
				currentObj.tpLeagueId = currentRow.tpLeagueId;
				currentObj.status = currentRow.status;
				sportsLeagueList.push(currentObj);
			}
			responseObject.responseData = sportsLeagueList;
		callback(null, responseObject);
	});
}

module.exports = findSportsLeagueList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findSportsLeagueList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}