var sportsLeagueMaster = require("../models/sports_league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function addSportsLeague(requestObject, callback) {
	var newSportsLeague = new sportsLeagueMaster({
		name : requestObject.name,
		shortName : requestObject.shortName,
		displayName : requestObject.displayName,
		key : requestObject.key,
		tpLeagueId : requestObject.tpLeagueId,
		status : requestObject.status,
		createBy : requestObject.createBy
	});

	var responseObject = new Object();
	newSportsLeague.save(function(error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = addSportsLeague;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.name = "";
	requestObject.shortName = "";
	requestObject.displayName = "";
	requestObject.key = "";
	requestObject.tpLeagueId = "";
	requestObject.status = "";
	requestObject.createBy = "";
	console.log(requestObject);

	addSportsLeague(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}