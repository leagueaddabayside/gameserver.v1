var sportsLeagueMaster = require("../models/sports_league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function updateSportsLeague(requestObject, callback) {
	var updateObject = new Object();
	if(requestObject.shortName !== 'undefined' && requestObject.shortName !== null)
		updateObject.shortName = requestObject.shortName;
	if(requestObject.displayName !== 'undefined' && requestObject.displayName !== null)
		updateObject.displayName = requestObject.displayName;
	if(requestObject.name !== 'undefined' && requestObject.name !== null)
		updateObject.name = requestObject.name;
	if(requestObject.status !== 'undefined' && requestObject.status !== null)
		updateObject.status = requestObject.status;
	updateObject.updateAt = new Date();

	var responseObject = new Object();
	var query = {sportsLeagueId: requestObject.sportsLeagueId};
	sportsLeagueMaster.findOneAndUpdate(query, updateObject, function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		responseObject.responseData = data;
		callback(null, responseObject);
	});
}

module.exports = updateSportsLeague;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	requestObject.shortName = "";
	requestObject.displayName = "";
	requestObject.name = "";
	requestObject.status = "";
	console.log(requestObject);

	updateSportsLeague(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}