var sportsLeagueMaster = require("../models/sports_league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");
var async = require("async");

function updateSportsLeagueDisplayOrder(requestObject, callback) {
	var currentRowNumber = 0;
	async.each(requestObject.sportsLeagueList, function(currentRow, cb) {
		currentRowNumber++;
		console.log('currentRow - ', currentRow);

		var updateObject = new Object();
		updateObject.displayOrder = currentRowNumber;
		var responseObject = new Object();
		var query = {sportsLeagueId : currentRow.sportsLeagueId};
		sportsLeagueMaster.findOneAndUpdate(query, updateObject, function(error, data) {
			if (error) {
				logger.error(error);
				cb(error);
				return;
			}
			responseObject.responseCode = responseCode.SUCCESS;
			responseObject.responseData = data;
			cb();
		});
	}, function(error) {
		if (error) {
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = updateSportsLeagueDisplayOrder;

// Unit Test Case
if (require.main === module) {
	var sportsLeagueList = [ {
		sportsLeagueId : 1,
		displayOrder : 1
	}, {
		sportsLeagueId : 2,
		displayOrder : 1
	} ];
	var responseObject = new Object();
	requestObject.sportsLeagueList = sportsLeagueList;
	console.log(requestObject);

	updateSportsLeague(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}