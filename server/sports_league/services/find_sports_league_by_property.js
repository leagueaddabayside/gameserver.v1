var sportsLeagueMaster = require("../models/sports_league_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findSportsLeagueByProperty(requestObject, callback) {
	var query = sportsLeagueMaster.findOne({});
	if (typeof requestObject.sportsLeagueId !== "undefined" && requestObject.sportsLeagueId !== null)
		query.where("sportsLeagueId").equals(requestObject.sportsLeagueId);
	if (typeof requestObject.key !== "undefined" && requestObject.key !== null)
		query.where("key").equals(requestObject.key);
	if (typeof requestObject.tpLeagueId !== "undefined" && requestObject.tpLeagueId !== null)
		query.where("tpLeagueId").equals(requestObject.tpLeagueId);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.name = currentRow.name;
			currentObj.shortName = currentRow.shortName;
			currentObj.displayName = currentRow.displayName;
			currentObj.key = currentRow.key;
			currentObj.tpLeagueId = currentRow.tpLeagueId;
			currentObj.status = currentRow.status;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findSportsLeagueByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.sportsLeagueId = "sportsLeagueId";
	//requestObject.key = "key";
	//requestObject.tpLeagueId = "tpLeagueId";

	findSportsLeagueByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}