var addSportsLeagueService = require("./add_sports_league");
var findSportsLeagueByPropertyService = require("./find_sports_league_by_property");
var findSportsLeagueListService = require("./find_sports_league_list");
var updateSportsLeagueService = require("./update_sports_league");
var updateSportsLeagueDisplayOrderService = require("./update_sports_league_display_order");
// Require

module.exports.addSportsLeague = addSportsLeagueService;
module.exports.findSportsLeagueByProperty = findSportsLeagueByPropertyService;
module.exports.findSportsLeagueList = findSportsLeagueListService;
module.exports.updateSportsLeague = updateSportsLeagueService;
module.exports.updateSportsLeagueDisplayOrder = updateSportsLeagueDisplayOrderService
// Export
