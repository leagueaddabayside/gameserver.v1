require('../../utils/mongo_connection');
var mongoose = require("mongoose");
var autoIncrement = require("mongoose-auto-increment");

var ScoreTemplateMasterSchema = new mongoose.Schema({
	templateId: {type: Number, unique: true},
	templateName: {type: String, unique: true},
	remarks: {type: String},
	data: {type: Object},
	status: {type: String, enum: ["ACTIVE", "INACTIVE"]},
	createBy: Number,
	createAt: {type: Date, default: Date.now},
	updateAt: {type: Date}
});

module.exports = mongoose.model("ScoreTemplateMaster", ScoreTemplateMasterSchema, "score_template_master");

ScoreTemplateMasterSchema.plugin(autoIncrement.plugin, {
	model: "ScoreTemplateMaster",
	field: "templateId",
	startAt: 1,
	incrementBy: 1
});