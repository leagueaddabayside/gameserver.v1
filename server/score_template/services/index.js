var addScoreTemplateService = require("./add_score_template");
var findScoreTemplateByPropertyService = require("./find_score_template_by_property");
var findScoreTemplateListService = require("./find_score_template_list");
var updateScoreTemplateService = require("./update_score_template");
// Require

module.exports.addScoreTemplate = addScoreTemplateService;
module.exports.findScoreTemplateByProperty = findScoreTemplateByPropertyService;
module.exports.findScoreTemplateList = findScoreTemplateListService;
module.exports.updateScoreTemplate = updateScoreTemplateService
// Export
