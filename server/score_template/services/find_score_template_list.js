var league_templateMaster = require("../models/score_template_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueTemplateList(requestObject, callback) {
	var query = league_templateMaster.find({});
	if (typeof requestObject.status !== "undefined" && requestObject.status !== null)
		query.where("status").equals(requestObject.status);

	if (typeof requestObject.templateArr !== "undefined" && requestObject.templateArr !== null)
		query.where("templateId").in(requestObject.templateArr);

	if (typeof requestObject.isDefaultLeague !== "undefined" && requestObject.isDefaultLeague !== null)
		query.where("data.isDefaultLeague").equals(requestObject.isDefaultLeague);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
		responseObject.responseCode = responseCode.SUCCESS;
			var league_templateArr = data;
			var league_templateList = [];
			for (var i=0, length=league_templateArr.length; i<length; i++) {
				var currentRow = league_templateArr[i];
				var currentObj = {};
				currentObj.templateId = currentRow.templateId;
				currentObj.templateName = currentRow.templateName;
				currentObj.remarks = currentRow.remarks;
				currentObj.data = currentRow.data;
				currentObj.status = currentRow.status;
				league_templateList.push(currentObj);
			}
			responseObject.responseData = league_templateList;
		callback(null, responseObject);
	});
}

module.exports = findLeagueTemplateList;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.status = "ACTIVE";

	findLeagueTemplateList(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}