var league_templateMaster = require("../models/score_template_master");
var logger = require("../../utils/logger").gameLogs;
var responseCode = require("../../utils/response_code");

function findLeagueTemplateByProperty(requestObject, callback) {
	var query = league_templateMaster.findOne({});
	if (typeof requestObject.templateId !== "undefined" && requestObject.templateId !== null)
		query.where("templateId").equals(requestObject.templateId);
	if (typeof requestObject.templateName !== "undefined" && requestObject.templateName !== null)
		query.where("templateName").equals(requestObject.templateName);

	var responseObject = new Object();
	query.exec(function (error, data) {
		if (error) {
			logger.error(error);
			responseObject.responseCode = responseCode.MONGO_ERROR;
			callback(error, responseObject);
			return;
		}
			var currentRow = data;
			var currentObj = {};
			if(data){
			currentObj.templateId = currentRow.templateId;
			currentObj.templateName = currentRow.templateName;
			currentObj.remarks = currentRow.remarks;
			currentObj.data = currentRow.data;
			currentObj.status = currentRow.status;
			currentObj.createBy = currentRow.createBy;
			}

			responseObject.responseData = currentObj;
		responseObject.responseCode = responseCode.SUCCESS;
		callback(null, responseObject);
	});
}

module.exports = findLeagueTemplateByProperty;

// Unit Test Case
if (require.main === module) {
	var requestObject = new Object();
	//requestObject.templateId = "templateId";
	//requestObject.templateName = "templateName";

	findLeagueTemplateByProperty(requestObject, function(error, responseObject) {
		console.log("Response Code - " + responseObject.responseCode);
		if (error)
			console.log("Error - " + error);
		else
			console.log("Response Data - " + responseObject.responseData);
	});
}