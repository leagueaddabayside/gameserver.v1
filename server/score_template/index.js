var obj = require('./controllers/index');
var express = require('express');
var route = express.Router();
var middleware = require('../middleware/index');

route.post("/addScoreTemplate",middleware.getAdminIdFromToken, obj.addScoreTemplate);
route.post("/updateScoreTemplate",middleware.getAdminIdFromToken, obj.updateScoreTemplate);
route.post("/findScoreTemplateByProperty",middleware.getAdminIdFromToken, obj.findScoreTemplateByProperty);
route.post("/findScoreTemplateList",middleware.getAdminIdFromToken, obj.findScoreTemplateList);
// Routes
module.exports = route;
