var addScoreTemplateApi = require("./add_score_template");
var findScoreTemplateByPropertyApi = require("./find_score_template_by_property");
var findScoreTemplateListApi = require("./find_score_template_list");
var updateScoreTemplateApi = require("./update_score_template");
// Require

module.exports.addScoreTemplate = addScoreTemplateApi;
module.exports.findScoreTemplateByProperty = findScoreTemplateByPropertyApi;
module.exports.findScoreTemplateList = findScoreTemplateListApi;
module.exports.updateScoreTemplate = updateScoreTemplateApi
// Export
