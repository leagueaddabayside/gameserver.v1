var logger = require("../../utils/logger").gameLogs;
var league_templateService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function findLeagueTemplateByProperty(request, response, next) {
	var requestObject = request.body;
	//console.log("findLeagueTemplateByProperty API :- Request - %j", requestObject);

	var responseObject = new Object();
	league_templateService.findScoreTemplateByProperty(requestObject, function(error, data) {
		if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("findLeagueTemplateByProperty API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = findLeagueTemplateByProperty;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		//requestObject.templateId = "templateId";
		//requestObject.templateName = "templateName";
		//requestObject.remarks = "remarks";
		//requestObject.data = "data";
		//requestObject.status = "status";
		//requestObject.createBy = "createBy";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		findLeagueTemplateByProperty(request, response);
	})();
}