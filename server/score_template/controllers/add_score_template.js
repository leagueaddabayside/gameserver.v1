var logger = require("../../utils/logger").gameLogs;
var league_templateService = require("../services/index");
var responseMessage = require("../../utils/response_message");
var responseCode = require("../../utils/response_code");

function addLeagueTemplate(request, response, next) {
	var requestObject = request.body;
	//console.log("addLeagueTemplate API :- Request - %j", requestObject);

	var responseObject = new Object();
	league_templateService.addScoreTemplate(requestObject, function(error, data) {
	if(data.responseCode !== responseCode.SUCCESS) {
			responseObject.respCode = data.responseCode;
			responseObject.message = responseMessage[data.responseCode];
		} else {
			responseObject.respCode = data.responseCode;
			responseObject.respData = data.responseData;
		}

		logger.info("addLeagueTemplate API :- Response - %j", responseObject);
		response.json(responseObject);
	});
}

module.exports = addLeagueTemplate;

// Unit Test Case
if (require.main === module) {
	(function() {
		var request = {};
		var response = {
			json : function(result) {
				console.log(JSON.stringify(result, null, 2));
			}
		};

		var requestObject = new Object();
		requestObject.templateName = "";
		requestObject.remarks = "";
		requestObject.data = "";
		requestObject.status = "";
		requestObject.createBy = "";

		console.log("Request Data - " + requestObject);
		request.body = requestObject;
		addLeagueTemplate(request, response);
	})();
}