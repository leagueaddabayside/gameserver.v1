'use strict';

const express = require('./express');
const config = require('../bin/config').server;
const https = require('https');
const fs = require('fs');

module.exports.start = function start(callback) {
    var app = express.init();

    if (config.ssl) {
        var options = {
            hostname: config.host,
            key: fs.readFileSync(config.privateKey),
            cert: fs.readFileSync(config.certificate)
        };

        https.createServer(options, app).listen(config.port, function () {
            console.log('https on: http://' + config.host + ':' + config.port);
        });

    } else {
        console.log("config in express init:",config)
        app.listen(config.port,function () {
            console.log('listening on port : ' + config.host + ':' + config.port);
        });
    }

};
