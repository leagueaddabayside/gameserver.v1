'use strict';

/**
 * Module dependencies.
 */
var config = require('../bin/config');
var logger = require(config.__base + 'server/utils/logger').gameLogs;
const path = require('path');

const express = require('express'),
    bodyParser = require('body-parser'),
    app = express(),
    cors = require('cors'),
    fs = require('fs');


/**
 * Initialize application middleware
 */
module.exports.initMiddleware = function (app) {
    app.use(cors());
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
};


module.exports.handleExceptions= () =>{
    process.on('uncaughtException', function (e) {
        logger.error(e);
    })
}

/**
 * Configure error handling
 */
module.exports.initErrorRoutes = function (app) {
    app.use(function (err, req, res, next) {
        // If the error object doesn't exists
        if (!err) {
            return next();
        }

        // Log it
        console.error(err.stack);

        // Redirect to error page
        // res.redirect('/server-error');
    });
};

/**
 * Configure routes loading
 */
module.exports.initRoutes = function (app) {
    // console.log("config base url",config.__base);
    // console.log("inside the init route")
    var route =  require(config.__base+'server/routes/index');
    route(app);
};


/**
 * Initialize the Express application
 */
module.exports.init = function (db) {

    // console.log(config);
    // Initialize express app
    var app = express();

    this.handleExceptions();

    this.initErrorRoutes(app);
    this.initMiddleware(app);
    this.initRoutes(app);

    return app;
};
