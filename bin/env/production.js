'use strict';

module.exports = {
  db: {
    uri: process.env.MONGOHQ_URL || process.env.MONGOLAB_URI || 'mongodb://' + (process.env.DB_1_PORT_27017_TCP_ADDR || 'localhost:27017') + '/notifications',
    options: {
      user: '',
      pass: ''
    },
    // Enable mongoose debug mode
    debug: process.env.MONGODB_DEBUG || false
  },
  session: {
    maxAge: 1200000,
    SECRET: 'notify'
  },
  mysql: {
    host: '192.168.2.210',
    user: 'gauss',
    password: '',
    database: 'fantasy_cricket',
    logginng:false,
  },
  redis: {
    host: '192.168.2.210',
    port: '6379'
  },
  mongo: {
    host: '192.168.2.210',
    port: '27017'
  },
  serverUrl : 'http://localhost:3000', //http://www.thepokerbaazi.com   http://localhost:3000
  WebsiteServerUrl : 'http://192.168.2.210:3000',
  isProduction : false,
  serverAuth: {
    accessKey: 'mn@zaq@213',
    ip: '127.0.0.1'
  },
  server: {
    ssl: false,
    port : 4000,
    host : '0.0.0.0',
    privateKey: '/etc/pki/CA/certs/leagueadda/www_leagueadda_com.key',
    certificate: '/etc/pki/CA/certs/leagueadda/www_leagueadda_com.crt'
  }
};
