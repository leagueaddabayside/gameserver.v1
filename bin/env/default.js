'use strict';

module.exports = {
    app: {
        title: 'Cricket Fantasy',
        description: 'Full-Stack JavaScript App with MongoDB, Express, and Node.js',
    },
    port: process.env.PORT || 3000

};
